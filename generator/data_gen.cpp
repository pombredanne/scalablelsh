/*
 * data_gen.cpp
 *
 *  Created on: May 21, 2012
 *      Author: george
 */
#include <string>
#include <iostream>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, char**argv){
	if(argc != 3){
		std::cout << "./data_gen <#dimensions> <#registers>"<<std::endl;
		exit(1);
	}
	int dimensions = atoi(argv[1]);
	int registers = atoi(argv[2]);

	// Write header
	std::cout << "KEYPOINTS SIFT" << std::endl;
	std::cout << "1" << std::endl; // number of images, not used
	std::cout << registers << std::endl;
	std::cout << "ID IMAGE ROW COL SCALE ANGLE VECTOR" << std::endl;

	for(int i = 0; i < registers; i++){
		std::cout << i << " image.key 1.0 1.0 1.0 1.0 ";
		for(int j = 0; j < dimensions; j++){
			uint8_t element = (uint8_t) (127 * ((float)random() / (float)RAND_MAX));
			std::cout << (int)element<<" ";
		}
		std::cout << std::endl;
	}
	return 1;
}


