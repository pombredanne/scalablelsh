/**
 * @file
 * LSBucketsPointsMsg.h
 *
 *  Created on: Sep 13, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef LSBUCKETPOINTSMSG_H_
#define LSBUCKETPOINTSMSG_H_

#include "LabelStream.h"

/**
 * \class MsgBktPt
 * \brief This messages
 * from BucketsIndex to DataPoints with
 * points' id that matched the same bucket as the query.
 * This message contains the points' id
 * found in the same query's bucket.
 *
 * The message is composed by the label, the query Id in header control field and
 * the number of instances of DataPoints which will receive points to calculate ANN.
 * This later information optimizes the number of messages sent among filters, and it is also
 * forwarded to Aggregator instances responsible for the query in order to know the end of query processing.
 *
 * Summarizing is this:
 * @code
 * [[MsgLS label <label> + <query Id>] + <number of DP instances in uint32_t> + <bucket's points in uint32_t>]
 * @endcode
 *
 */
class MsgBktPt : public MsgLS {
private:
	T_P * pointsId;
	T_Q * query;

	/** Header info besides the MsgLS header. */
	/*! Number of info in the local header. */
	const static uint32_t l_header_ninfo = 2;
	/*! Size of the label in 4 bytes and control close stream in other 4. */
	const static uint32_t l_sizeof_header = l_header_ninfo*sizeof(uint32_t);
	/*! Positions in header for number of DataPoints instances which will receive at least one point. */
	const static uint32_t l_header_pos_nDpInst = 0;
	/*! Position in header for  id used in query. */
	const static uint32_t l_header_pos_probeId = 1;
	/*! Size of all headers in the message including label stream and local.*/
	const static uint32_t global_sizeof_header = l_sizeof_header + MsgLS::sizeof_header;

	/**
	 * Get value in local header message.
	 * @param l_hd_pos Position in local header.
	 * @return return the value for the position.
	 */
	uint32_t getLocalHeaderPos(uint32_t l_hd_pos) const {
		if(l_hd_pos >= l_header_ninfo) {
			cerr << "[MsgBktPt][getLocalHeaderPos] ERROR accessing invalid position " << l_hd_pos << endl;
			exit(1);
		}
		return ((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo + l_hd_pos];
	}

	/**
	 * Set value in local header message.
	 * @param l_hd_pos Position in local header.
	 * @param value Value assigned.
	 */
	void setLocalHeaderPos(uint32_t l_hd_pos, uint32_t value) const {
		if(l_hd_pos >= l_header_ninfo) {
			cerr << "[MsgBktPt][setLocalHeaderPos] ERROR accessing invalid position " << l_hd_pos << endl;
			exit(EXIT_FAILURE);
		}
		((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo + l_hd_pos] =  value;
	}

	void setUpMessage(uint32_t q_size, uint32_t p_size) {
		if(MsgLS::getMsg() == NULL) {
			cerr << "[MsgBktPt][setUpMessage] ERROR msg not allocated. " << endl;
			exit(EXIT_FAILURE);
		}

		/* Set up the pointers of the message. */
		if(p_size > 0) {
			pointsId = (T_P *) ( ((uint8_t *) MsgLS::getMsg()) + global_sizeof_header );
		}  else {
			pointsId = NULL;
		}
		if(q_size > 0) {
			query = (T_Q *) ( ((uint8_t *) MsgLS::getMsg()) + global_sizeof_header + (p_size*sizeof(T_P)) );
		} else {
			query = NULL;
		}
	}

public:
	/**
	 * MsgBktPt constructor.
	 * Receives the number of elements in query and the number of points from the bucket to allocate
	 * the message. It also allocates more sizeof(uint32_t) to store the number of DataPoints instances
	 * which will receive points for this bucket in this query.
	 *
	 * @param q_size Number of elements in query.
	 * @param p_size Number of elements in points.
	 */
	MsgBktPt(uint32_t q_size, uint32_t p_size) :
		MsgLS((q_size*sizeof(T_Q))+(p_size*sizeof(T_P))+l_sizeof_header)
	{
		/** Store 0 as the number of DataPoints instances (nDpInst) in the message (from 8th byte).
		 * It is the first four bytes after the LabelHeader and LabelControl.*/
		((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo] = 0;

		setUpMessage(q_size, p_size);
	}

	MsgBktPt() : MsgLS(), pointsId(NULL), query(NULL)
	{}

	/**
	 * MsgBktPt destructor.
	 *
	 */
	virtual ~MsgBktPt()
	{
		pointsId = NULL;
		query = NULL;
	}

	/**
	 * Set up the message.
	 * @param q_size Number of elements in query.
	 * @param p_size Returns the number of points in the message.
	 */
	void deserialize(uint32_t q_size, uint32_t& p_size) {
		p_size = getNumPoints(getSizeMsg(), q_size);
		setUpMessage(q_size, p_size);
//		cout << "deserialize p_size " << p_size << " q_size " << q_size << " getSizeMsg " << getSizeMsg() << endl;
	}


	/**
	 * Get the pointer to pointsId buffer in the message.
	 * @return the pointer to pointsId buffer in the message.
	 */
	T_P * getPointsId() const {
		return pointsId;
	}

	/**
	 * Get the pointer to query buffer in the message.
	 * @return the pointer to query buffer in the message.
	 */
	T_Q * getQuery() const {
		return query;
	}

	/**
	 * Given the message's size, it returns the number of points within the message.
	 *
	 * @param mem_msg_size Message size in bytes.
	 * @param d Number of dimensions of the query (it means the number of elements in query).
	 * @return Number of points in the message.
	 */
	static uint32_t getNumPoints(const uint32_t mem_msg_size, const uint32_t d) {
		uint32_t n_points = 0;
		uint32_t mem_size_hdr_qy = global_sizeof_header + (d*sizeof(T_Q));
		if(mem_msg_size > mem_size_hdr_qy) {
			n_points = (mem_msg_size - mem_size_hdr_qy)/sizeof(T_P);
		}
		return n_points;
	}

	/**
	 * Get the number of DataPoints' instances which will receive points from the bucket.
	 * It is the first four bytes after the LabelHeader and LabelControl.
	 * @return the number of DataPoints' instances which will receive points from the bucket.
	 */
	uint32_t getNDpInst() const {
		return getLocalHeaderPos(l_header_pos_nDpInst);
	}

	/**
	 * Set the number of DataPoints' instances which will receive points from the bucket.
	 * It is the first four bytes after the LabelHeader and LabelControl.
	 * @param dpInst the number of DataPoints' instances which will receive points from the bucket.
	 */
	void setNDpInst(uint32_t dpInst) {
		setLocalHeaderPos(l_header_pos_nDpInst, dpInst);
	}

	/**
	 * Get the hash table id for this query.
	 * It is the second four bytes after the LabelHeader and LabelControl.
	 * @return the hash table id.
	 */
	uint32_t getHashTableId(const uint32_t T) {
		return getLocalHeaderPos(l_header_pos_probeId)/T;
	}

	/**
	 * Get the probe id for this query.
	 * It is the second four bytes after the LabelHeader and LabelControl.
	 * @return the probe id.
	 */
	uint32_t getProbeId() {
		return getLocalHeaderPos(l_header_pos_probeId);
	}

	/**
	 * Set the probe id for this query.
	 * It is the second four bytes after the LabelHeader and LabelControl.
	 * @param probeid the probe id.
	 */
	void setProbeId(uint32_t htid) {
		setLocalHeaderPos(l_header_pos_probeId, htid);
	}
};

#endif /* LSBUCKETPOINTSMSG_H_ */
