/**
X * @file
 * BucketsIndex.cpp
 *
 *  Created on: Aug 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#include "BucketsIndex.h"

struct threadDataBI{
	int tid;
	void *BucketsIndex;
};

void *callThreadBI(void *arg){
	BucketsIndex *bi = (BucketsIndex *)((threadDataBI*) arg)->BucketsIndex;
	int tid = (int)((threadDataBI*) arg)->tid;
	bi->findAndSendQueryBucket(tid);
	free(arg);
	pthread_exit(NULL);
}

BucketsIndex::BucketsIndex(uint32_t g,
						uint32_t i,
						uint32_t n_f_inst,
						LabelStream * ir2bi,
						LabelStream * qr2bi,
						LabelStream * bi2dp,
						int k,
						int L,
						int m,
						int d,
						int T,
						int fbith) :
						FilterInstance(g, i, n_f_inst),
						ir2bi(ir2bi),
						qr2bi(qr2bi),
						bi2dp(bi2dp),
						k(k),
						L(L),
						m(m),
						d(d),
						T(T)
{
	/* Defines the number of threads to execute findAndSendQueryBucket.
	 * If the number of threads is not defined, it uses one thread per core.*/
	if (fbith == NOT_DEFINED || fbith <= 0) {
		this->numberOfThreads = (int) Util::getNumProcs();
	} else {
		this->numberOfThreads = fbith;
	}

#ifdef DEBUG_P_BIF
	cout << "[BI] Constructor." << endl;
#endif
	cout << "[BI] Using " << this->numberOfThreads << " threads." << endl;

	if (T > 0) {
		multiprobe = true;
		std::cout << "Using multiprobe T = " << T << "." << std::endl;
	} else {
		multiprobe = false;
		std::cout << "Not using multiprobe T " << T << "." << std::endl;
	}

	/* Recv bucket from Input Reader and store them on hashTable. */
	recvBucket();

	pthread_mutex_init(&networkMutex, NULL);

	/* Create the mutex to be used when communication is needed. */
	workerThreads = (pthread_t *) malloc(sizeof(pthread_t) * this->numberOfThreads);
	if(!workerThreads) {
		cerr << "[BucketsIndex][Constructor] ERROR could not allocate workerThreads." << endl;
		exit(EXIT_FAILURE);
	}

	/* Creates each threads with the structure associated. */
	for (int i = 1; i < this->numberOfThreads; i++ ){
		threadDataBI *arg = (threadDataBI *) malloc(sizeof(threadDataBI));
		if(!arg) {
			cerr << "[BucketsIndex][Constructor] ERROR could not allocate args thread." << endl;
			exit(EXIT_FAILURE);
		}
		arg->tid = i;
		arg->BucketsIndex = this;
		int ret = pthread_create(&(workerThreads[arg->tid]), NULL, callThreadBI, (void *)arg);
		if (ret != 0){
			errno = ret;
			perror("[BucketsIndex][Constructor] pthread_create()");
			exit(EXIT_FAILURE);
		}
	}

	/* For each find the databucket and send it to DataPoint to calculate ANN. */
	findAndSendQueryBucket(0);
}

BucketsIndex::~BucketsIndex(){

	pthread_mutex_destroy(&networkMutex);

	free(workerThreads);

#ifdef DEBUG_P_BIF
	cout << "[BI] Destructor." << endl;
#endif
}

/**
 * Receive a point and add it to a bucket, if the bucket does not exist creat a new one.
 */
void BucketsIndex::recvBucket() {
	cout << "[BI][" << getInstanceId() <<"][recvBucket][" << Util::getStringTime() << "] Started." << endl;

	/* String of bucket key. */
    std::string str_bucket_key;

    int reg_id = 0;

    /* Bucket key */
    T_KEY bucket_key = 0;

    /* Chain key */
    T_KEY chain_key = 0;

    /* Hash table id */
    T_HTI htid = 0;

    /* Label used to LabelStream point to DP. */
    T_KEY label_key = 0;

    uint32_t bk_recvd = 0;

    while (1) {
    	MsgBkt * msg = new MsgBkt();

    	if(ir2bi->probeAndReadStream(msg) == EOW) {
    		break;
    	}

    	msg->deserialize();

    	uint32_t npoints = msg->getHeaderControl();

#ifdef DEBUG_P_BIF
		std::cout << "Recved msg np " << npoints << std::endl;
#endif

    	for (uint32_t i = 0; i < npoints; i++) {

    		/* Identifier of the register. */
    		reg_id = msg->getPointId(i);

    		/* Get the chain_key from the message. */
    		chain_key = msg->getChainKey(i);

    		/* Get the bucket_key from the message. */
    		bucket_key = msg->getBucketKey(i);

    		/* Get hash table id from message. */
    		htid = msg->getHashTableId(i);

			/* Get label key from message. */
			label_key = msg->getLabelKey(i);

    		/* Generates the string bucket key using bucket key and hash table id. */
    		str_bucket_key = bktInt2Str(bucket_key, htid);

#ifdef DEBUG_P_BIF
    		std::cout << "[BI] p "<< reg_id << " bucket_key " << bucket_key << " chain_key "<< chain_key << " str bk " << str_bucket_key << std::endl;
#endif

    		/* Add the bucket to the hash. */
    		hashTable.addPoint(str_bucket_key, chain_key, reg_id, label_key);

    		str_bucket_key.clear();

    		bk_recvd++;
    		if(bk_recvd % 300000 == 0){
    			std::cout << "[BI][" << getInstanceId() << "][recvBucket]["  << Util::getStringTime() << "] " <<  bk_recvd << " registers indexed!" << std::endl;
    		}
    	}

    	delete msg;

	}

	cout << "[BI][" << getInstanceId() <<"][recvBucket][" << Util::getStringTime() << "] Finished. Total of " << bk_recvd << " buckets msgs have been received." << endl;
}

/**
 * Receive a query and send the number of points from the bucket that matches the query.
 * The points' id are sent to DataPoints Filter.
 */
void BucketsIndex::findAndSendQueryBucket(int tid) {
	cout << "[BI][" << getInstanceId() <<"][findAndSendQueryBucket][th "<<tid<<"][" << Util::getStringTime() << "]" << " Started." << endl;

	/*
	 * Point's id list for each DataPoint instance,
	 * thus I can send the query along with all bucket points
	 * within the same message.
	 */
	std::list<T_P> points_list[bi2dp->getDestInstSize()];

	std::string str_bucket_key;

    uint64_t q_recvd = 0;
    uint64_t p_sent = 0;

#ifdef LSH_BIF_STATS
    uint64_t msg_sent_dp = 0;
	uint64_t msg_size_dp = 0;
#endif

	uint32_t T_aux = 0;

	/*! When multiprobe is being used the T_aux must equal 1. */
	if(multiprobe) {
		T_aux = T;
	} else {
		T_aux = 1;
	}

	/*! Receive the message with bucket (in integers).*/
	while(1) {
		/*! Message to be received by QueryReceiver instances. */
		MsgQyBkt * msg_in = new MsgQyBkt();

		pthread_mutex_lock(&networkMutex);
		if( qr2bi->probeAndReadStream(msg_in) == EOW ) {
			pthread_mutex_unlock(&networkMutex);
			break;
		}
		pthread_mutex_unlock(&networkMutex);

		if(msg_in == NULL) {
			cerr << "[DP][recvQueryPoints] ERROR msg pointer is null." << endl;
			exit(EXIT_FAILURE);
		}

		msg_in->deserialize(d);

		q_recvd++;
		if(q_recvd % 50000 == 0){
			std::cout << "[BI][" << getInstanceId() << "][findAndSendQueryBucket][th "<<tid<<"] " <<  q_recvd << " queries recvd!" << std::endl;
		}

		/* Pointer to query array in the message. */
		uint8_t * q = msg_in->getQuery();

		/* Identifier of the query. */
		uint32_t qId = msg_in->getQueryId();

		uint32_t nProbes = msg_in->getNProbes();

#ifdef DEBUG_P_BIF
		std::cout << "[BI][th "<<tid<<"] === Recv new message qId "<< qId << " num probes " << nProbes << std::endl;
#endif

		for (uint32_t iProbe = 0; iProbe < nProbes; iProbe++) {

			/* Probe id */
			T_HTI probeId = msg_in->getProbeId(iProbe);

			/* Besides the query id, we get the hash table id that generated this hash key.*/
			T_HTI htid = msg_in->getHashTableId(iProbe, T_aux);

			/* Chain_key */
			T_KEY chain_key = msg_in->getChainKey(iProbe);

			/* Get the bucket from the message. */
			T_KEY bucket_key = msg_in->getBucketKey(iProbe);

			/* Convert bucket_key in integers to string. */
			str_bucket_key = bktInt2Str(bucket_key, htid);

#ifdef DEBUG_P_BIF
			std::cout << "[BI][th "<<tid<<"] iProbe " << iProbe << " qId "<< qId << " ProbeId " << probeId << " htid " << htid << " bucket_key " << bucket_key << " chain_key "<< chain_key << " str bk " << str_bucket_key << std::endl;
#endif

			/* Get the databucket from hashtable. */
			DataBucket<DBPnt> * curDataBucket = hashTable.retrieveDataBucket(str_bucket_key, chain_key);

			if(curDataBucket != NULL) {
#ifdef DEBUG_P_BIF
				cout << "[BI][th "<<tid<<"] Found bucket for qid  " << qId << " bucket size " << curDataBucket->getSize() << endl;
#endif

				/* Check if the bucket has at least one point. */
				if (curDataBucket->getSize() <= 0) {
					cerr << "ERROR DataBucket with no points (size equals 0).  " << endl;
					exit(1);
				}

				/* Number of DataPoints' instances which will receive at least one point from the query's bucket. */
				uint32_t nDpInst = 0;

				/* For each point in bucket add it to its DataPoints instance list. */
				for(size_t i = 0;  i < curDataBucket->getSize(); i++) {
					const DBPnt& dbpnt = curDataBucket->getPoint(i);
					int p = dbpnt.getPointId();

					//int inst_dest = (retrievePointLabel(p) % bi2dp->getDestInstSize());
					int inst_dest = (dbpnt.getLabel() % bi2dp->getDestInstSize());

#ifdef DEBUG_P_BIF
					cout << "[BI][th "<<tid<<"] inst_dest " << inst_dest << " push point " << p << endl;
#endif
					points_list[inst_dest].push_back(p);
				}

				/* Calculate the number of DataPoints instances which will receive the any point. */
				for(int i = 0; i < bi2dp->getDestInstSize(); i++) {
					if(points_list[i].size() > 0) {
						nDpInst++;
					}
				}

				/* Set the messages to send to each DataPoint instance with all buckets point. */
				for(int i = 0; i < bi2dp->getDestInstSize(); i++) {
					uint32_t list_n_p = points_list[i].size();

					/*
					 * Only send a message if there is at least one point
					 * to the current DataPoints' instance.
					 */
					if (list_n_p > 0) {
						/*
						 * Message to be sent to DataPoints instances.
						 * Sent one point per message.
						 */
						MsgBktPt msg_out(d, list_n_p);
#ifdef DEBUG_P_BIF
						cout << "[BI][th "<<tid<<"] n_points "  << list_n_p  << endl;
#endif

						/* Setting up the points to msg out to DataPoints filter. */
						T_P * p_out = msg_out.getPointsId();
						for(uint32_t j = 0; j < list_n_p; j++) {
							p_out[j] = points_list[i].front();
#ifdef DEBUG_P_BIF
							cout << "[BI][th "<<tid<<"] pop p " << j << " = " << p_out[j] << endl;
#endif
							points_list[i].pop_front();
						}

						msg_out.setHeaderLabel(i);
						msg_out.setHeaderControl(qId);
						msg_out.setNDpInst(nDpInst);
						msg_out.setProbeId(probeId);

#ifdef DEBUG_P_BIF
						cout << "[th "<<tid<<"] Msg Debug headerLabel " << msg_out.getHeaderLabel()
									<< " headercontrol(qId) " << msg_out.getHeaderControl()
									<< " ndpinst " << msg_out.getNDpInst()
									<< " probeId " << msg_out.getProbeId() << endl;
#endif

						T_Q * q_out = msg_out.getQuery();
						if(q_out != NULL) {
							memcpy(q_out, q, sizeof(T_Q)*d);
						}

						pthread_mutex_lock(&networkMutex);
						bi2dp->sendBuffer(msg_out);
						pthread_mutex_unlock(&networkMutex);

#ifdef LSH_BIF_STATS
						msg_sent_dp++;
						msg_size_dp += msg_out.getSizeMsg();
#endif

						p_sent++;
						if(p_sent % 50000 == 0){
							std::cout << "[BI][" << getInstanceId() << "][th "<<tid<<"][findAndSendQueryBucket][" << Util::getStringTime() << "] " <<  p_sent << " msgs sent to DP!" << std::endl;
						}

#ifdef DEBUG_P_BIF
						cout << "[BI][th "<<tid<<"] sent msg to inst " << i << endl;
#endif

						points_list[i].clear();
					}
				}
			} else {
				/*
				 * When there is no bucket available, an empty message is sent to DataPoints.
				 * We used to send an empty message to each DataPoints instance and then it was forwarded to Aggregator instance
				 * to know when the query processing is finished. But this approach is cumbersome, so we now send the message to
				 * just one DataPoints instance with the number of DataPoints instance which will receive at least one point.
				 * This information is forwarded to Aggregator which now can know how many messages it must wait to finish the
				 * query processing.
				 */
				MsgBktPt msg_out(0,0);
				msg_out.setHeaderLabel(qId);
				msg_out.setHeaderControl(qId);
				msg_out.setNDpInst(1);
				msg_out.setProbeId(probeId);

				pthread_mutex_lock(&networkMutex);
				bi2dp->sendBuffer(msg_out);
				pthread_mutex_unlock(&networkMutex);

#ifdef LSH_BIF_STATS
				msg_sent_dp++;
				msg_size_dp += msg_out.getSizeMsg();
#endif

#ifdef DEBUG_P_BIF
				cout << "[BI][th "<<tid<<"] qId  " << qId << " ProbeId " << probeId << " hashTableId "<< htid << " sent msg with bucket NULL"<< endl;
#endif
			}
		}
		delete msg_in;
	}

	if(tid == 0) {
		// Join... make sure all other threads have finished
		this->joinThreads();

		/* No more queries, close the stream to DataPoints. */
		bi2dp->closeLabelStream();
	}

#ifdef LSH_BIF_STATS
	pthread_mutex_lock(&networkMutex);
	cout << "[BI][" << getInstanceId() <<"][Stats][th "<<tid<<"] Messages sent to DP " << msg_sent_dp << "; Total bytes sent " << msg_size_dp << "; Average size " << (msg_sent_dp > 0 ? msg_size_dp/msg_sent_dp : 0) << " (bytes/msg)." << endl;
	pthread_mutex_unlock(&networkMutex);
#endif

	cout << "[BI][" << getInstanceId() <<"][findAndSendQueryBucket][th "<<tid<<"][" << Util::getStringTime() << "] end recving queries. Total of " << q_recvd << " of query msgs have been received." << endl;
}


std::string BucketsIndex::bktInt2Str(T_KEY bucket_key, T_HTI hashTableId) {
	return DefaultCalcHashKey::KeyInt2Str(bucket_key, hashTableId);
}

void BucketsIndex::joinThreads() {
#ifdef DEBUG_P_BIF
	cout << "[BI] Start joining threads." << endl;
#endif
	for(int i= 1; i < this->numberOfThreads; i++){
			pthread_join(this->workerThreads[i] , NULL);
#ifdef DEBUG_P_BIF
			cout << "[BI] Thread " << i << " joined." << endl;
#endif
	}
}

