/**
 * @file
 * Aggregator.cpp
 *
 *  Created on: Sep 6, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#include "Aggregator.h"

Aggregator::Aggregator(uint32_t g,
						uint32_t i,
						uint32_t n_f_inst,
						LabelStream * dp2ag,
						FileOut &f_io,
						int L,
						int T,
						int k) :
						FilterInstance(g,i,n_f_inst),
						dp2ag(dp2ag),
						f_io_out(&f_io),
						L(L),
						T(T),
						k(k)
{
	this->initTime = 0;
	f_io_out->printOutputHeader(k);

	if (T > 0) {
		multiprobe = true;
		std::cout << "Using multiprobe T = " << T << "." << std::endl;
	} else {
		multiprobe = false;
		std::cout << "Not using multiprobe T " << T << "." << std::endl;
	}

	recvANN();
}


Aggregator::~Aggregator()
{
	std::cout << "Aggregator exec. time(s) = " << ((float)(Util::ClockGetTime() - this->initTime))/1000 << std::endl;
	std::tr1::unordered_map<uint32_t, QueryANN *>::iterator it = queriesANN_hash.begin();
	while(it != queriesANN_hash.end()){
		delete (*it).second;
		it++;
	}
#ifdef DEBUG_P_AGF
	cout << "[AG] Destructor" << endl;
#endif
}

void Aggregator::recvANN()
{
	cout << "[AG][" << getInstanceId() <<"][recvANN]" << " Started "  << endl;

	int n_p = 0;

	uint32_t T_aux = 0;

	/*! When multiprobe is being used the T_aux must equal 1. */
	if(multiprobe) {
		T_aux = T;
	} else {
		T_aux = 1;
	}

    uint32_t q_recvd = 0;
	uint32_t q_ended = 0;

	while(1) {
		int mem_size_msg;
		MsgAnn * msg_in = new MsgAnn();

		if((mem_size_msg = dp2ag->probeAndReadStream(msg_in)) == EOW) {
			break;
		}

		if(msg_in == NULL) {
			cerr << "[AG][recvANN] ERROR msg pointer is null." << endl;
			exit(EXIT_FAILURE);
		}

		n_p = msg_in->getNumPoints(mem_size_msg);
		msg_in->deserialize(n_p);

		if(this->initTime == 0){
			this->initTime = Util::ClockGetTime();
		}

		int qId = msg_in->getHeaderLabel();

		q_recvd++;
		if(q_recvd % 100000 == 0){
			std::cout << "[AG][" << getInstanceId() << "][recvANN] " <<  q_recvd << " msgs recvd!" << std::endl;
		}

		/* Number of DataPoints instances to Aggregator wait to end query processing. */
		uint32_t nDpInst = msg_in->getNDpInst();

		/* Probe id for this query. */
		uint32_t probeId = msg_in->getProbeId();

#ifdef DEBUG_P_AGF
		cout << "[AG][recvANN] recv msg with qId " << qId << " with " << n_p << " points, nDpInst " << nDpInst << " and probeId " << probeId << "." << endl;
#endif

		QueryANN * curQANN = NULL;

		/* Try to find the query in the hash. */
		std::tr1::unordered_map<uint32_t, QueryANN *>::iterator it = queriesANN_hash.find(qId);

		/* If found, get it, else create a new QueryANN. */
		if(it != queriesANN_hash.end()) {
#ifdef DEBUG_P_AGF
			cout << " Found the query " << qId << "." << endl;
#endif
			curQANN = (*it).second;
		} else {
			curQANN = new QueryANN(qId, this->L*T_aux);
			queriesANN_hash.insert(std::pair<uint32_t, QueryANN *>(qId, curQANN));
#ifdef DEBUG_P_AGF
			cout << " Created a new query " << qId << " in hash." << endl;
#endif
		}

		if(curQANN == NULL) {
			cerr << "[AG][recvANN] Error curQANN NULL. " << endl;
			exit(1);
		}

		/* Increment the number of messages received for query qId. */
		curQANN->incRecvdAnnMsgs();

		/* If it is the first message for this probe will add nDpInst to the number of messages to wait from DataPoints instances. */
		curQANN->add2MaxDpInstMsgs(probeId, nDpInst);

		if(n_p > 0) {
			T_P * pointsId = msg_in->getPointsId();
			if(pointsId == NULL) {
				cerr << "[DP][sendANN2Agg] Error pointsId equals NULL. " << endl;
				exit(1);
			}
			T_D * pointsDist = msg_in->getPointsDist();
			if(pointsDist == NULL) {
				cerr << "[DP][sendANN2Agg] Error pointsDist equals NULL. " << endl;
				exit(1);
			}

			/* Calculates the global ANNPoints using the current message. */
			updateQueryANN(pointsId, pointsDist, *curQANN, n_p, this->k);
		}

		/*
		 * If received query's Ann from all DataPoints' instances,
		 * it is ready to finish and print the query.
		 * Then, the current QueryANN is deleted.
		 */
		if(curQANN->queryFinished()) {
#ifdef DEBUG_P_AGF
			cout << "Query " << qId << " finalizou e vai imprimir." << endl;
#endif
			printANN(curQANN->getAnn(), qId);

			q_ended++;
			if(q_ended % 25000 == 0){
				std::cout << "[AG][" << getInstanceId() << "][recvANN][" << Util::getStringTime() << "]  " <<  q_ended << " queries ended!" << std::endl;
			}

			/* Needs to remove the iterator from map. */
			queriesANN_hash.erase(qId);
			delete curQANN;
		}

		delete msg_in;
	}

	cout << "[AG][" << getInstanceId() <<"][recvANN]" << " Finished. Total of " << q_recvd << " queries messages have been received and " << q_ended << " queries have been ended." << endl;
}

//TODO generalize the Operator::calculateANN to have an interface similar to this one, then we can have one code for sequential and parallel versions.
void Aggregator::updateQueryANN(T_P* pId, T_D* pDist,
		QueryANN& curQANN, const uint32_t num_points, const uint32_t k) const {

	std::vector<ANNPoint>& ann = curQANN.getAnn();

#ifdef DEBUG_P_AGF
	cout << "[Ag][updateQueryANN] Vai atualizar " << num_points << " pontos." << endl;
#endif

	for(uint32_t i = 0; i < num_points; i++) {
		ANNPoint point;
		point.setId(pId[i]);
		point.setDistance(pDist[i]);

#ifdef DEBUG_P_AGF
		cout << "Updating " << pId[i] << " dist " << pDist[i] << "." << endl;
#endif

		std::vector<ANNPoint>::iterator it = ann.begin();

		if(it==ann.end()){
			ann.push_back(point);
		}else{
			bool insert=true;

			// Find the place to insert point
			while(it != ann.end()){
				// Avoid the same point from being inserted due different hash functions
				if((*it).getId() == point.getId()){
					insert=false;
				}
				// If found the place to insert point
				if((*it).getDistance() > point.getDistance() ){
					break;
				}

				it++;
			}
			if(insert)
				ann.insert(it, point);
		}
		if(ann.size() > k){
			ann.pop_back();
		}
	}
}

void Aggregator::printANN(std::vector<ANNPoint>& ann, const uint32_t qId) {
	f_io_out->printANN(ann, qId);
}

