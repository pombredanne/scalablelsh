/**
 * @file
 * Aggregator.h
 *
 *  Created on: Sep 6, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef AGGREGATOR_H_
#define AGGREGATOR_H_

#include <vector>
#include <tr1/unordered_map>

#include <FileIO.h>
#include <ANNPoint.h>

#include "FilterInstance.h"
#include "LSAnnMsg.h"
#include "Util.h"

//#ifndef DEBUG_P_AGF
//#define DEBUG_P_AGF
//#endif

/**
 * \class Aggregator
 * \brief Receives the ANN from each
 * DataPoints instance and selects the k global ANNs.
 *
 * The queries are distributed among the instances, which
 * waits for all "local" ANNs from DataPoints' instances
 * until calculate the global ANN to print
 * the result for each query.
 */
class Aggregator: public FilterInstance {
private:

    /**
     * \class QueryANN
     * \brief Class to store the queries' ANN
     * until all messages from DataPoints are received
     * and the global ANN
     * is calculated.
     *
     * For each hash table there is a number of messager to arrive from DataPoints.
	 * It is needed to add these numbers to know the maximum number of messages to wait until
	 * finishes the query processing.
     *
     */
	class QueryANN {
	private:
		/*! Number of probes. */
		int nProbes;
		/*! Unique query identifier. */
		uint32_t qId;
		/*! Current Anns of qId. */
		std::vector<ANNPoint>  ann;
		/*! Number of received messages with Knn from DataPoints instances. */
		uint32_t recvdAnnMsgs;
		/*!
		 * Number of messages which must receive from DataPoints to calculate global Ann for this query.
		 * Comprises the number of messages for all hash tables.
		 */
		uint32_t nMaxDpInstMsgs;
		/*!
		 * Control for each query to verify if the nDpInstMsg for each hash table has arrived.
		 * They are summed up in the nMaxDpInstMsgs variable. One position for each hash table,
		 * there are L hash tables. For example, if the first nDpInstMsg for hash table i has arrived
		 * then the recvdHTnmsgs[i] is set to true, this number is summed up in the nMaxDpInstMsgs
		 * and any further message arrived will be discarded.
		 */
		bool * recvdHTnmsgs;

		QueryANN();
	public:
		/**
		 * QueryANN constructor.
		 * \param qId Unique query identifier.
		 */
		QueryANN(uint32_t qId, int nProbes) :
			nProbes(nProbes), qId(qId), recvdAnnMsgs(0), nMaxDpInstMsgs(0)
		{
			recvdHTnmsgs = new bool[this->nProbes];
			for(int i=0; i < this->nProbes; i++) {
				recvdHTnmsgs[i] = false;
			}
		}

		/**
		 * QueryANN destructor.
		 */
		~QueryANN(){
			delete [] recvdHTnmsgs;
		}

		/**
		 * Define a vector with ANN.
		 * \param ann Vector with ANN.
		 */
		void setAnn(const std::vector<ANNPoint>& ann) {
			this->ann = ann;
		}

        /**
         * Get a reference with current ANN.
         * \return reference of current ANN.
         */
		std::vector<ANNPoint>& getAnn() {
			return ann;
		}

		/**
		 * Get query's identifier.
		 * @return query's identifier.
		 */
		uint32_t getId() const {
			return qId;
		}

		/**
		 * Increment the number of messages received.
		 * \return incremented number of messages received
		 * for the query.
		 */
		uint32_t incRecvdAnnMsgs() {
			return ++recvdAnnMsgs;
		}

		/**
		 * Get the number of messages received for this query.
		 * @return number of messages received with the ANN for
		 * this query.
		 */
		uint32_t getRecvdAnnMsgs() const {
			return recvdAnnMsgs;
		}

		/**
		 * For each probe there is a number of messages (at least T*L) to arrive from DataPoints.
		 * This method add these numbers to know the maximum number of messages to wait until
		 * finishes the query processing.
		 * Only add each hash table max number of messages once.
		 * @param probeId Probe identifier [0..(L*T)]
		 * @param nDpInst Number of messages to wait for this hash table id from DataPoints instances.
		 */
		void add2MaxDpInstMsgs(uint32_t probeId, uint32_t nDpInst) {
			if(probeId >= (uint32_t) this->nProbes) {
				cerr << "[QueryANN][add2MaxDpInstMsgs] ERROR probe id with invalid value " << probeId << " must be between 0 and" << nProbes << endl;
				exit(1);
			}
			/** Only add if it is the first message to arrive for each hash table. */
			if(recvdHTnmsgs[probeId] == false) {
				this->nMaxDpInstMsgs += nDpInst;
#ifdef DEBUG_P_AGF
				cout << "[AG][QueryANN][add2MaxDpInstMsgs] adding Max DP probeId " << probeId << " nDpInst " << nDpInst << " total " << this->nMaxDpInstMsgs << "." << endl;
#endif
				recvdHTnmsgs[probeId] = true;
			}
		}

		/**
		 * Check if all messages has been received from DataPoints instances
		 * to calculate the global ANN and finish the processing of this query.
		 * @return true if it has received all messages or false otherwise.
		 */
		bool queryFinished(){
			/* Verify if received at least one message for each hash table for this query	. */
			for (uint32_t i = 0; i < (uint32_t) this->nProbes; i++) {
				if(recvdHTnmsgs[i] == false) {
					return false;
				}
			}
			return this->recvdAnnMsgs == this->nMaxDpInstMsgs;
		}

	};

	/*! Stream with knn points from DataPoints. */
	LabelStream * dp2ag;

	/*!  Output file class. */
	FileOut * f_io_out;

	/*!  Structure that holds each incoming query ANNs to calculates global ANN. */
	std::tr1::unordered_map<uint32_t, QueryANN *> queriesANN_hash;

	int L, /*!  Number of hash tables created by the application. */
		T, /*!  Number of probes per hashtable. */
	    k; /*!  Number of neighbors to be returned. */

	bool multiprobe;

	/*!  Receive all Ann from DataPoints and print the result. */
	void recvANN();
	/*!  Print the Anns received. */
	void printANN( std::vector<ANNPoint>& ann, const uint32_t qId);

	void updateQueryANN(T_P * pId, T_D * pDist, QueryANN & curQANN, const uint32_t num_points, const uint32_t k) const;

	Aggregator();
public:
	long long initTime;

    /**
     * Aggregator constructor.
     * \param g MPI global process rank.
     * \param i Instance id among only the same instances, zero-based.
     * \param n_f_inst Number of instances in the same filter.
     * \param dp2ag LabelStream between DataPoints' instances and Aggregator's instances.
     * \param f_out Output stream to print the results.
     * \param L Number of hash tables.
     * \param k Maximum number of neighbors returned.
     */
	Aggregator(uint32_t g,
			uint32_t i,
			uint32_t n_f_inst,
			LabelStream * dp2ag,
			FileOut &f_out,
			int L,
			int T,
			int k);
    /**
     * Aggregator destructor.
     */
	virtual ~Aggregator();
};

#endif /* AGGREGATOR_H_ */
