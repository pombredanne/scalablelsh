/*
 * QueryReceiver.cpp
 *
 *  Created on: Sep 6, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#include "QueryReceiver.h"

QueryReceiver::QueryReceiver(uint32_t g,
							uint32_t i,
							uint32_t n_f_inst,
							LabelStream * qr2bi,
							FileIn &f_io_in,
							int L,
							int m,
							int dimensions,
							int w,
							int T,
							float R,
							unsigned queryRate) :
							FilterInstance(g, i, n_f_inst),
							qr2bi(qr2bi),
							L(L),
							m(m),
							d(dimensions),
							w(w),
							T(T),
							R(R),
							queryRate(queryRate)
{
	/*
	 * The same seed is used in InputReader and QueryReceiver.
	 * Therefore, the pseudo-random generation of the hash tables
	 * are made in the same order for both filters, and
	 * they have the same hash tables with the same hash functions.
	 */
	srandom(DEFAULT_INITIAL_SEED);
	f_io = &f_io_in;

	this->hashTableSize =	 f_io_in.getNumberOfRegisters();
	if(hashTableSize == 0) {
		cerr << "[QueryReceiver][Constructor] WARNING Size of hash table is 0 (maybe because the base number of register is absent). Returning..." << endl;
		qr2bi->closeLabelStream();
		return;
	}

	for(int i = 0; i < L*m; i++){
		hashFunctions.push_back(PStableHash(dimensions, w));
	}

	for(int i = 0; i < m; i++){
		this->mainHash.push_back(DefaultCalcHashKey::genRandomUns32(1, DefaultCalcHashKey::MAX_HASH_RND));
	}
	for(int i = 0; i < m; i++){
		this->controlHash.push_back(DefaultCalcHashKey::genRandomUns32(1, DefaultCalcHashKey::MAX_HASH_RND));
	}

	if (T > 0) {
		multiprobe = true;
		std::cout << "Using multiprobe T = " << T << "." << std::endl;
	} else {
		multiprobe = false;
		std::cout << "Not using multiprobe T " << T << "." << std::endl;
	}

	/* NOT using chain_key. */
	this->useChainKey = false;

	if( ! useChainKey ) {
		std::cout << "** NOT using chain_key. **" << std::endl;
	}
	readAndSendQuery();
}

int QueryReceiver::createInstQueryId(const int inst_id, const int query_id) const {
	stringstream ss;
	ss << inst_id << query_id;
	return atoi(ss.str().c_str());
}

QueryReceiver::~QueryReceiver() {
#ifdef DEBUG_P_QRF
	cout << "[QR] Destructor." << endl;
#endif
}

/*!
 * Read the query file and send to BucketIndex instance.
 */
void QueryReceiver::readAndSendQuery() {
	std::cout << "[QR]["<< getInstanceId() <<"][readAndSendQuery][" << Util::getStringTime() << "] Started." << std::endl;

	std::vector<uint8_t> point;

	uint32_t query_id = 0;

	uint64_t query_read = 0;

	uint32_t T_aux = 0;

	/*! When multiprobe is being used the T_aux must equal 1. */
	if(multiprobe) {
		T_aux = T;
	} else {
		T_aux = 1;
	}

	/*! Bucket_Key */
	std::vector< std::vector<T_KEY> > bucket_keys(L, std::vector<T_KEY>(T_aux,0));

	/*! Chain key */
	std::vector<T_KEY> chain_key(L, 0);

	/*! List of BI instances with the list of Ls for each query. */
	std::vector< std::list<uint32_t> > bi_inst_list(qr2bi->getDestInstSize(), std::list<uint32_t>());

	/*! To know which BI instance will be used. */
	std::vector<uint32_t> bi_dest_vec(L*T_aux, 0);

	unsigned long sleepTime = (1000000)/queryRate;

	while(1){

		point.clear();
		query_id = f_io->readQueryPoint(point);
		if (point.size() == 0) {
			break;
		}

		/*!
		 * To have multiple instances of QueryReceiver each query must have its unique
		 * identifier. Thus, the query id from now on will be instance's id + query id.
		 */
		// DEPRECATED int inst_query_id = createInstQueryId(getInstanceId(), query_id);


#ifdef DEBUG_P_QRF
		cout << "[QR] Hashing q " << query_id << std::endl;
#endif
		/*! Send the bucket_key to BucketIndex instance. */
		if (multiprobe) {

#ifdef DEBUG_P_QRF
			std::cout << "Multiprobe :" << std::endl;
#endif

			for(int iL = 0; iL < L; iL++){

				/*! Generate the probe buckets for each L hashtable. */
				calcHashKeyMP(point, iL, bucket_keys[iL]);

				/*! Calculate the receiver BucketsIndex instance for each probe e put them in the same list to be sent coalesced over the network. */
				for(uint32_t iT = 0; iT < bucket_keys[iL].size(); iT++) {

					/* The probe indices in bi_list are linearized by the number of hashtables and the number of probes.*/
					uint32_t ind = iL*T+iT;

					uint32_t bi_inst = bucket_keys[iL][iT] % qr2bi->getDestInstSize();

					bi_inst_list[bi_inst].push_back(ind);

					bi_dest_vec[ind] = bi_inst;

#ifdef DEBUG_P_QRF
					cout << "[QR] htid " <<  iL << " instance id " << bi_inst << " Probe id " << ind << " bucket_key " << bucket_keys[iL][iT] <<  " chain_key " << chain_key[iL] << std::endl;
#endif
				}
			}

#ifdef DEBUG_P_QRF
			std::cout << "Fim" << std::endl;
#endif
		} else {
			for(int iL = 0; iL < L; iL++){

				calcHashKey(point, iL, bucket_keys[iL][0], chain_key[iL]);

				/* Chain_key is ignored when using multiprobe. */
				if( ! useChainKey ) {
					chain_key[iL] = 0;
				}

				uint32_t bi_inst = bucket_keys[iL][0] % qr2bi->getDestInstSize();

				bi_inst_list[bi_inst].push_back(iL);

				bi_dest_vec[iL] = bi_inst;

	#ifdef DEBUG_P_QRF
				cout << "[QR] htid " <<  iL << " instance id " << bi_inst  << " bucket_key " << bucket_keys[iL][0] <<  " chain_key " << chain_key[iL] << std::endl;
	#endif
			}
		}

		/*! Coalesce message with query's probes (L*T) for the same BucketsIndex. */
		for(uint32_t iProbe = 0; iProbe < (uint32_t) L*T_aux; iProbe++ ) {

			/*! Get the BI instance receiver. */
			uint32_t bi_dest = bi_dest_vec[iProbe];

			/*! Get the number of Probes this bi_inst will receive. */
			uint32_t list_size =  bi_inst_list[bi_dest].size();

			/*! If there is no Probe to this BI instance forget it. */
			if(list_size > 0) {

#ifdef DEBUG_P_QRF
				cout << "[QR] Creating msg q " << query_id << std::endl;
#endif

				/*!
				 * Message to send BucketsIndex with bucket key, chain key, hash table id and query elements.
				 */
				MsgQyBkt msg_bi(d, list_size);
				msg_bi.setHeaderLabel(bi_dest);
				msg_bi.setHeaderControl(0);
				msg_bi.setQueryId(query_id);

				/*! Get the query's values for each Probe on the list of probes. */
				for(uint32_t ii = 0; ii < list_size; ii++) {

					/*! The probe indices in bi_list are linearized by the maximum number of hash tables (L) and the maximum number of probes per hash table (T).*/
					uint32_t ind = bi_inst_list[bi_dest].front();
					bi_inst_list[bi_dest].pop_front();

					/*! Get the Probe's hashtable id L.*/
					uint32_t tmp_iL = ind/T_aux;
					/*! Get the Probe's value in the list of its L.*/
					uint32_t tmp_iT = ind%T_aux;

					msg_bi.setBucketKey(ii, bucket_keys[tmp_iL][tmp_iT]);
					msg_bi.setChainKey(ii, chain_key[tmp_iL]);
					msg_bi.setProbeId(ii, ind);
					msg_bi.incNProbes();
#ifdef DEBUG_P_QRF
					cout << "[QR] Probe id " << msg_bi.getProbeId(ii) << " bucket_key " << msg_bi.getBucketKey(ii) << " chain_key " << msg_bi.getChainKey(ii) << " num probes " << msg_bi.getNProbes()  << std::endl;
#endif
				}

				/*! Must clean the list to not send the message again to the same BI instance.*/
				bi_inst_list[bi_dest].clear();

				T_Q * q = msg_bi.getQuery();
				if(q != NULL) {
					memcpy(q, &point[0], sizeof(T_Q)*d);
				}

#ifdef DEBUG_P_QRF
				cout << "[QR] Sending q " << query_id << " to " << bi_dest << " with " << list_size << " probes." << endl;
#endif

				qr2bi->sendBuffer(msg_bi);
			}
		}

		query_read++;
		if(query_read % 100000 == 0){
			std::cout << "[QR]["<< getInstanceId() <<"][readAndSendQuery][" << Util::getStringTime() << "] " << query_read << " queries read (id " << query_id << ")!" << std::endl;
		}

		usleep(sleepTime);
	}
	qr2bi->closeLabelStream();

	std::cout << "[QR]["<< getInstanceId() <<"][readAndSendQuery][" << Util::getStringTime() << "] Finished. Total of " << query_read << " queries have been read." << std::endl;
}

/*!
 * Returns the hashkey in two formats: integer array and a string.
 */
void QueryReceiver::calcHashKey(const std::vector<uint8_t> &point,
									const int hashTableId,
									T_KEY& bucket_key,
									T_KEY& chain_key)
{
	DefaultCalcHashKey::calcHashKey(point,
									hashTableId,
									hashFunctions,
									m,
									hashTableSize,
									R,
									mainHash,
									controlHash,
									bucket_key,
									chain_key);
}

void QueryReceiver::calcHashKeyMP(const std::vector<uint8_t> &point,
									const int  hashTableId,
									std::vector<T_KEY> &seq)
{
	MultiProbeLsh::genProbeSequence(point,
								    hashTableId,
								    this->hashFunctions,
								    this->m,
								    this->hashTableSize,
								    this->R,
								    this->mainHash,
								    this->T,
								    seq);
}
