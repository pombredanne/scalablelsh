/*
 * DataPoints.h
 *
 *  Created on: Sep 4, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef DATAPOINTS_H_
#define DATAPOINTS_H_

#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <Util.h>
#include <PointsTable.h>
#include <DataBucket.h>
#include <Operator.h>
#include <DBPnt.h>

#include "FilterInstance.h"
#include "LSDataPointMsg.h"
#include "LSBucketPointsMsg.h"
#include "LSAnnMsg.h"

/*
#ifndef DEBUG_P_DPF
#define DEBUG_P_DPF
#endif
*/


/**
 *  \class DataPoints
 *  \brief Implements the DataPoints instance.
 *
 *  Stores the input points in PointsTable. Receives messages with input
 *  points from InputReader. It also receives the points' id within the same
 *  query's bucket, which after are used to calculate the ANN. These ANN are then
 *  sent to Aggregator.
 */
class DataPoints: public FilterInstance {
private:

	std::vector<uint32_t> translateG2LIndex_vec;
	std::vector<uint32_t> translateL2GIndex_vec;


	/*!< ADT which contains the points from input. */
	PointsTable pointsTable;

	/*!< Stream with points sent from InputReader. */
	LabelStream * ir2dp;

	/*!< Stream with points' id matched within the same bucket from BucketsIndex. */
	LabelStream * bi2dp;

	/*!< Stream with knn points to Aggregator. */
	LabelStream * dp2ag;

	int L, /*!< Number of hash tables created by the application. */
	    m, /*!< Number of hash functions for each hash table. */
	    d, /*!< Number of dimensions in each point. */
	    k; /*!< Number of neighbors to be returned. */

	// structure containing information about the threads
	pthread_t *workerThreads;
	int numberOfThreads;

	pthread_mutex_t networkMutex;

	void joinThreads();

	void recvInputPoints();

	uint64_t sendANN2Agg(const std::vector<ANNPoint> &curAnn, const int qId, const uint32_t nDpInst, const uint32_t htid);
	uint64_t sendEmtpyMsg2Agg(const int qId, const uint32_t nDpInst, const uint32_t htid);
	void createTranslationDB(uint32_t global_id);
	uint32_t translateG2LPIndex(uint32_t global_id);
	uint32_t translateL2GPIndex(uint32_t local_id);


	DataPoints();
public:
	/**
	 * DataPoints constructor.
	 * \param g MPI global process rank.
	 * \param i Instance id among only the same instances, zero-based.
	 * \param n_f_inst Number of instances in the same filter.
	 * \param in2dp LabelStream between InputReader's instances and DataPoints' instances.
	 * \param bi2dp LabelStream between BucketsIndex's instances and DataPoints' instances.
	 * \param dp2ag LabelStream between DataPoints' instances and Aggregator's instances.
	 * \param L Number of hash tables.
	 * \param m Number of hash functions for each hash table.
	 * \param d Number of points' dimensions.
	 * \param k Maximum number of neighbors returned.
	 */
	DataPoints(uint32_t g,
				uint32_t i,
				uint32_t n_f_inst,
				LabelStream * in2dp,
				LabelStream * bi2dp,
				LabelStream * dp2ag,
				int L,
				int m,
				int d,
				int k,
				int fdpth);

	/**
	 * DataPoints destructor.
	 *
	 */
	~DataPoints();

	void recvQueryPoints(int tid);
};

void DataPoints::joinThreads() {
	for(int i= 1; i < this->numberOfThreads; i++){
			pthread_join(this->workerThreads[i] , NULL);
	}
}

#endif /* DATAPOINTS_H_ */
