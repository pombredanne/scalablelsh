/*
 * LabelStream.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: thiago
 */

#include "LabelStream.h"


/**
 * Send buffer message.
 * @param msg Pointer to message buffer.
 * @param size_msg Size of message in bytes.
 */
void LabelStream::send(void * msg, uint32_t size_msg) {
#ifdef DEBUG_P_LS
	cout << "[LS][send] to " << labelFunction(msg, size_msg) << " size " << size_msg << endl;
#endif
	Stream::send(msg, size_msg, labelFunction(msg, size_msg));
}

void LabelStream::send(void * msg, uint32_t size_msg, int dest, int tag) {
#ifdef DEBUG_P_LS
	cout << "[LS][send] to " << dest << " tag " << tag << endl;
#endif
	Stream::send(msg, size_msg, dest, tag);
}

/**
 * Receive buffer method.
 * @param msg Pointer to message buffer.
 * @param size_msg Size of message in bytes.
 * @param source source id filter.
 */
void LabelStream::recv(void * msg, uint32_t size_msg, int source) {
	Stream::recv(msg, size_msg, source);
}

void LabelStream::recv(void * msg, uint32_t size_msg, int source, int tag) {
	Stream::recv(msg, size_msg, source, tag);
}

/**
 * Check if there is any message to receive.
 * @return Size of message to be received in bytes.
 */
int LabelStream::probe(int &source) {
	int mem_size_msg = -1;
	/* TODO Only check the instances source with stream still opened. */
	mem_size_msg = Stream::probe(MPI_ANY_SOURCE, source);
	return mem_size_msg;
}

int LabelStream::probe(int &source, int tag) {
	int mem_size_msg = -1;
	/* TODO Only check the instances source with stream still opened. */
	mem_size_msg = Stream::probe(MPI_ANY_SOURCE, source, tag);
	return mem_size_msg;
}

/**
 *
 * @param source
 * @return
 */
int LabelStream::iprobe(int &source) {
	int mem_size_msg = -1;
	/* TODO Only check the instances source with stream still opened. */
	mem_size_msg = Stream::iprobe(MPI_ANY_SOURCE, source);
	return mem_size_msg;
}

int LabelStream::iprobe(int &source, int tag) {
	int mem_size_msg = -1, out_tag;
	/* TODO Only check the instances source with stream still opened. */
	mem_size_msg = Stream::iprobe(MPI_ANY_SOURCE, source, tag, out_tag);
	return mem_size_msg;
}


int LabelStream::iprobe(int &source, int tag, int &out_tag) {
	int mem_size_msg = -1;
	/* TODO Only check the instances source with stream still opened. */
	mem_size_msg = Stream::iprobe(MPI_ANY_SOURCE, source, tag, out_tag);
	return mem_size_msg;
}


/**
 *
 * @return
 */
LabelStream::InstType LabelStream::getInstType() const {
	return type;
}

/** Close one more source instance stream. */
void LabelStream::closeSourceInstLS() {
	sourceInst_open--;
}

/**
 * Get the number of source filter instances with stream open .
 * \return the number of source filter instances with stream open .
 */
int32_t LabelStream::getActiveSourceInst() const {
	return sourceInst_open;
}

/**
 *
 * @return
 */
int32_t LabelStream::getDestInstInitRank() const {
	return destInst_init_rank;
}

/**
 * Get the number of destination instances.
 * @return the number of destination instances.
 */
int32_t LabelStream::getDestInstSize() const {
	return destInst_size;
}

/**
 * Get the initial rank of source instances.
 * @return the initial rank of source instances.
 */
int32_t LabelStream::getSourceInstInitRank() const {
	return sourceInst_init_rank;
}

/**
 * Get Number of source instances.
 * @return number of source instances.
 */
int32_t LabelStream::getSourceInstSize() const {
	return sourceInst_size;
}

/**
 * LabelStream constructor.
 * @param oF_i_r Initial rank of source filter instances.
 * @param oF_n Number of source filter instances.
 * @param dF_i_r Initial rank of destination filter instances.
 * @param dF_n Number of destination filter instances.
 * @param tag LabelStream unique identifier.
 */
LabelStream::LabelStream(int32_t oF_i_r,
		int32_t oF_n,
		int32_t dF_i_r,
		int32_t dF_n,
		int tag) : Stream(tag),
				sourceInst_open(oF_n),
				sourceInst_init_rank(oF_i_r),
				sourceInst_size(oF_n),
				destInst_init_rank(dF_i_r),
				destInst_size(dF_n)
{
	if(sourceInst_init_rank == LabelStream::LS_NO_SOURCE ||
		sourceInst_size == LabelStream::LS_NO_SOURCE) {
		type = SOURCE;
	} else if (destInst_init_rank == LabelStream::LS_NO_DEST ||
	   destInst_size == LabelStream::LS_NO_DEST) {
		type = DESTINATION;
	}

}

/**
 * LabelStream destructor.
 */
LabelStream::~LabelStream()
{}


/**
 * Read buffer with previously allocated message.
 * Handle the EOWs received from source filter's instances.
 * If the message is an EOW, counts it and waits the next message.
 * \param b Message buffer.
 * \return EOW or ORDINARY_MSG.
 */
int LabelStream::readBuffer(MsgLS * b, const int source) {
	int ret = ORDINARY_MSG;

	/* Check if the stream is already closed. */
	if(this->getActiveSourceInst() == 0) {
		return EOW;
	}

	while(1) {
#ifdef DEBUG_P_LS
		cout << "[LS][readBuffer] b.getSizeMsg " << b->getSizeMsg() << endl;
#endif
		LabelStream::recv((void *) b->getMsg() , b->getSizeMsg(), source);
		if(b->isEOWmsg()) {
			//			cout << " Recv antes " << closed_origF << endl;
			closeSourceInstLS();
#ifdef DEBUG_P_LS
			cout << "[LS][readBuffer] Recv msg closeLabelStream  left " << getActiveSourceInst() << endl;
#endif
			if(getActiveSourceInst() == 0) {
				ret = EOW;
				break;
			}
		} else {
			//				cout << "Recvd " << b.getHeaderControl() << "." << endl;
			ret = ORDINARY_MSG;
			break;
		}
		/* Sleep some time until check all instances again. */
		//usleep(USLEEP_TIME);
	}

	return ret;
}


/**
 * Send buffer message.
 * @param b MsgAnn object.
 */
void LabelStream::sendBuffer(MsgLS& b) {
	LabelStream::send((void *) b.getMsg(), b.getSizeMsg());
}

/**
 * Probe and receive messages. Only returns if a message is received.
 * Will allocate the message according to size probed.
 * @param msg Pointer to message buffer.
 * @return size of the message received or EOW.
 */
//TODO Improve this implementation making it use the int probeAndReadStream(MsgLS& m, const int l_tag).
int LabelStream::probeAndReadStream(MsgLS * m) {
	int ret = ORDINARY_MSG, mem_size_msg, src;

	while(1) {
		/* Check if the stream is already closed. */
		if(this->getActiveSourceInst() == 0) {
			ret = EOW;
			break;
		}

		mem_size_msg = iprobe(src);

		/* Check  if received a message, otherwise wait and then test again. */
		if(mem_size_msg > 0) {
			/* Control messages are checked here and do not return to the user. */
			if ((unsigned int) mem_size_msg > MsgLS::getSizeofHeader()) {
				m->allocMsgLS(mem_size_msg);
				LabelStream::recv((void *) m->getMsg(), m->getSizeMsg(), src);
				ret = (int) mem_size_msg;
				break;
			} else {
				/* Receive the control message and take the necessary actions. */
				MsgLS msg(0);
				LabelStream::recv((void *) msg.getMsg(), msg.getSizeMsg(), src);
				if(msg.isEOWmsg()) {
					closeSourceInstLS();
#ifdef DEBUG_P_LS
					cout << "[LS][probeAndReadStream] Recv msg closeLabelStream  left " << getActiveSourceInst() << endl;
#endif
				} else {
					cerr << "[LS][probeAndReadStream] unknown message type." << endl;
				}
			}
		} else {
			//usleep(USLEEP_TIME);
		}
	}

	return ret;
}

/**
 * Probe and receive messages with an specific tag. Only returns if a message is received.
 * Will allocate the message according to size probed.
 *
 * @param m Pointer to message buffer.
 * @param l_tag Tag to check the message.
 * @return size of the message received or EOW.
 */
int LabelStream::probeAndReadStream(MsgLS& m, const int l_tag) {
	int ret = ORDINARY_MSG, mem_size_msg, src;

	while(1) {

		mem_size_msg = iprobe(src, l_tag);

		/* Check  if received a message, otherwise wait and then test again. */
		if(mem_size_msg > 0) {
			m.allocMsgLS(mem_size_msg);
			LabelStream::recv((void *) m.getMsg(), m.getSizeMsg(), src, l_tag);
			ret = (int) mem_size_msg;
			break;
		} else {
			//usleep(USLEEP_TIME);
		}
	}

	return ret;
}


/**
 * Checks whether the stream is closed, if not check if there is any message to receive.
 * @param src Returns by reference the sender of the message.
 * @return EOW if the stream is closed or the sized of the message to be received.
 */
int LabelStream::probeStream(int &src) {
	int ret, mem_size_msg = 0;

#ifdef DEBUG_P_LS
	cout << "[LS][probeStream] Started " << endl;
#endif

	while(1) {
		/* Check if the stream is already closed. */
		if(this->getActiveSourceInst() == 0) {
			ret = EOW;
			break;
		}

		mem_size_msg = iprobe(src);

		/* Check  if received a message, otherwise wait and then test again. */
		if(mem_size_msg > 0) {
			/* Control messages are checked here and do not return to the user. */
			if ((unsigned int) mem_size_msg > MsgLS::getSizeofHeader()) {
				ret = (int) mem_size_msg;
				break;
			} else {
				/* Receive the control message and take the necessary actions. */
				MsgLS msg(0);
				LabelStream::recv((void *) msg.getMsg(), msg.getSizeMsg(), src);
				if(msg.isEOWmsg()) {
					closeSourceInstLS();
#ifdef DEBUG_P_LS
					cout << "[LS][readBuffer] Recv msg closeLabelStream  left " << getActiveSourceInst() << endl;
#endif
				} else {
					cerr << "[LS][probeStream] unknown message type." << endl;
				}
			}
		} else {
			//usleep(USLEEP_TIME);
		}
	}


#ifdef DEBUG_P_LS
	cout << "[LS][probeStream] Finished " << endl;
#endif

	return ret;
}


//	/** Function which returns the label to route it. */
//	virtual T_LS labelFunction(void * msg, uint32_t size_label) const = 0;
T_LS LabelStream::labelFunction(void * msg, uint32_t size_label) const {
	T_LS dest_filter = ((T_LS *) msg)[0];
	dest_filter %= getDestInstSize();
	dest_filter += getDestInstInitRank();

	return dest_filter;
}

/**
 * Close the stream by sending EOW to all destination instances.
 */
void LabelStream::closeLabelStream() {
	MsgLS b(0);

	/* Send the message to all destination filters of this stream. */
	for (int i = 0; i < destInst_size; i++) {
		b.setHeaderLabel(i);
		b.setMsgEOW();
		LabelStream::send((void *) b.getMsg(), b.getSizeMsg());
#ifdef DEBUG_P_LS
		cout << "Sent msg closeLabelStream to instance " << b.getHeaderLabel() <<  endl;
#endif
	}
}
