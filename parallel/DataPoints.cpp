/**
 * @file
 * DataPoints.cpp
 *
 *  Created on: Sep 4, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#include "DataPoints.h"

struct threadData{
	int tid;
	void *DataPoints;
};

void *callThread(void *arg){
	DataPoints *dp = (DataPoints *)((threadData*) arg)->DataPoints;
	int tid = (int)((threadData*) arg)->tid;
	dp->recvQueryPoints(tid);
	free(arg);
	pthread_exit(NULL);
}

DataPoints::DataPoints(uint32_t g,
						uint32_t i,
						uint32_t n_f_inst,
						LabelStream * ir2dp,
						LabelStream * bi2dp,
						LabelStream * dp2ag,
						int L,
						int m,
						int d,
						int k,
						int fdpth) : FilterInstance(g, i, n_f_inst),
								ir2dp(ir2dp),
								bi2dp(bi2dp),
								dp2ag(dp2ag),
								L(L),
								m(m),
								d(d),
								k(k)
{
	/* If the number of threads is not defined, it uses one thread per core.*/
	if (fdpth == NOT_DEFINED || fdpth <= 0) {
		this->numberOfThreads = (int) Util::getNumProcs();
	} else {
		this->numberOfThreads = fdpth;
	}

#ifdef DEBUG_P_DPF
	cout << "[DP] Constructor." << endl;
#endif
	cout << "[DP] Using " << this->numberOfThreads << " threads." << endl;

	/* Recv input points. */
	recvInputPoints();

	pthread_mutex_init(&networkMutex, NULL);

	workerThreads = (pthread_t *) malloc(sizeof(pthread_t) * this->numberOfThreads);
	if(!workerThreads) {
		cerr << "[DataPoints][Constructor] ERROR could not allocate workerThreads." << endl;
		exit(EXIT_FAILURE);
	}

	for (int i = 1; i < this->numberOfThreads; i++ ){
		threadData *arg = (threadData *) malloc(sizeof(threadData));
		if(!arg) {
			cerr << "[DataPoints][Constructor] ERROR could not allocate args thread." << endl;
			exit(EXIT_FAILURE);
		}
		arg->tid = i;
		arg->DataPoints = this;
		int ret = pthread_create(&(workerThreads[arg->tid]), NULL, callThread, (void *)arg);
		if (ret){
			errno = ret;
			perror("[DataPoints][Constructor] pthread_create()");
			exit(EXIT_FAILURE);
		}
	}

	recvQueryPoints(0);
}

DataPoints::~DataPoints() {

	pthread_mutex_destroy(&networkMutex);

	free(workerThreads);

	translateL2GIndex_vec.clear();
	translateG2LIndex_vec.clear();

#ifdef DEBUG_P_DPF
	cout << "[DP] Destructor." << endl;
#endif
}

void DataPoints::recvInputPoints() {
	cout << "[DP]["<< getInstanceId() <<"][recvInputPoints][" << Util::getStringTime() << "] Started." << endl;

	/* Number of points received. */
	uint32_t np_recvd = 0;

	while(1) {
		MsgDP<uint8_t> * m = new MsgDP<uint8_t>(d);

		if (ir2dp->probeAndReadStream(m) == EOW) {
			break;
		}

		m->deserialize();

		uint32_t npoints = m->getHeaderControl();

#ifdef DEBUG_P_DPF
		cout << "Recved msg np " << npoints << endl;
#endif

		for	(uint32_t i = 0; i < npoints; i++) {

			uint8_t * point = m->getPointValues(i);
			uint32_t pId = m->getPointId(i);

			createTranslationDB(pId);

			//pointsTable.addPoint(std::vector<uint8_t>(point, point + d));
			if( ! pointsTable.addPoint(point, translateG2LPIndex(pId), d) ) {
				cerr << "[DataPoints][recvInputPoints] ERROR could not add point " << pId << "." << endl;
				exit(EXIT_FAILURE);
			}

#ifdef DEBUG_P_DPF
			cout << pId << " tP (" << translateG2LPIndex(pId) << ")" << ", ";
#endif
			np_recvd++;
			if(np_recvd % 300000 == 0){
#ifdef DEBUG_P_DPF
				cout << endl;
#endif
				std::cout << "[DP][" << getInstanceId() << "][recvInputPoints][" << Util::getStringTime() << "] " << np_recvd << " registers indexed!" << std::endl;
			}
		}
		delete m;
	}

	cout << "[DP][" << getInstanceId() << "][recvInputPoints][" << Util::getStringTime() << "] Finished. Total of " << np_recvd << " points have been received." << endl;
}


/*
 * Receive the points within the same query's bucket from BucketsIndex.
 * Then, calculate ANN and send the results to Aggregator.
 */
void DataPoints::recvQueryPoints(int tid) {

	cout << "[DP][" << getInstanceId() << "][recvQueryPoints][th "<<tid<<"][" << Util::getStringTime() << "] Started." << endl;

#ifdef LSH_DPF_STATS
	uint64_t msg_sent_ag = 0;
#endif
	uint64_t msg_size_ag = 0;

	/* Number queries received.*/
	uint64_t q_recvd = 0;
	uint64_t p_sent = 0;

	uint32_t n_p = 0;

	while(1) {
		MsgBktPt * msg_in = new MsgBktPt();

		pthread_mutex_lock(&networkMutex);
		if(bi2dp->probeAndReadStream(msg_in) == EOW) {
			pthread_mutex_unlock(&networkMutex);
			break;
		}
		pthread_mutex_unlock(&networkMutex);

		if(msg_in == NULL) {
			cerr << "[DP][recvQueryPoints] ERROR msg pointer is null." << endl;
			exit(EXIT_FAILURE);
		}

		msg_in->deserialize(d, n_p);

		int qId = msg_in->getHeaderControl();

		/* Probe id */
		uint32_t probeId = msg_in->getProbeId();

		/* Number of DataPoints instances to Aggregator wait to end query processing. */
		uint32_t nDpInst = msg_in->getNDpInst();

		q_recvd++;
		if(q_recvd % 10000 == 0){
			std::cout << "[DP][" << getInstanceId() << "][th "<<tid<<"][recvQueryPoints][" << Util::getStringTime() << "] " <<  q_recvd << " msgs recvd!" << std::endl;
		}

		/*
		 * If there is more than 0 points, send them to Aggregator, else
		 * send an empty message.
		 */
		if(n_p > 0) {
			T_P * point = msg_in->getPointsId();
			T_Q * query = msg_in->getQuery();
			DataBucket<DBPnt> * buck = new DataBucket<DBPnt>();

#ifdef DEBUG_P_DPF
			cout << "[DP][th "<<tid<<"] recvd qId " << qId << " Probe ID " << probeId << " and "<< n_p << " points ";
#endif
			for(uint32_t i = 0; i < n_p; i++) {
#ifdef DEBUG_P_DPF
				cout << " ["<< i << "] " << " = " << point[i] << " (tP " << translateG2LPIndex(point[i]) << ")";
#endif
				DBPnt dbpnt(translateG2LPIndex(point[i]),translateG2LPIndex(point[i]));
				buck->addPoint(dbpnt);
			}
#ifdef DEBUG_P_DPF
			cout << endl;
#endif

//			cout <<"fim 1" << endl;
			/* Copying the query in (T_Q *) to vector. */
			vector<T_Q> q_vec(query, query + d);
//			cout <<"fim 2" << endl;

			/* ADT to store Ann. */
			std::vector<ANNPoint> curANN;
//			cout <<"fim 3" << endl;

			Operator::calculateANN(curANN, k, q_vec, buck, pointsTable);
//			cout <<"fim 4" << endl;

			/* Send the Ann to aggregator filter. */
			msg_size_ag += sendANN2Agg(curANN, qId, nDpInst, probeId);

#ifdef LSH_DPF_STATS
			msg_sent_ag++;
#endif

			p_sent++;
			if(p_sent % 100000 == 0){
				std::cout << "[DP][" << getInstanceId() << "][th "<<tid<<"][recvQueryPoints][" << Util::getStringTime() << "] " <<  p_sent << " msgs sent to AG!" << std::endl;
			}

			delete buck;
		} else {
#ifdef DEBUG_P_DPF
			cout << "[DP][th "<<tid<<"] recvd qId " << qId << " nDpInst " << nDpInst << " probeId " << probeId << " empty msg ." << endl;
#endif

			msg_size_ag += sendEmtpyMsg2Agg(qId, nDpInst, probeId);

#ifdef LSH_DPF_STATS
			msg_sent_ag++;
#endif
		}

		delete msg_in;
	}

	if(tid == 0){
		// Join... make sure all other threads have finished
		this->joinThreads();

		/* No messages with ann. Close the stream to Aggregator.*/
		dp2ag->closeLabelStream();
	}

#ifdef LSH_DPF_STATS
	pthread_mutex_lock(&networkMutex);
	cout << "[DP]["<<getInstanceId()<< "][Stats][th "<<tid<<"] Messages sent to AG " << msg_sent_ag << "; Total bytes sent " << msg_size_ag << "; Average size " << (msg_sent_ag > 0 ? msg_size_ag/msg_sent_ag : 0) << " (bytes/msg)." << endl;
	pthread_mutex_unlock(&networkMutex);
#endif
	pthread_mutex_lock(&networkMutex);
	cout << "[DP]["<<getInstanceId()<<"][recvQueryPoints][th "<<tid<<"][" << Util::getStringTime() << "] Finished. Total of " << q_recvd << " query msgs have been received."  << endl;
	pthread_mutex_unlock(&networkMutex);

}

/*!
 * Send the Ann points (with global id) to Aggregator Filter.
 */
uint64_t DataPoints::sendANN2Agg(const std::vector<ANNPoint> &curAnn, const int qId, const uint32_t nDpInst, const uint32_t probeId) {
	MsgAnn msg_out(curAnn.size());

	T_P * pointsId = msg_out.getPointsId();
	if(pointsId == NULL) {
		cerr << "[DP][sendANN2Agg] Error pointsId equals NULL. " << endl;
		exit(1);
	}

	T_D * pointsDist = msg_out.getPointsDist();
	if(pointsDist == NULL) {
		cerr << "[DP][sendANN2Agg] Error pointsDist equals NULL. " << endl;
		exit(1);
	}

	for(uint32_t i = 0; i < curAnn.size(); i++) {
		/* Translates back the id of points from local to global. */
		pointsId[i] = translateL2GPIndex(curAnn[i].getId());
		pointsDist[i] = curAnn[i].getDistance();
	}

	/* Set the number of DataPoints instances to Aggregator must wait to end query processing. */
	msg_out.setHeaderLabel(qId);
	msg_out.setNDpInst(nDpInst);
	msg_out.setProbeId(probeId);

	pthread_mutex_lock(&networkMutex);
	dp2ag->sendBuffer(msg_out);
	pthread_mutex_unlock(&networkMutex);

#ifdef DEBUG_P_DPF
	cout << "[DP][sendANN2Agg] sent msg with qId " << qId  << " nDpInst " << nDpInst << " probeId " << probeId << endl;
#endif
	return msg_out.getSizeMsg();
}

/*!
 * Send empty message to Aggregator.
 */
uint64_t DataPoints::sendEmtpyMsg2Agg(const int qId, const uint32_t nDpInst, const uint32_t probeId) {
	MsgAnn msg_out(0);
	/* Set the number of DataPoints instances to Aggregator must wait to end query processing. */
	msg_out.setHeaderLabel(qId);
	msg_out.setNDpInst(nDpInst);
	msg_out.setProbeId(probeId);

	pthread_mutex_lock(&networkMutex);
	dp2ag->sendBuffer(msg_out);
	pthread_mutex_unlock(&networkMutex);

	return msg_out.getSizeMsg();
}

/**
 * Create the database index based on point  global id.
 * Now it supports receiving points independent of order and distribution.
 * @param global_id
 */
void DataPoints::createTranslationDB(uint32_t global_id) {
	translateL2GIndex_vec.push_back(global_id);

	if( translateG2LIndex_vec.size() <= global_id ) {
		translateG2LIndex_vec.resize(global_id+1);
	}

	translateG2LIndex_vec[global_id] = translateL2GIndex_vec.size()-1;
}

/**
 * Translate global point index to local point index.
 * This is necessary because the PointsTable needs the points to be
 * stored with an incremental id. However, with multiple instances of
 * DataPoints the points are spread over the instances regarding a KEY (i.e. point id or chain_key) and the ids are not sequential.
 */
uint32_t DataPoints::translateG2LPIndex(uint32_t global_id) {
/*	ret = global_id / this->getNFilterInstances();*/
	return translateG2LIndex_vec[global_id];
}

/**
 * Translate local point index to global point index.
 * This is necessary because the PointsTable needs the points to be
 * stored with an incremental id. However, with multiple instances of
 * DataPoints the points are spread over the instances and the ids are not sequential.
 * BE CAREFUL: the code only works if the datapoints are distributed in a round-robin way.
 */
uint32_t DataPoints::translateL2GPIndex(uint32_t local_id) {
/*	ret = (local_id * this->getNFilterInstances()) + this->getInstanceId();*/
	return translateL2GIndex_vec[local_id];
}
