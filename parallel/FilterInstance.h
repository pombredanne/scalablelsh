/*
 * FilterInstance.h
 *
 *  Created on: Aug 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */


#ifndef FILTERINSTANCE_H_
#define FILTERINSTANCE_H_

/** To be used when the parameter is not defined.*/
#define NOT_DEFINED -1


#include <stdint.h>
/**
 *  \class FilterInstance
 *  \brief Base Class for implementing filter classes.
 */
class FilterInstance {
private:
	/*!< Id among all mpi process. */
	uint32_t global_id;
	/*!< Id among only the same instances, zero-based id. */
	uint32_t instance_id;
	/*!< Total number of siblings instances in the same filter. */
	uint32_t n_filter_instances;

	FilterInstance();
protected:

public:
	/**
	 * FilterInstance Constructor.
	 * \param g MPI global process rank.
	 * \param i Instance id among only the same instances, zero-based.
	 * \param n_f_inst Number of instances in the same filter.
	 *
	 */
	FilterInstance(uint32_t g, uint32_t i, uint32_t n_f_inst) : global_id(g), instance_id(i), n_filter_instances(n_f_inst) {}
	/**
	 * FilterInstance Destructor.
	 */
	virtual ~FilterInstance(){}

	/**
	 *  Get the global id of this instance.
	 *  \return MPI global process rank.
	 */
	uint32_t getGlobalId() const { return global_id; }
	/**
	 *  Get the instance id of this filter, zero-based id.
	 *  \return instance's id among its filter.
	 */
	uint32_t getInstanceId() const {return instance_id; }
	/**
	 *  Total number of siblings instances in the same filter.
	 *  \return total number of sibling instances.
	 */
	uint32_t getNFilterInstances() const {return n_filter_instances;}
};


#endif /* FILTERINSTANCE_H_ */
