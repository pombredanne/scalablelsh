/*
 * MsgLS.h
 *
 *  Created on: Feb 11, 2013
 *      Author: thiago
 */

#ifndef MSGLS_H_
#define MSGLS_H_

#include "LabelStream.h"

/**
 * \class MsgLS
 * \brief Abstract class to be used as base for every label stream
 * message.
 */
class MsgLS {
private:
	void * msg;
	uint32_t size_msg;

	inline void init_class(uint32_t size_msg_in) {
		size_msg = size_msg_in + sizeof_header;
		alloc(size_msg);
	}

	inline void alloc(uint32_t size) {
  		if(size < sizeof_header) {
  			std::cerr << "[MsgLS][allocMsgLS] ERROR size is smaller than message header. " << std::endl;
  			exit(EXIT_FAILURE);
  		}

  		/** Fill up the size msg variable. */
  		if(size_msg == 0) {
  			size_msg = size;
  		}

//		std::cout << "+++ MsgLS alloc " << (void *) msg << " size " << size <<  std::endl;

		msg = malloc(size);
		if(msg == NULL) {
			std::cerr << "[MsgLS][allocMsgLS]" << " ERROR could not allocate msg." << std::endl;
			exit(EXIT_FAILURE);
		}
	}
protected:
	/*! Number of info in the header. */
	const static uint32_t header_ninfo = 2;
	/*! Size of the label in 4 bytes and control close stream in other 4. */
	const static uint32_t sizeof_header = header_ninfo*sizeof(int);
	/*! Positions in header for information. */
	const static uint32_t header_pos_label = 0;
	/*! May be register id or EOW*/
	const static uint32_t header_pos_control = 1;
public:

	/**
	 * MsgLS constructor.
	 * Receives the "net" size of the message and adds internally the header size (sizeof_header) of MsgLS.
	 * @param size_msg_in Receive the size the message, it will add the header of MsgLS.
	 */
	MsgLS(uint32_t size_msg_in) {
		init_class(size_msg_in);
	}

	/**
	 * Create an MsgLS but do not allocate any variable.
	 * After calling this, should call alocMsgLS().
	 */
	MsgLS() : msg(NULL), size_msg(0) {}

	MsgLS(const MsgLS& in) : msg(NULL), size_msg(in.size_msg) {
		if(in.msg == NULL) {
			std::cerr << "[MsgLS][CopyC]" << " ERROR in.msg is not allocated." << std::endl;
  			exit(EXIT_FAILURE);
		}
		alloc(size_msg);
		memcpy(msg, in.msg, in.size_msg);
	}

 	virtual ~MsgLS() {
 		if(msg != NULL) {
			free(msg);
			msg = NULL;
		}
	}

  	MsgLS& operator=(const MsgLS& rhs) {
  		size_msg = rhs.size_msg;
  		if(rhs.msg == NULL) {
  			std::cerr << "[MsgLS][operator=]" << " ERROR rhs.msg is not allocated." << std::endl;
  			exit(EXIT_FAILURE);
  		}
  		if(msg == NULL) {
  			std::cerr << "[MsgLS][operator=]" << " ERROR msg is not allocated." << std::endl;
  			exit(EXIT_FAILURE);
  		}
  		if(size_msg > 0) {
  			memcpy(msg, rhs.msg, rhs.size_msg);
  		}
  		return *this;
  	}

  	/**
  	 * Allocates the data for message.
  	 *
  	 * \attention size parameter needs to be at least the size of the header.
  	 * Should not be called more than once.
  	 *
  	 * @param size size of message to be allocated must include the size of the header.
  	 */
  	inline void allocMsgLS(uint32_t size) {
  		/** Does not do anything if it is already allocated. */
  		if(msg != NULL) {
  			std::cerr << "[MsgLS][allocMsgLS]" << " ERROR msg is already allocated." << std::endl;
  			exit(EXIT_FAILURE);
  		}
  		alloc(size);
  	}

	void setHeaderLabel(int label){
		((int *)msg)[header_pos_label] = label;
	}

	/* Return info from msg header. */
	int getHeaderLabel() {
		return ((int *) msg)[header_pos_label];
	}

	/* Used to recv point id or EOW. */
	void setHeaderControl(int regId) {
		((int *) msg)[header_pos_control] = (int) regId;
	}

	/* Return reg Id or EOW from msg header. */
	int getHeaderControl() {
		return ((int *) msg)[header_pos_control];
	}

	/*Define this msg as EOW. */
	void setMsgEOW() {
		((int *) msg)[header_pos_control] = EOW;
	}

	void * getMsg() const {
		return msg;
	}

	bool isEOWmsg() {
		if(getHeaderControl() == EOW) {
			return true;
		} else {
			return false;
		}
	}

	uint32_t getSizeMsg() {
		return size_msg;
	}

	static const uint32_t getSizeofHeader() {
		return sizeof_header;
	}

};

#endif /* MSGLS_H_ */
