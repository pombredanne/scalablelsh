/*
 * QueryReceiver.h
 *
 *  Created on: Sep 6, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef QUERYRECEIVER_H_
#define QUERYRECEIVER_H_

#include <list>
#include <vector>

#include <PStableHash.h>
#include <FileIO.h>
#include <DefaultCalcHashKey.h>
#include <Multiprobe.h>

#include "FilterInstance.h"
#include "LSQueryBucketMsg.h"

//#ifndef DEBUG_P_QRF
//#define DEBUG_P_QRF
//#endif

/**
 * \class QueryReceiver
 * \brief Reads the queries from disk and send to be processed.
 *
 * Each file has as its name the prefix plus the instance rank.
 * Moreover, the id of the queries must be unique globally. This
 * means that it may have multiple files but each query must have
 * an unique identifier for all files.
 *
 * This filter implements the QueryReceiver. It sends the
 * queries to BucketsIndex using each query's hash
 * to address each message.
 */
class QueryReceiver: public FilterInstance {
private:
	/*! Stream queries to BucketsIndex. */
	LabelStream * qr2bi;
	/*! Object to read the queries. */
	FileIn * f_io;

	int L, /*! Number of hashtables created by the application. */
		m, /*! Number of hash functions for each hashtable. */
		d, /*! Number of dimensions in each point. */
		w,
		T; /*! Number of probes per hashtable. */

	float R;
	bool multiprobe;
	unsigned hashTableSize;
	unsigned queryRate;

	/*
	 * The multiprobe approach does not have the chain_key.
	 * Thus, when comparing with multiprobe we do not use chain_key.
	 */
	bool useChainKey;

	std::vector<unsigned> mainHash;
	std::vector<unsigned> controlHash;

	/**
	 * Creates the global query id.
	 * To have multiple instances of QueryReceiver each query must have its unique
	 * identifier. Thus, the query id from now on will be instance's id + query id.
	 * @param inst_id Instance unique identifier.
	 * @param query_id Query identifier.
	 * @return Global unique query identifier.
	 */
	int createInstQueryId(const int inst_id, const int query_id) const;

	/**
	 * Set of all h(v) hash functions, first m are
	 * for g1, second set of h are for g2, and so on.
	 */
	std::vector<PStableHash> hashFunctions;

	void calcHashKey(const std::vector<uint8_t> &point,
						const int hashTableId,
						T_KEY& bucket_key,
						T_KEY& chain_key);

	void calcHashKeyMP(const std::vector<uint8_t> &point,
						const int  hashTableId,
						std::vector<uint32_t> &seq);

	void readAndSendQuery();

	QueryReceiver();
public:
	/**
	 * QueryReceiver constructor.
	 * \param g MPI global process rank.
	 * \param i Instance id among only the same instances, zero-based.
	 * \param n_f_inst Number of instances in the same filter.
	 * \param qr2bi
	 * \param f_io_in
	 * \param L Number of hash tables.
	 * \param m Number of hash functions for each hash table.
	 * \param dimensions Number of points' dimensions.
	 * \param w Needs some explanation
	 * \param R radius of the the search
	 * \param T Number of probes per hashtable
	 */
	QueryReceiver(uint32_t g,
			uint32_t i,
			uint32_t n_f_inst,
			LabelStream * qr2bi,
			FileIn &f_io_in,
			int L,
			int m,
			int dimensions,
			int w,
			int T,
			float R,
			unsigned queryRate);
    /**
     * QueryReceiver destructor.
     */
	virtual ~QueryReceiver();
};

#endif /* QUERYRECEIVER_H_ */
