#include "InputReader.h"


InputReader::InputReader(uint32_t g,
						uint32_t i,
						uint32_t n_f_inst,
						LabelStream * ir2bi,
						LabelStream * ir2dp,
						FileIn &f_io_in,
						int L,
						int m,
						int dimensions,
						int w,
						int w_zorder,
						float R,
						bool use_ck,
						bool use_zo) :
							FilterInstance(g, i, n_f_inst),
							ir2bi(ir2bi),
							ir2dp(ir2dp),
							L(L),
							m(m),
							d(dimensions),
							w(w),
							w_zorder(w_zorder),
							R(R),
							use_ck_label(use_ck),
							use_zo_label(use_zo)
{
	/*
	 * The same seed is used in InputReader and QueryReceiver.
	 * Therefore, the pseudo-random generation of the hash tables
	 * are made in the same order for both filters, and
	 * they have the same hash tables with the same hash functions.
	 */
	srandom(DEFAULT_INITIAL_SEED);
	f_io = &f_io_in;

	this->hashTableSize = f_io_in.getNumberOfRegisters();

	for(int i = 0; i < L*m; i++){
		hashFunctions.push_back(PStableHash(dimensions, w));
	}

	for(int i = 0; i < m; i++){
		this->mainHash.push_back(DefaultCalcHashKey::genRandomUns32(1, DefaultCalcHashKey::MAX_HASH_RND));
	}
	for(int i = 0; i < m; i++){
		this->controlHash.push_back(DefaultCalcHashKey::genRandomUns32(1, DefaultCalcHashKey::MAX_HASH_RND));
	}

	/* NOT using chain_key. */
	this->useChainKey = false;

	if( ! useChainKey ) {
		std::cout << "** NOT using chain_key. **" << std::endl;
	}

	readAndSendInput();
}
	

InputReader::~InputReader(){
#ifdef DEBUG_P_IRF
	cout << "[IR] Destructor." << endl;
#endif
}

/*
 * Read and send to next filter.
 *
 */
void InputReader::readAndSendInput() {

	std::cout << "[IR]["<< getInstanceId() <<"][readAndSendInput][" << Util::getStringTime() << "] Started." << std::endl;

	std::vector<uint8_t> point;

	/*
	 * Vector with haHash key in integers to send the message to next filters.
	 * Each Msg contains chain_key and hash_key <hashtableId + hashkey[0..m-1] + reg_id> .
	 */
	std::vector<MsgBkt> msg_bi_vec(ir2bi->getDestInstSize(), MsgBkt(MAX_POINTS_PER_MSG_2_BI));

	/* Bucket_Key */
	T_KEY bucket_key = 0;

	int reg_id = 0;

#ifdef LSH_IRF_STATS
	uint64_t reg_sent_bi = 0;
	uint64_t msg_size_bi = 0;
	uint64_t reg_sent_dp = 0;
	uint64_t msg_size_dp = 0;
#endif

	uint64_t reg_read = 0;

	/* Chain_key */
	T_KEY chain_key = 0;

	/* The point label can be anything (i.e. chain_key, bucket_key or point id). */
	T_KEY point_DP_label = 0;

	/*
	 * Variables to send points messages to DataPoints filter.
	 */
	std::vector< MsgDP<uint8_t> > msg_ir2dp_vec(ir2dp->getDestInstSize(), MsgDP<uint8_t>(MAX_POINTS_PER_MSG_2_DP, d));

#ifdef DEBUG_P_IRF
	cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Created structure for " << msg_bi_vec.size() << " BI(s) and " << msg_ir2dp_vec.size() << " DP(s)."  << endl;
#endif

	while(1){

		point.clear();
		reg_id = f_io->readInputPoint(point);
		if (point.size() == 0) {
			break;
		}

		/* Send the bucket_key to BucketIndex instance. */
		for(int j = 0; j < L; j++){
			calcHashKey(point, j, bucket_key, chain_key);

			uint32_t bi_inst = bucket_key % ir2bi->getDestInstSize();

			if(j==0) {
				if(use_ck_label) {
					point_DP_label = chain_key;
				} else if (use_zo_label) {
					point_DP_label = DistPntsZOrder::calcZorderLabel(point, d, w_zorder);
				}
			}

			/* Chain_key is ignored when using multiprobe. */
			if( ! useChainKey ) {
				chain_key = 0;
			}

			if (use_ck_label || use_zo_label) {
				msg_bi_vec[bi_inst].push_back(reg_id, chain_key, bucket_key, j, point_DP_label);
			} else {
				msg_bi_vec[bi_inst].push_back(reg_id, chain_key, bucket_key, j, reg_id);
			}

#ifdef DEBUG_P_IRF
			cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Pushed back point " << reg_id << " htid " << j << " to BI " << bi_inst << endl;
#endif

			if(msg_bi_vec[bi_inst].isBufferFull()) {
				msg_bi_vec[bi_inst].setHeaderLabel(bi_inst);
				msg_bi_vec[bi_inst].setHeaderControl(MAX_POINTS_PER_MSG_2_BI);

				ir2bi->sendBuffer(msg_bi_vec[bi_inst]);

				msg_bi_vec[bi_inst].flushBuffer();

#ifdef DEBUG_P_IRF
				cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Buffer full sent " << MAX_POINTS_PER_MSG_2_BI << " to BI " << bi_inst << endl;
#endif

#ifdef LSH_IRF_STATS
				reg_sent_bi++;
				msg_size_bi += msg_bi_vec[bi_inst].getSizeMsg();
#endif
			}

#ifdef DEBUG_P_IRF
			cout << "[IR] p " << reg_id << " bucket_key " << bucket_key << " chain_key " << chain_key << endl;
#endif
		}

		uint32_t dp_inst;
		if(use_ck_label || use_zo_label) {
			dp_inst = point_DP_label % ir2dp->getDestInstSize();
		} else {
			dp_inst = reg_id % ir2dp->getDestInstSize();
		}

		/* Send the points to DataPoints instance. */
		msg_ir2dp_vec[dp_inst].push_back(reg_id, &point[0]);

#ifdef DEBUG_P_IRF
		cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Pushed back point " << reg_id << " to DP " << dp_inst << endl;
#endif

		/* Check if the msg buffer must be emptied to any DP destination. */
		if(msg_ir2dp_vec[dp_inst].isBufferFull()) {
			msg_ir2dp_vec[dp_inst].setHeaderLabel(dp_inst);
			msg_ir2dp_vec[dp_inst].setHeaderControl(MAX_POINTS_PER_MSG_2_DP);

			/* Send buffer message. */
			ir2dp->sendBuffer(msg_ir2dp_vec[dp_inst]);

			msg_ir2dp_vec[dp_inst].flushBuffer();

#ifdef DEBUG_P_IRF
			cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Buffer full sent " << MAX_POINTS_PER_MSG_2_DP << " to DP " << dp_inst << endl;
#endif

#ifdef LSH_IRF_STATS
			reg_sent_dp++;
			msg_size_dp += msg_ir2dp_vec[dp_inst].getSizeMsg();
#endif
		}

		reg_read++;
		if(reg_read % 300000 == 0){
			std::cout << "[IR]["<< getInstanceId() <<"][readAndSendInput][" << Util::getStringTime() << "] " << reg_read << " registers read (id " << reg_id << ")!" << std::endl;
		}

	}

	/* Send the uncompleted buffers to BI. */
	for(int inst = 0; inst < ir2bi->getDestInstSize(); inst++) {
		if(msg_bi_vec[inst].getLastPos() > 0) {
			msg_bi_vec[inst].setHeaderLabel(inst);
			msg_bi_vec[inst].setHeaderControl(msg_bi_vec[inst].getLastPos());

			ir2bi->sendBuffer(msg_bi_vec[inst]);

#ifdef DEBUG_P_IRF
			cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Buffer emptied sent " << msg_bi_vec[inst].getLastPos() << " to BI " << inst << endl;
#endif

#ifdef LSH_IRF_STATS
			reg_sent_bi++;
			msg_size_bi += msg_bi_vec[inst].getSizeMsg();
#endif
		}
	}

	/* Send the uncompleted buffers to DP. */
	for(int inst = 0; inst < ir2dp->getDestInstSize(); inst++) {
		if (msg_ir2dp_vec[inst].getLastPos() > 0) {
			msg_ir2dp_vec[inst].setHeaderLabel(inst);
			msg_ir2dp_vec[inst].setHeaderControl(msg_ir2dp_vec[inst].getLastPos());

			/* Send buffer message. */
			ir2dp->sendBuffer(msg_ir2dp_vec[inst]);

#ifdef DEBUG_P_IRF
			cout << "[IR]["<< getInstanceId() <<"][readAndSendInput] Buffer emptied sent " << msg_ir2dp_vec[inst].getLastPos() << " to DP " << inst << endl;
#endif

#ifdef LSH_IRF_STATS
			reg_sent_dp++;
			msg_size_dp += msg_ir2dp_vec[inst].getSizeMsg();
#endif
		}
	}

	ir2bi->closeLabelStream();
	ir2dp->closeLabelStream();

#ifdef LSH_IRF_STATS
	cout << "[IR]["<< getInstanceId() <<"][Stats] Messages sent to BI " << reg_sent_bi << " with up to "<< MAX_POINTS_PER_MSG_2_BI <<" points buckets each; Total bytes sent " << msg_size_bi << "; Average size " << (reg_sent_bi > 0 ? msg_size_bi/reg_sent_bi : 0) << " (bytes/msg)." << endl;
	cout << "[IR]["<< getInstanceId() <<"][Stats] Messages sent to DP " << reg_sent_dp << " with up to "<< MAX_POINTS_PER_MSG_2_DP <<" points each; Total bytes sent " << msg_size_dp << "; Average size " << (reg_sent_dp > 0 ? msg_size_dp/reg_sent_dp : 0) << " (bytes/msg)." << endl;
#endif

	std::cout << "[IR]["<< getInstanceId() <<"][readAndSendInput][" << Util::getStringTime() << "] Finished. Total of " << reg_read << " registers have been read." << std::endl;
}

/*
 * Returns the hashkey in two formats: integer array and a string.
 */
void InputReader::calcHashKey(const std::vector<uint8_t> &point,
									const int hashTableId,
									T_KEY& bucket_key,
									T_KEY& chain_key)
{
	DefaultCalcHashKey::calcHashKey(point,
									hashTableId,
									hashFunctions,
									m,
									hashTableSize,
									R,
									mainHash,
									controlHash,
									bucket_key,
									chain_key);
}
