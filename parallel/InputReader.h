/*
 * InputReader.h
 *
 *  Created on: Aug 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */ 



#ifndef INPUTREADER_H_
#define INPUTREADER_H_ 

#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include <DefaultCalcHashKey.h>
#include <DistPntsZOrder.h>
#include <Util.h>
#include <FileIO.h>

#include "LSH.h"
#include "FilterInstance.h"
#include "LSBucketMsg.h"
#include "LSDataPointMsg.h"

//#ifndef DEBUG_P_IRF
//#define DEBUG_P_IRF
//#endif

#define MAX_POINTS_PER_MSG_2_BI 200
#define MAX_POINTS_PER_MSG_2_DP 50

/**
 * \class InputReader
 * \brief Reads the input points from disk and send
 * them to DataPoints and their hash to BucketsIndex.
 *
 * Each file has as name the prefix plus the instance rank.
 * Moreover, the id of the points must be unique globally. This
 * means that it may have multiple files but each poist must have
 * an unique identifier for all files.
 *
 * This filter implements the InputReader. It sends the
 * points read from disk and their hash to the BucketsIndex create point's
 * bucket.
 */
class InputReader : public FilterInstance {
private:
	/*! Stream with hash to BucketsIndex. */
	LabelStream * ir2bi;
	/*! Stream with points to DataPoints. */
	LabelStream * ir2dp;
	/*! Object to read the points from disk. */
	FileIn * f_io;

	int L, /*! Number of hashtables created by the application. */
		m, /*! Number of hash functions for each hashtable. */
		d, /*! Number of dimensions in each point. */
		w,
		w_zorder;

	unsigned hashTableSize;

	float R;

	/*
	 * The multiprobe approach does not have the chain_key.
	 * Thus, when comparing with multiprobe we do not use chain_key.
	 */
	bool useChainKey;

	/*! Define wether the label will be the chain_key, the point_id or the z_order. */
	bool use_ck_label, use_zo_label;

	std::vector<unsigned> mainHash;
	std::vector<unsigned> controlHash;

	/*!
	 *! Set of all h(v) hash functions, first m are
	 *! for g1, second set of h are for g2, and so on.
	 */
	std::vector<PStableHash> hashFunctions;

	void calcHashKey(const std::vector<uint8_t> &point,
										const int hashTableId,
										T_KEY& bucket_key,
										T_KEY& chain_key);
	void readAndSendInput();

	InputReader();
protected:

public:
	/**
	 * InputReader constructor.
	 * \param g MPI global process rank.
	 * \param i Instance id among only the same instances, zero-based.
	 * \param n_f_inst Number of instances in the same filter.
     * \param ir2bi LabelStream between InputReader's instances and BucketsIndex's instances.
     * \param ir2dp LabelStream between InputReader's instances and DataPoints' instances.
     * \param f_io_in Input stream to read the database.
     * \param L Number of hash tables.
	 * \param m Number of hash functions for each hash table.
	 * \param dimensions Number of points' dimensions.
	 * \param w Needs some description
	 * \param w_zorder
	 * \param R
	 */
	InputReader(uint32_t g,
				uint32_t i,
				uint32_t n_f_inst,
				LabelStream * ir2bi,
				LabelStream * ir2dp,
				FileIn &f_io_in,
				int L,
				int m,
				int dimensions,
				int w,
				int w_zorder,
				float R,
				bool use_ck,
				bool use_zo);
    /**
     * InputReader destructor.
     */
	~InputReader();
};


#endif /* INPUTREADER_H_ */
