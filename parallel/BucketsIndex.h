/**
 * @file
 * BucketsIndex.h
 *
 *  Created on: Aug 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */ 


#ifndef BUCKETSINDEX_H_
#define BUCKETSINDEX_H_ 

#include <stdint.h>
#include <stdlib.h>
#include <sstream>
#include <list>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include <HashTable.h>
#include <Util.h>
#include <DefaultCalcHashKey.h>

#include <tr1/unordered_map>

#include "FilterInstance.h"
#include "LabelStream.h"
#include "LabelStreamFC.h"
#include "LSBucketMsg.h"
#include "LSQueryBucketMsg.h"
#include "LSBucketPointsMsg.h"

//#ifndef DEBUG_P_BIF
//#define DEBUG_P_BIF
//#endif

/**
 * \class BucketsIndex
 * \brief Implements the BucketsIndex instance.
 *
 *  Receives the input points from InputReader and store them at their respectively buckets.
 *  Then, it receives each query from QueryReceiver, find its bucket, and send points id
 *  from the bucket found to Aggregator's instances.
 *
 *  We used to send an message to each DataPoints instance for each query and then it was forwarded to Aggregator instance
 *  to know when the query processing is finished. But when there is no point to send this approach is cumbersome,
 *  so we now send the message to
 *  just one DataPoints instance with the number of DataPoints instances which will receive at least one point.
 *  This information is forwarded to Aggregator which now can know how many messages it must wait to finish the
 *  query processing.
 */
class BucketsIndex : public FilterInstance {
private:
        /*! Stream with buckets from input reader. */
        LabelStream * ir2bi;
        /*! Stream with bucket message from query filter. */
        LabelStream * qr2bi;
        /*! Stream with points' id matched within the same bucket to DataPoints. */
        LabelStream * bi2dp;

        HashTable hashTable;
        int k, /*! Number of neighbors returned. */
            L, /*! Number of hash tables created by the application. */
            m, /*! Number of hash functions for each hash table. */
            d, /*! Number of dimensions in each point. */
			T; /*! Number of probes per hashtable. */

    	bool multiprobe;

    	// structure containing information about the threads
    	pthread_t *workerThreads;
    	int numberOfThreads;

    	pthread_mutex_t networkMutex;

    	void joinThreads();

    	std::string bktInt2Str(T_KEY bucket_key, T_HTI hashTableId);

        void recvBucket();

        BucketsIndex();
protected:

public:
        /**
         * BucketsIndex constructor.
         * \param g MPI global process rank.
	     * \param i Instance id among only the same instances, zero-based.
	     * \param n_f_inst Number of instances in the same filter.
	     * \param ir2bi LabelStream between InputReader's instances and BucketsIndex's instances.
	     * \param qr2bi LabelStream between QueryReceiver's instances and BucketsIndex's instances.
	     * \param bi2dp LabelStream between BucketsIndex's instances and DataPoints' instances.
	     * \param L Number of hash tables.
	     * \param m Number of hash functions for each hash table.
	     * \param d Number of points' dimensions.
	     * \param k Maximum number of neighbors returned.
	     * \param T Number of probes per hash table.
         */
        BucketsIndex(uint32_t g,
        			uint32_t i,
        			uint32_t n_f_inst,
        			LabelStream * ir2bi,
                    LabelStream * qr2bi,
                    LabelStream * bi2dp,
                    int k,
                    int L,
                    int m,
                    int d,
                    int T,
                    int fbith);
        /**
         * BucketsIndex destructor.
         */
        ~BucketsIndex();

        void findAndSendQueryBucket(int tid);
};
#endif /* BUCKETSINDEX_H_ */
