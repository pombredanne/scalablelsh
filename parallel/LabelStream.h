/*
 * LabelStream.h
 *
 *  Created on: Aug 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */ 


#ifndef LABELSTREAM_H_
#define LABELSTREAM_H_

#include <pthread.h>
#include <mpi.h>
#include <iostream>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>

#define EOW -2
#define ORDINARY_MSG 0 /*Defines a correct msg.*/
#define USLEEP_TIME 1000 //1 Milliseconds

#include "Stream.h"
#include "MsgLS.h"

using namespace std;

/*! Types used within LabelStream classes. */
/*! Type for points' distance pointers. */
typedef float T_D;
/*! Type for input point messages.*/
typedef uint32_t T_P;
/*! Type for query messages. */
typedef uint8_t T_Q;
/*! Type for bucket key and chain_key header messages. */
typedef uint32_t T_KEY;
/*! Type for hash table id header messages. */
typedef uint32_t T_HTI;
/*! Type to be used by labelFunction. */
typedef uint32_t T_LS;

//#ifndef DEBUG_P_LS
//#define DEBUG_P_LS
//#endif

/**
 * \class LabelStream
 * \brief Abstract class which defines a LabelStream.
 *
 * \attention Class' methods are not thread-safe.
 * This class defines a LabelStream based on Stream interface.
 * The message is routed base on its content (label).
 * EOW is used to control the termination process. When all
 * the source instances send an EOW means that the stream can be closed.
 * Therefore, when all the streams are closed the filter can finish its execution.
 */
class LabelStream : public Stream {
public:
	/*! To be used by an instance that is source for stream. */
	const static int LS_NO_SOURCE = -1;
	/*! To be use by an instance that is destination for stream. */
	const static int LS_NO_DEST = -1;

	enum InstType {
		SOURCE,
		DESTINATION
	};

	/**
	 * LabelStream constructor.
	 * @param oF_i_r Initial rank of source filter instances.
	 * @param oF_n Number of source filter instances.
	 * @param dF_i_r Initial rank of destination filter instances.
	 * @param dF_n Number of destination filter instances.
	 * @param tag LabelStream unique identifier.
	 */
	LabelStream(int32_t oF_i_r,
				int32_t oF_n,
				int32_t dF_i_r,
				int32_t dF_n,
				int tag);

	/**
	 * LabelStream destructor.
	 */
	virtual ~LabelStream();

	/**
	 * Read buffer with previously allocated message.
	 * Handle the EOWs received from source filter's instances.
	 * If the message is an EOW, counts it and waits the next message.
	 * \param b Message buffer.
	 * \return EOW or ORDINARY_MSG.
	 */
	//virtual int readBuffer(MsgLS * b, const int source);
	int readBuffer(MsgLS * b, const int source);
    /**
     * Send buffer message.
     * @param b MsgAnn object.
     */
	//virtual void sendBuffer(MsgLS& b);
	void sendBuffer(MsgLS& b);
	/**
	 * Checks whether the stream is closed, if not check if there is any message to receive.
	 * @param src Returns by reference the sender of the message.
	 * @return EOW if the stream is closed or the sized of the message to be received.
	 */
	//virtual int probeStream(int &src);
	int probeStream(int &src);
	/**
	 *
	 * @param m
	 * @return
	 */
	//virtual int probeAndReadStream(MsgLS * m);
	int probeAndReadStream(MsgLS * m);
//	/** Function which returns the label to route it. */
//	virtual T_LS labelFunction(void * msg, uint32_t size_label) const = 0;
	T_LS labelFunction(void * msg, uint32_t size_label) const;

	/**
	 * Close the stream by sending EOW to all destination instances.
	 */
	void closeLabelStream() ;

	/**
	 * Get the number of source filter instances with stream opened.
	 * \return the number of source filter instances with stream open .
	 */
	int32_t getActiveSourceInst() const;

	/**
	 *
	 * @return
	 */
	int32_t getDestInstInitRank() const;

	/**
	 * Get the number of destination instances.
	 * @return the number of destination instances.
	 */
	int32_t getDestInstSize() const;

	/**
	 * Get the initial rank of source instances.
	 * @return the initial rank of source instances.
	 */
	int32_t getSourceInstInitRank() const;

	/**
	 * Get Number of source instances.
	 * @return number of source instances.
	 */
	int32_t getSourceInstSize() const;

	/**
	 * Return the type of this LS instance.
	 * @return Source or Destination.
	 */
	InstType getInstType() const;

protected:
	/**
	 * Send buffer message.
	 * @param msg Pointer to message buffer.
	 * @param size_msg Size of message in bytes.
	 */
	void send(void * msg, uint32_t size_msg);

	void send(void * msg, uint32_t size_msg, int dest, int tag);

	/**
	 * Receive buffer method.
	 * @param msg Pointer to message buffer.
	 * @param size_msg Size of message in bytes.
	 * @param source source id filter.
	 */
	void recv(void * msg, uint32_t size_msg, int source);

	void recv(void * msg, uint32_t size_msg, int source, int tag);

	/**
	 * Check if there is any message to receive.
	 * @return Size of message to be received in bytes.
	 */
	int probe(int &source);

	int probe(int &source, int tag);

	/**
	 * Check if there is any message to receive.
	 * @return Size of message to be received in bytes.
	 */
	int iprobe(int &source);

	int iprobe(int &source, int tag);

	int iprobe(int &source, int tag, int &out_tag);

	/** Close one more source instance stream. */
	void closeSourceInstLS();

	/**
	 * Probe and receive messages with an specific tag. Only returns if a message is received.
	 * Will allocate the message according to size probed.
	 *
	 * @param m Pointer to message buffer.
	 * @param l_tag Tag to check the message.
	 * @ param src return the sender of the message.
	 * @return size of the message received or EOW.
	 */
	int probeAndReadStream(MsgLS& m, const int l_tag);

private:
	/*! Number of source instances with stream still opened. */
	int32_t sourceInst_open;
	/*! Initial rank for source filter of the stream. */
	int32_t sourceInst_init_rank;
	/*! Number of ranks, or instances, on source filters. */
	int32_t sourceInst_size;
	/*! Initial rank for destination filter of the stream.  */
	int32_t destInst_init_rank;
	/*! Number of ranks, or instances, on destination filters. */
	int32_t destInst_size;

	/*! States if this is a source instance or a destination instance in label stream comm world. */
	LabelStream::InstType type;

	LabelStream();
};


#endif /* LABELSTREAM_H_ */
