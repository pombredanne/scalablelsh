/*
 * LabelStreamFC.h
 *
 *  Created on: Feb 26, 2013
 *      Author: uq49
 */

#ifndef LABELSTREAMFC_H_
#define LABELSTREAMFC_H_

#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <errno.h>
#include <pthread.h>
#include "Logger.h"
#include "LabelStream.h"

//#ifndef DEBUG_P_LSFC
//#define DEBUG_P_LSFC
//#endif

#define DEF_LIST_MIN_SIZE 1000
#define DEF_N_MSGS_PER_REQ 100

/**
 *  \class LabelStreamFC
 *  \brief Class which implements LabelStream with flow control(FC).
 *
 *  \attention Class' methods are not thread safe. When using multiple threads
 *  accessing any object of this class must use mutexes.
 *
 *  This class implements the LabelStream with flow control (FC) by accounting
 *  the request sent and received by stream. According to the number request made by receiver
 *  instance the sender knows if it can send more messages or not.
 *  It has a thread just to receive the messages and store them in a list created for
 *  which stream.
 *
 */
class LabelStreamFC: public LabelStream {
private:

	/** The instance tag for internal communication in flow control.
	 * It will be the stream's tag plus TAG_MSG_MAX_EXCLUSIVE.*/
	int lsfc_tag;
	/** Minimum number that the list of messages can have before requesting for more. */
	uint32_t LIST_MIN_SIZE;
	/** Number of messages to be requested for each source instance. */
	uint32_t N_MSGS_PER_REQ;

	/** Stores the number of messages requested.
	 * The same variable is used in the Producer (source) and
	 * in the Consumer (destination). Of course, each one has its
	 * own copy because they are in different process.
	 */
	int32_t numMsgsRequested;

	/**
	 * This is the thread which receives all the messages from the source instances
	 * and add them to the list.
	 */
	pthread_t * commThread;

	const static std::string classname;

#ifdef DEBUG_P_LSFC
	/** Log class */
	Logger * logCommThread;
	Logger * log;
#endif

	/**
	 * Message with l_msg_ninfo values to request
	 * more messages from the instance producer.
	 * The first value is the number of messages requested.
	 */
	class MsgRequest : public MsgLS {
	private:
		const static uint32_t l_msg_ninfo = 1;
		const static uint32_t l_sizeof_msg = l_msg_ninfo*sizeof(uint32_t);
		const static uint32_t l_header_pos_nreq = 0;
	protected:

	public:
		/**
		 * Constructor for MsgRequests. It has a fixed size with
		 * the number of messages requested. After the constructor
		 * you must call alocMsgLS().
		 */
		MsgRequest() : MsgLS()
		{}

		~MsgRequest()
		{}

		uint32_t getNumMsgsRequed() {
			return ((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo + l_header_pos_nreq];
		}

		void setNumMsgsRequed(uint32_t value) {
			((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo + l_header_pos_nreq] = value;
		}

		/**
		 * Allocates the message's memory calling MsgLS::alocMsgLs().
		 */
		void allocMsgRequest() {
			this->allocMsgLS(l_sizeof_msg + MsgLS::getSizeofHeader());
		}
	};


	/**
	 * A thread safe list to receive the messages.
	 * One thread will receive the messages and put them on the list.
	 * When the instance's threads call readBuffer they will receive a
	 * message taken from the list.
	 */
	template<class T>
	class ListMsgFC  {
	private:
		pthread_mutex_t * list_mutex;
		std::list<T> * internList;

		void CreateList() {
			internList = new std::list<T>();

			/* Create Mutex. */
			list_mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));
			if(!list_mutex) {
				std::cerr << "[LabelStreamFC][ListMsgFC][CreateList] ERROR Could not allocate tasklist_mutex." << std::endl;
				exit(EXIT_FAILURE);
			}
			int status = pthread_mutex_init(list_mutex,NULL);
			if(status != 0) {
				std::cerr << "[LabelStreamFC][ListMsgFC][CreateList] ERROR tasklist_mutex could not be started. The error was " << strerror(status) << "." << endl;
				exit(1);
			}
			/* ************ */
		}
	public:
		ListMsgFC () {
			CreateList();
		}

		~ListMsgFC() {
			if(!internList->empty()) {
				cerr << "[LabelStreamFC][ListMsgFC] Error finishing the ListMsgFC with the list not empty!" << endl;
			}
			internList->clear();
			delete internList;
			pthread_mutex_destroy(list_mutex);
			free(list_mutex);
		}

		bool empty() const {
			bool ret;
			pthread_mutex_lock(list_mutex);

			ret = internList->empty();

			pthread_mutex_unlock(list_mutex);
			return ret;
		}

		T& front() {
			pthread_mutex_lock(list_mutex);

			T& ret = internList->front();

			pthread_mutex_unlock(list_mutex);
			return ret;
		}

		void push_back(const T& val) {
			pthread_mutex_lock(list_mutex);

			internList->push_back(val);

			pthread_mutex_unlock(list_mutex);
		}

		void pop_front() {
			pthread_mutex_lock(list_mutex);

			internList->pop_front();

			pthread_mutex_unlock(list_mutex);
		}

		size_t size() const {
			size_t ret;
			pthread_mutex_lock(list_mutex);

			ret = internList->size();

			pthread_mutex_unlock(list_mutex);
			return ret;
		}
	};


	/**
	 * Vector of lists of messages. Each stream will
	 * have one entry on this vector, which will have
	 * its messages received by the communication thread.
	 */
	static std::vector<LabelStreamFC::ListMsgFC<MsgLS *> *> vec_msgs_lists;

	/**
	 * Map to <stream tag -> position in vector messages>
	 */
	//static std::map<int, int> map_msgs_tags;
	static std::vector<int> vec_msgs_tags;

	/**
	 * Store the pointer for each LSFC object to manage the stream.
	 */
	static std::vector<LabelStreamFC *> vec_objs_pointers;

    /**
     * This vector has one mutex for each LSFC object created. Each mutex is used by the object to a
	 * access its list of messages and by communication thread to push messages on the lists.
	 */
	static std::vector<pthread_mutex_t > vec_msgListMutexFC;

	/**
	 * Mutex to guarantee that only one thread will access the network functions (i.e MPI::Send).
	 */
	static pthread_mutex_t globalCommMutexFC;

	/**
	 * Controls the current number of LSFC instantiated objects.
	 */
	static int numTotalObj;

	/**
	 * Number of dest streams.
	 */
	static int nStreamsDest;

	/**
	 * States if the comm threads has been created.
	 */
	static bool commThreadCreated;

	/**
	 * Position of the list of this stream in the vec_msgs_lists.
	 */
	int vec_pos;

	/**
	 * States if this object is the last stream to be created, if true
	 * then the communication thread can be created.
	 */
	bool lastStream;

    /**
     * Called on the constructor to initialize everything.
     */
    void CreateLSFC();

    /**
     * Create the thread for communication.
     */
    void createCommThread();

    /**
     * Destroy the thread for communication.
     */
    void destroyCommThread();

    /**
     * Decide if the Producer (source) may send more messages.
     * @return true or false.
     */
    bool canSendMessages();

    /**
     * Decide if the Consumer (destination) may request more messages.
     * @return true or false.
     */
    bool canRequestMessages();

    /**
     * Update the numMsgsRequested.
     * @param msgsToSent
     */
    void updateNumMsgsReqsed(uint32_t msgsToSent);

    /**
     * When a message is sent by the Producer (source) or
     * a message is received by the Consumer (destination)
     * the number of messages requested is decremented.
     */
    void decrementeMsgsReqsed();

    /**
     * Checks it the stream is closed by looking for the
     * number of messages in the list and the number of source instances
     * in the stream.
     */
    bool isEndOfWork();

    /**
     * Send a request for more
     * messages to source filter.
     */
    void sendRequest();

    /**
     * Probe and Read request messages.
     * These messages only use
     * a specific message tag. Therefore, this function
     * do not care about controlling messages such as EOW.
     *
     * @param m Message buffer.
     */
    void probeAndRecvRequestFC(MsgRequest &m);

public:
    /**
   	 * LabelStreamFC constructor.
   	 * It has the flow control between the two instance filters.
   	 * @param oF_i_r Initial rank of source filter instances.
   	 * @param oF_n Number of source filter instances.
   	 * @param dF_i_r Initial rank of destination filter instances.
   	 * @param dF_n Number of destination filter instances.
   	 * @param tag LabelStream unique identifier.
   	 * @param lastStream_in States that if this object is the last stream
   	 * to be created. Then, the communication thread can be created.
   	 */
	LabelStreamFC(int32_t oF_i_r,
				int32_t oF_n,
				int32_t dF_i_r,
				int32_t dF_n,
				int tag,
				bool lastStream_in);

	/**
   	 * LabelStreamFC constructor.
   	 * It has the flow control between the two instance filters.
   	 * @param oF_i_r Initial rank of source filter instances.
   	 * @param oF_n Number of source filter instances.
   	 * @param dF_i_r Initial rank of destination filter instances.
   	 * @param dF_n Number of destination filter instances.
   	 * @param tag LabelStream unique identifier.
	 * @param l_min_size Minimum number of messages in the list before request more.
	 * @param n_msg_req Number of messages to be requested for each source.
	 * @param lastStream_in States that if this object is the last stream
   	 * to be created. Then, the communication thread can be created.
	 */
	LabelStreamFC(int32_t oF_i_r,
				int32_t oF_n,
				int32_t dF_i_r,
				int32_t dF_n,
				int tag,
				uint32_t l_min_size,
				uint32_t n_msg_req,
				bool lastStream_in);

	/**
	 * LabelStreamFC destructor.
	 */
	virtual ~LabelStreamFC();

    /**
     * Send buffer message.
     * @param b MsgAnn object.
     */
	void sendBuffer(MsgLS& b);

	/**
 	 * Read buffer with previously allocated message.
	 * Handle the EOWs received from source filter's instances.
	 * If the message is an EOW, counts it and waits the next message.
	 * @param b Message buffer (preveously allocated).
	 * @param source
	 * @return EOW or ORDINARY_MSG.
	 */
	int readBuffer(MsgLS * b, const int source);

	/**
	 * Checks whether the stream is closed, if not check if there is any message to receive.
	 * @param src Returns by reference the sender of the message.
	 * @return EOW if the stream is closed or the sized of the message to be received.
	 */
	int probeStream(int &src);

	/**
	 * Probe the stream, check message size, allocate the message and receive the message.
	 * @param m Message buffer not allocated, it will be allocated inside after the probe knows the message size.
	 * @return EOW if the stream is closed or the sized of the message to be received.
	 */
	int probeAndReadStream(MsgLS * m);

	/**
	 * The thread exclusive for communication will
	 * call this method.
	 * It will receive the messages and copy them to
	 * the message list.
	 */
	void commProccessThread(int tid);

	/**
	 *
	 * @return
	 */
	uint32_t getListMinSize() const;

	/**
	 *
	 * @param listMinSize
	 */
	void setListMinSize(uint32_t listMinSize);

	/**
	 *
	 * @return
	 */
	uint32_t getMsgsPerReq() const;

	/**
	 *
	 * @param msgsPerReq
	 */
	void setMsgsPerReq(uint32_t msgsPerReq);
};

#endif /* LABELSTREAMFC_H_ */
