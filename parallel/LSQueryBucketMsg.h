/**
 * @file
 * LSDataPointBucketMsg.h
 *
 *  Created on: Sep 18, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef LSQUERYBUCKETMSG_H_
#define LSQUERYBUCKETMSG_H_

#include "LabelStream.h"

/**
 * \class MsgQyBkt
 * \brief Messages from QueryReceiver (query) to BucketsIndex with probes for query's points and bucket_key.
 *
 * The message is composed by the label (bucket_key),
 * the query Id in header control field, the probe identifier,
 * the elements of bucket_key and, lastly, the elements of query.
 *
 * Summarizing is this:
 * @code
 * [[MsgLS label <bucket_key (int32_t)> + <query Id>] + <chain key> + <probe id> + <bucket_key (uint32_t)> + <query in uint32_t>]
 * @endcode
 */
class MsgQyBkt : public MsgLS {
private:
	uint32_t q_size, n_probes_max;

	uint32_t * queryId;
	uint32_t * n_probes;
	T_Q  * query;
	T_KEY * chain_key;
	T_KEY * bucket_key;
	T_HTI * probeId;

	void setUpMessage(const uint32_t q_size, const uint32_t n_Ls_max) {
		if(MsgLS::getMsg() == NULL) {
			cerr << "[MsgBktPt][setUpMessage] ERROR msg not allocated. " << endl;
			exit(EXIT_FAILURE);
		}
		uint32_t prev_bytes = MsgLS::sizeof_header;
		queryId = (uint32_t *) ( ((uint8_t *) MsgLS::getMsg()) +  prev_bytes);

		prev_bytes += sizeof(uint32_t);
		n_probes = (uint32_t *) ( ((uint8_t *) MsgLS::getMsg()) + prev_bytes );

		prev_bytes += sizeof(uint32_t);

		/** Set up the pointers of the message.*/
		query = (T_Q *) ( ((uint8_t *) MsgLS::getMsg()) + prev_bytes );

		prev_bytes += q_size*sizeof(T_Q);
		chain_key = (T_KEY *) ( ((uint8_t *) MsgLS::getMsg()) + prev_bytes);

		prev_bytes += n_Ls_max*sizeof(T_KEY);
		bucket_key = (T_KEY *) ( ((uint8_t *) MsgLS::getMsg()) + prev_bytes);

		prev_bytes += n_Ls_max*sizeof(T_KEY);
		probeId = (T_HTI * ) ( ((uint8_t *) MsgLS::getMsg()) + prev_bytes);
	}

public:
	/**
	 * MsgQyBkt constructor.
	 * Receives the number of elements in query to allocate
	 * the message. It also allocates size of T_HTI, T_CK and T_BK to store the probe id,
	 * chain key and bucket key, respectively.
	 *
	 * @param q_size Number of elements in query.
	 * @param n_L number of probes.
	 */
	MsgQyBkt(const uint32_t q_size_in, const uint32_t n_probes_max_in) :
		MsgLS(sizeof(uint32_t)+sizeof(uint32_t)+(q_size_in*sizeof(T_Q))+(n_probes_max_in*(sizeof(T_KEY)+sizeof(T_KEY)+sizeof(T_HTI)))), q_size(q_size_in), n_probes_max(n_probes_max_in)
	{
		setUpMessage(q_size, n_probes_max);
		resetNProbes();
	}

	/**
	 *
	 */
	MsgQyBkt() : MsgLS(), q_size(0), n_probes_max(0), queryId(NULL), n_probes(NULL), query(NULL),
						chain_key(NULL), bucket_key(NULL), probeId(NULL)
	{}

	MsgQyBkt(const MsgQyBkt &in) : MsgLS(in), q_size(in.q_size), n_probes_max(in.n_probes_max){
		setUpMessage(q_size, n_probes_max);
	}

	/**
	 * MsgQyBkt destructor.
	 */
	virtual ~MsgQyBkt()
	{
		q_size = 0; n_probes_max = 0;
		queryId = NULL; n_probes = NULL; query = NULL;
		chain_key = NULL; bucket_key = NULL; probeId = NULL;
	}

	/**
	 * Set up the message.
	 */
	void deserialize(const uint32_t q_size_in) {
		n_probes_max = getMaxNumberOfProbes(getSizeMsg(), q_size_in);
		setUpMessage(q_size_in, n_probes_max);
	}

	/**
	 * Get the pointer to query buffer in the message.
	 * @return the pointer to query buffer in the message.
	 */
	T_Q * getQuery() const {
		return query;
	}

	uint32_t getQueryId() const {
		return *queryId;
	}

	void setQueryId(const uint32_t qId) {
		*queryId = qId;
	}

	uint32_t getNProbes() const {
		return *n_probes;
	}

	void incNProbes() {
		(*n_probes)++;
	}

	void resetNProbes() {
		*n_probes = 0;
	}

	/**
	 * Get the bucket_key in the message.
	 * @return the bucket_key in the message.
	 */
	T_KEY getBucketKey(const uint32_t i_Probe) const {
		return bucket_key[i_Probe];
	}

	/**
	 * Set the bucket_key to current message.
	 * @param i_Probe ith Hashtable value to be set.
	 * @param b_k bucket_key to current message.
	 */
	void setBucketKey(const uint32_t i_Probe, T_KEY b_k) {
		bucket_key[i_Probe] = b_k;
	}

	/**
	 * Get the chain_key to current message.
	 * @return chain_key to current message.
	 */
	T_KEY getChainKey(const uint32_t i_Probe) const {
		return chain_key[i_Probe];
	}

	/**
	 * Set the chain_key to current message.
	 * @param c_k chain_key to current message.
	 */
	void setChainKey(const uint32_t i_Probe, T_KEY c_k) {
		chain_key[i_Probe] = c_k;
	}

	/**
	 * Get hash table identifier dividing the probeId by number of probes per hash table.
	 * It is used to build the hash key.
	 * @param i_Probe index of the probe in the message.
	 * @param T number of probes per hash table.
	 * @return probe identifier (used to build the hash key).
	 */
	T_HTI getHashTableId(const uint32_t i_Probe, const uint32_t T) const {
		return probeId[i_Probe]/T;
	}

	/**
	 * Get probe identifier used to build the hash key.
	 * @return probe identifier (used to get the T id and hash table id).
	 */
	T_HTI getProbeId(const uint32_t i_Probe) const {
		return probeId[i_Probe];
	}

	/**
	 * Set probe identifier used to build the hash key.
	 * @param htid  identifier (used to build the hash key).
	 */
	void setProbeId(const uint32_t i_Probe, T_HTI probeid) {
		probeId[i_Probe] = probeid;
	}

	static uint32_t getMaxNumberOfProbes(const uint32_t mem_msg_size, const uint32_t q_size) {
		uint32_t n_Ls = 0;
		if(mem_msg_size > MsgLS::sizeof_header) {
			n_Ls = ( mem_msg_size - MsgLS::sizeof_header - (2*sizeof(uint32_t)) - (q_size*sizeof(T_Q)) ) / ( sizeof(T_KEY) + sizeof(T_KEY) + sizeof(T_HTI) );
		}
		return n_Ls;
	}

};

#endif /* LSQUERYBUCKETMSG_H_ */
