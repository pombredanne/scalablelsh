
#include <mpi.h>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <getopt.h>

#include "FilterInstance.h"
#include "InputReader.h"
#include "BucketsIndex.h"
#include "DataPoints.h"
#include "QueryReceiver.h"
#include "Aggregator.h"
#include "LabelStreamFC.h"
using namespace std;


#define TAG_IR2BI 1
#define TAG_IR2DP 2
#define TAG_QR2BI 3
#define TAG_BI2DP 4
#define TAG_DP2AG 5

void usage () {
	cout << "Usage: mpirun ./lsh "<< endl;
	cout << "  -h, --help  prints this help usage;" << endl;
	cout << "  -i, --fir   <input reader instances>   default 1;" << endl;
//		cout << "-r, --fqr <query receiver instances> " << endl;
	cout << "      --firck <Enable chain_key as the point label for LabelStream to DataPoints, otherwise will be the point id>" << endl;
	cout << "      --firzo <Enable z-order as the point label for LabelStream to DataPoints, otherwise will be the point id>" << endl;
	cout << "      --firzow <zow to be used only in the zorder computation, default is the same as w>" << endl;
	cout << "  -b, --fbi   <buckets index instances>  default 1;" << endl;
	cout << "      --fbith <num threads for each buckets index instance> default uses all cores;" << endl;
	cout << "  -c, --fdp   <data points instances>    default 1;" << endl;
	cout << "      --fdpth <num threads for each data points instance>   default uses all cores;" << endl;
	cout << "  -a, --fag   <aggregator instances>     default 1; " << endl;
	cout << "  -p <pointsFileName prefix> -q <queryFileName prefix> -o <outputFileName> -k <kValue, default 10> -L <#HashTables, default 1> " << endl;
	cout << "      --fclm  <Flow Control minimum number on the list of messages can have before requesting for more, default 1000>" << endl;
	cout << "      --fcnm  <Flow Control number of messages to be requested for each source instance, default 100>" << endl;
	cout << "      --nofc  <DISABLE Flow Control>" << endl;
	cout << "  -m <#HashFunctionPerFunctionL, default 2> -d <#dimensions> -w <w, default 4> -r <radius, default 1.0> -z <queryRate, default 1000>" << endl;
	cout << "  -T <number of probes per hash table in multiprobe, greater than 1 means multiprobe will be used>" << endl;
	exit(1);
}

int main(int argc, char ** argv) {
	string pointsFileName;
	string queryFileName;
	string outputFileName;
    char hostname[256];


	int fir = 1, fbi = 1, fbith = -1, fdp = 1, fdpth = -1, fag = 1, L = 1, k = 10, m = 2, d = NOT_DEFINED, w = 4, T = 0;
	int w_zorder = w; // The default is the same as w for the program.
	bool nofc = false;
	bool irck = false;
	bool irzo = false;
	uint32_t fclm = -1, fcnm = -1;
	int rank, size;
	float R = 1.0;
	unsigned queryRate = 1000;

	FilterInstance * inst = NULL;


	MPI_Init (&argc, &argv);	/* starts MPI */
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
	MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */

    gethostname(hostname, sizeof(hostname));

	static struct option long_options[] = {
			{"help"  ,required_argument  ,0  ,'h'},
			{"fir"   ,required_argument  ,0  ,'i'},
//			{"fqr"   ,required_argument  ,0  ,'r'},
			{"firck" ,no_argument        ,0  ,11},
			{"firzo" ,no_argument        ,0  ,12},
			{"firzow",required_argument  ,0  ,13},
			{"fbi"   ,required_argument  ,0  ,'b'},
			{"fbith" ,required_argument  ,0  , 4},
			{"fdp"   ,required_argument  ,0  ,'c'},
			{"fdpth" ,required_argument  ,0  , 6},
			{"fag"   ,required_argument  ,0  ,'a'},
			{"fclm"  ,required_argument  ,0  , 8},
			{"fcnm"  ,required_argument  ,0  , 9},
			{"nofc"  ,no_argument        ,0  , 10},
			{0,    0,                0   , 0}
	};

	int option_index;
	int optc;
	while ( ( optc = getopt_long(argc,
								argv,
								"hi:I:b:B:c:C:a:A:p:P:q:Q:k:K:l:L:o:O:m:M:d:D:w:W:r:R:T:Z:z:",
								long_options,
								&option_index) ) != -1)
	{
		switch (optc)
		{
			case 'h':
			case 'H':
				if(rank == 0) {
					usage();
				}
				MPI_Finalize();
				exit(EXIT_SUCCESS);
			case 'i':
 			case 'I':
				if (optarg != NULL) {
					//Number of instances of filter InputReader and QueryReceiver
					fir = atoi(optarg);
//					cout<<"a="<<a<<endl;
				}
				break;
/*			case 'r':
 			case 'R':
				if (optarg != NULL) {
					//Number of instances of filter QueryReceiver
					fqr = atoi(optarg);
//					cout<<"a="<<a<<endl;
				}
				break;*/
 			case 11:
 				irck = true;
 				break;
 			case 12:
 				irzo = true;
 				break;
 			case 13:
				if (optarg != NULL) {
					w_zorder = atoi(optarg);
				}
				break;
			case 'b':
			case 'B':
				if (optarg != NULL) {
					//Number of instances of filter BucketsIndex
					fbi = atoi(optarg);
//					cout<<"b="<<b<<endl;
				}
				break;
			case 4:
				if (optarg != NULL) {
					//Number of threads used by each instance of BucketsIndex
					fbith = atoi(optarg);
					//cout << "fbith " << fbith << endl;
				}
				break;
			case 'c':
			case 'C':
				if (optarg != NULL) {
					//Number of instances of filter DataPoints
					fdp = atoi(optarg);
//					cout<<"b="<<b<<endl;
				}
				break;
			case 6:
				if (optarg != NULL) {
					//Number of threads used by each instance of DataPoints
					fdpth = atoi(optarg);
					//cout << "fdpth " << fdpth << endl;
				}
				break;
			case 8:
				if (optarg != NULL) {
					//Number of threads used by each instance of DataPoints
					fclm = atoi(optarg);
					//cout << "fdpth " << fdpth << endl;
				}
				break;
			case 9:
				if (optarg != NULL) {
					//Number of threads used by each instance of DataPoints
					fcnm = atoi(optarg);
					//cout << "fdpth " << fdpth << endl;
				}
				break;
			case 10:
				//States that no flow control will be used.
				nofc = true;
				//cout << "fdpth " << fdpth << endl;
				break;
			case 'a':
			case 'A':
				if (optarg != NULL) {
					//Number of instances of filter Aggregator
					fag = atoi(optarg);
//					cout<<"b="<<b<<endl;
				}
				break;
			case 'p':
			case 'P':
				if (optarg != NULL) {   
					//le o nome do arquivo de entrada que contem os pontos 
					pointsFileName.append(optarg);
//					cout<<pointsFileName<<endl;
				}
				break;	
			case 'q':
			case 'Q':
				if (optarg != NULL) {   
					//le o nome do arquivo de entrada com o ponto de consulta
					queryFileName.append(optarg);
//					cout<<queryFileName<<endl;
				}
				break;
			case 'o':
			case 'O':
				if (optarg != NULL) {
					//le o nome do arquivo de entrada com o ponto de consulta
					outputFileName.append(optarg);
//					cout<<outputFileName<<endl;
				}
				break;
			case 'k':
			case 'K':
				if (optarg != NULL) {
					//le o numero de vizinhos a serem retornados pelo programa
					k = atoi(optarg);	
//					cout<<"k="<<k<<endl;
				} 
				break;
			case 'l':
			case 'L':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					L = atoi(optarg);
//					cout<<"L="<<L<<endl;
				}
				break;
			case 'm':
			case 'M':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					m = atoi(optarg);
//					cout<<"m="<<m<<endl;
				}
				break;
			case 'd':
			case 'D':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					d = atoi(optarg);
//					cout<<"d="<<d<<endl;
				}
				break;
			case 'w':
			case 'W':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					w = atoi(optarg);
//					cout<<"w="<<w<<endl;
				}
				break;
			case 'r':
			case 'R':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					R = atof(optarg);
					cout<<"R="<<R<<endl;
				}
				break;
			case 'z':
			case 'Z':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					queryRate = atoi(optarg);
					cout<<"queryRate="<<queryRate<<endl;
				}
				break;
			case 'T':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					T = atoi(optarg);
					cout<<"multiprobe="<<T<<endl;
				}
				break;
			default:
				std::cout << "ERROR!! Unrecognized parameter" <<std::endl;
				usage();
				exit(EXIT_FAILURE);
				break;
		}
	}

	/* Check if received the dimensions parameter. */
	if (d == NOT_DEFINED) {
		if (rank == 0) {
			cerr << "ERROR!! The parameter d (dimensions of database) must be received as parameter. " << endl;
		}
		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	/* Check if received the points file name, queries file name and output file name. */
	if (pointsFileName.empty() || queryFileName.empty() || outputFileName.empty()) {
		if (rank == 0) {
			cerr << "ERROR!! The points database file name (-p), queries file name (-q), and output file name must be received as parameter. " << endl;
		}
		MPI_Finalize();
		exit(EXIT_FAILURE);
	}

	/* If irck and irzo are true we chose as default irck. */
	if(irck && irzo) {
		irzo = false;
	}

	/* Prints all the parameters used. */
	if(rank == 0) {
		cout << "./main -i " << fir << " --firck " << irck << " --firzo " << irzo << " --firzow " << w_zorder << " -b " << fbi << " --fbith " << fbith << " -c " << fdp << " --fdpth " << fdpth << " -a " << fag;
		cout << " -p " << pointsFileName << " -q " << queryFileName << " -o " << outputFileName;
		cout << " -k " << k << " -L " << L << " --fclm " <<  fclm << " --fcnm " << fcnm << " --nofc " << nofc << " -m " << m << " -d " << d << " -w " << w;
		cout << " -r " << R << " -z " << queryRate << " -T " << T << endl;
	}

	/* Check if the number of MPI process is at least the number of instances needed. */
	if (fir+fbi+fdp+fag > size) {
		cerr << " more filter instances than total mpi process.  " << endl;
		exit(1);
	}
	
	/* Start executing the filters. */
	if (rank < fir) {
		cout << "Creating InputReader   - rank " << rank << " - " << fir << " instances - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;

		/** Each file has the prefix plus the instance rank. */
		stringstream file_name_p;
		file_name_p << pointsFileName << "_" << rank;

		stringstream file_name_q;
		file_name_q << queryFileName << "_" << rank;

		FileIn f_IO(file_name_p.str(), file_name_q.str());

		LabelStream * ir2bi;
		LabelStream * ir2dp;
/*		if(nofc) {*/
			ir2bi = new LabelStream(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir, fbi, TAG_IR2BI);
			ir2dp = new LabelStream(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir+fbi, fdp, TAG_IR2DP);
/*		} else {
			ir2bi = new LabelStreamFC(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir, fbi, TAG_IR2BI, false);
			ir2dp = new LabelStreamFC(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir+fbi, fdp, TAG_IR2DP, true);
		}*/

		inst = new InputReader(rank, rank, fir, ir2bi, ir2dp, f_IO, L, m, d, w, w_zorder, R, irck, irzo);

		delete ir2bi;
		delete ir2dp;
		delete inst;

		cout << "Finished InputReader   - rank " << rank << " - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;

		cout << "Creating QueryReceiver - rank " << rank << " - " << fir << " instances - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;

		LabelStream * qr2bi;
/*		if(nofc) {*/
			qr2bi = new LabelStream(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir, fbi, TAG_QR2BI);
/*		} else {
			qr2bi = new LabelStreamFC(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir, fbi, TAG_QR2BI, true);
		}*/

		inst = new QueryReceiver(rank, rank, fir, qr2bi, f_IO, L, m, d, w, T, R, queryRate);

		delete qr2bi;
		delete inst;

		cout << "Finished QueryReceiver - rank " << rank << " - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;
	} else if (rank >= fir && rank < fir+fbi) {
		cout << "Creating BucketsIndex  - rank " << rank << " - " << fbi << " instances - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;

		LabelStream * ir2bi;
		LabelStream * qr2bi;
		LabelStream * bi2dp;
/*		if(nofc) {*/
			ir2bi = new LabelStream(0, fir, LabelStream::LS_NO_DEST,LabelStream::LS_NO_DEST, TAG_IR2BI);
			qr2bi = new LabelStream(0, fir, LabelStream::LS_NO_DEST,LabelStream::LS_NO_DEST, TAG_QR2BI);
			bi2dp = new LabelStream(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir+fbi, fdp, TAG_BI2DP);
/*		} else {
			ir2bi = new LabelStreamFC(0, fir, LabelStream::LS_NO_DEST,LabelStream::LS_NO_DEST, TAG_IR2BI, false);
			qr2bi = new LabelStreamFC(0, fir, LabelStream::LS_NO_DEST,LabelStream::LS_NO_DEST, TAG_QR2BI, false);
			bi2dp = new LabelStreamFC(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir+fbi, fdp, TAG_BI2DP, true);
		}*/

		inst = new BucketsIndex(rank, rank - fir, fbi, ir2bi, qr2bi, bi2dp, k, L, m, d, T, fbith);

		delete ir2bi;
		delete qr2bi;
		delete bi2dp;
		delete inst;

		cout << "Finished BucketsIndex  - rank " << rank << " - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;
	} else if (rank >= fir+fbi && rank < fir+fbi+fdp) {
		cout << "Creating DataPoints    - rank " << rank << " - " << fdp << " instances - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;

		LabelStream * ir2dp;
		LabelStream * bi2dp;
		LabelStream * dp2ag;
/*		if(nofc) {*/
			ir2dp = new LabelStream(0, fir, LabelStream::LS_NO_DEST, LabelStream::LS_NO_DEST, TAG_IR2DP);
			bi2dp = new LabelStream(fir, fbi, LabelStream::LS_NO_DEST,LabelStream::LS_NO_DEST, TAG_BI2DP);
			dp2ag = new LabelStream(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir+fbi+fdp, fag, TAG_DP2AG);
/*		} else {
			ir2dp = new LabelStreamFC(0, fir, LabelStream::LS_NO_DEST, LabelStream::LS_NO_DEST, TAG_IR2DP, false);
			bi2dp = new LabelStreamFC(fir, fbi, LabelStream::LS_NO_DEST,LabelStream::LS_NO_DEST, TAG_BI2DP, false);
			dp2ag = new LabelStreamFC(LabelStream::LS_NO_SOURCE, LabelStream::LS_NO_SOURCE, fir+fbi+fdp, fag, TAG_DP2AG, true);
		}*/

		inst = new DataPoints(rank, rank - (fir+fbi), fdp, ir2dp, bi2dp, dp2ag, L, m, d, k, fdpth);

		delete ir2dp;
		delete bi2dp;
		delete inst;

		cout << "Finished DataPoints    - rank " << rank << " - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;
	} else if ( rank >= fir+fbi+fdp && rank < fir+fbi+fdp+fag) {
		cout << "Creating Aggregator    - rank " << rank << " - " << fag << " instances - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;

		int instRank = rank - (fir+fbi+fdp);
		stringstream outFile_stream;
		outFile_stream << outputFileName << "_" << instRank;
		FileOut f_IO(outFile_stream.str());

		LabelStream * dp2ag;
/*		if(nofc) {*/
			dp2ag = new LabelStream(fir+fbi, fdp, LabelStream::LS_NO_DEST, LabelStream::LS_NO_DEST, TAG_DP2AG);
/*		} else {
			dp2ag = new LabelStreamFC(fir+fbi, fdp, LabelStream::LS_NO_DEST, LabelStream::LS_NO_DEST, TAG_DP2AG, true);
		}*/

		inst = new Aggregator(rank, instRank, fag, dp2ag, f_IO, L, T, k);

		delete dp2ag;
		delete inst;

		cout << "Finished Aggregator    - rank " << rank << " - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;
	} else { 
		cout <<  "Creating lazy process - rank " << rank << " - pid " << getpid() << " - " << hostname << " - " << Util::getStringTime() << endl;
		inst = NULL;
	}

	MPI_Finalize();
}

