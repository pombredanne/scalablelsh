/**
 * @file
 * LSAnnMsg.h
 *
 *  Created on: Sep 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef LSANNMSG_H_
#define LSANNMSG_H_

#include "LabelStream.h"
#include "MsgLS.h"


/**
 * \class MsgAnn
 * \brief Message between DataPoints and Aggregator.
 * Message with points' id and distance.
 *
 * The message contains all points' ids and then all the points' distance.
 * So, to access ith point in the message just do
 * qid = pointsId[i] and dist = pointsDist[i].
 * HeaderControl field of the message holds the number of DataPoints
 * instances which received at least on point from query's bucket.
 *
 * Summarizing is this:
 * @code
 * [[MsgLS label <label query Id> + <NULL>] + <number of DP instances in uint32_t> + <hash table id in uint32_t> + <bucket's points in uint32_t>]
 * @endcode
 *
 */
class MsgAnn : public MsgLS {
private:
	/*! Each points has an identification and a distance. */
	T_P * pointsId;
	T_D * pointsDist;

	/** Header info besides the MsgLS header. */
	/*! Number of info in the local header. */
	const static uint32_t l_header_ninfo = 2;
	/*! Size of the label in 4 bytes and control close stream in other 4. */
	const static uint32_t l_sizeof_header = l_header_ninfo*sizeof(uint32_t);
	/*! Positions in header for number of DataPoints instances which will receive at least one point. */
	const static uint32_t l_header_pos_nDpInst = 0;
	/*! Position in header for Hash table id used in query. */
	const static uint32_t l_header_pos_probeId = 1;
	/*! Size of all headers in the message including label stream and local.*/
	const static uint32_t global_sizeof_header = l_sizeof_header + MsgLS::sizeof_header;

	/**
	 * Get value in local header message.
	 * @param l_hd_pos Position in local header.
	 * @return return the value for the position.
	 */
	uint32_t getLocalHeaderPos(uint32_t l_hd_pos) const {
		if(l_hd_pos >= l_header_ninfo) {
			cerr << "[MsgBktPt][getLocalHeaderPos] ERROR accessing invalid position " << l_hd_pos << endl;
			exit(1);
		}
		return ((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo + l_hd_pos];
	}

	/**
	 * Set value in local header message.
	 * @param l_hd_pos Position in local header.
	 * @param value Value assigned.
	 */
	void setLocalHeaderPos(uint32_t l_hd_pos, uint32_t value) const {
		if(l_hd_pos >= l_header_ninfo) {
			cerr << "[MsgBktPt][setLocalHeaderPos] ERROR accessing invalid position " << l_hd_pos << endl;
			exit(1);
		}
		((uint32_t *) MsgLS::getMsg())[MsgLS::header_ninfo + l_hd_pos] =  value;
	}

protected:
public:
	/**
	 * MsgAnn constructor.
	 * @param num_points Number of points within message.
	 */
	MsgAnn(uint32_t num_points_in) : MsgLS((num_points_in*sizeof(T_P))+(num_points_in*sizeof(T_D))+l_sizeof_header) {
		/* point equals header_size bytes after the start of the message. */
		deserialize(num_points_in);
	}

	/**
	 * MsgAnn constructor.
	 * It receives the total size of message including all headers.
	 * @param mem_size_msg Size of the message to be received in bytes.
	 */
	MsgAnn() : MsgLS(), pointsId(NULL), pointsDist(NULL) {
	}

	/**
	 * MsgAnn Destructor.
	 */
	virtual ~MsgAnn()
	{
		pointsId   = NULL;
		pointsDist = NULL;
	}

	/**
	 *
	 */
	void deserialize(uint32_t num_points) {
		if(MsgLS::getMsg() == NULL) {
			cerr << "[MsgAnn][deserialize] ERROR msg not allocated. " << endl;
			exit(EXIT_FAILURE);
		}

		if(num_points > 0) {
			pointsId = (T_P *) (((uint8_t *) MsgLS::getMsg()) + global_sizeof_header);
			pointsDist = (T_D *) (((uint8_t *) pointsId) + (num_points*sizeof(T_P)));
		}  else {
			pointsId = NULL;
			pointsDist = NULL;
		}
	}

	/**
	 * Get pointer to points' distance buffer.
	 * @return pointer to points' distance buffer.
	 */
	T_D* getPointsDist() const {
		return pointsDist;
	}

	/**
	 * Get pointer to points' id buffer.
	 * @return pointer to points' id buffer.
	 */
	T_P* getPointsId() const {
		return pointsId;
	}

	/**
	 * Given the message's size, it returns the number of points within the message.
	 * @return the number of points within the message.
	 */
	static uint32_t getNumPoints(const int32_t mem_msg_size) {
		uint32_t n_points = 0;
		if((uint32_t)mem_msg_size > global_sizeof_header) {
			n_points = (mem_msg_size - global_sizeof_header)/(sizeof(T_D) + sizeof(T_P));
		}
		return n_points;
	}

	/**
	 * Get the number of DataPoints' instances which will receive points from the bucket.
	 * It is the first four bytes after the LabelHeader and LabelControl.
	 * @return the number of DataPoints' instances which will receive points from the bucket.
	 */
	uint32_t getNDpInst() const {
		return getLocalHeaderPos(l_header_pos_nDpInst);
	}

	/**
	 * Set the number of DataPoints' instances which will receive points from the bucket.
	 * It is the first four bytes after the LabelHeader and LabelControl.
	 * @param dpInst the number of DataPoints' instances which will receive points from the bucket.
	 */
	void setNDpInst(uint32_t dpInst) {
		setLocalHeaderPos(l_header_pos_nDpInst, dpInst);
	}

	/**
	 * Get the hash table id for this query.
	 * It is the second four bytes after the LabelHeader and LabelControl.
	 * @return the hash table id.
	 */
	uint32_t getHashTableId(const uint32_t T) {
		return getLocalHeaderPos(l_header_pos_probeId)/T;
	}

	/**
	 * Get the probe id for this query.
	 * It is the second four bytes after the LabelHeader and LabelControl.
	 * @return the probe id.
	 */
	uint32_t getProbeId() {
		return getLocalHeaderPos(l_header_pos_probeId);
	}

	/**
	 * Set the hash table id for this query.
	 * It is the second four bytes after the LabelHeader and LabelControl.
	 * @param htid the hash table id.
	 */
	void setProbeId(uint32_t probeid) {
		setLocalHeaderPos(l_header_pos_probeId, probeid);
	}
};

#endif /* LSANNMSG_H_ */
