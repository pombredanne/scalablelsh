/**
 * @file
 * LSBucketMsg.h
 *
 *  Created on: Sep 10, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef LSBUCKETMSG_H_
#define LSBUCKETMSG_H_

#include "LabelStream.h"


/**
 * \class MsgBkt
 * \brief Message with chain_key and bucket_key.
 * This LabelStream send messages from InputReader
 * to BucketsIndex with
 * the point's or querie's bucket_key (and their ids)
 * to be inserted in a bucket.
 *
 *
 * The message is composed by the label (bucket_key), header
 * control empty, the chain_key and bucket_key.
 *
 * Summarizing is this:
 * @code
 * [[MsgBkt label <bucket_key (int32_t)> + <empty>] + <chain key> + <bucket_key (uint32_t)>]
 * @endcode
 */
class MsgBkt : public MsgLS {
private:
	/*! Pointer to get points id. */
	uint32_t * pointsId;
	/*! Pointer to get chain_key. */
	T_KEY * chain_key;
	/*! Pointer to get bucket_key. */
	T_KEY * bucket_key;
	/*! Pointer to get hashtTableId. */
	T_HTI * hashTableId;
	/*! Point label key to DP. */
	T_KEY * label_key;
	/*! Last position in the message occupied by a point. */
	uint32_t last_pos;
	/*! Maximum number of points this message can hold. */
	uint32_t max_pts;

	void setUpMessage(const uint32_t np_max) {
		/** */
		pointsId = (uint32_t *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header );

		/** Points chain_key to address. */
		chain_key = (T_KEY *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header + ( sizeof(uint32_t) * np_max ) );

		/** Points to header_size bytes after the start of the message plus the chain_key. */
		bucket_key = (T_KEY *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header + ( ( sizeof(uint32_t) + sizeof(T_KEY) ) * np_max ) );

		/** Points to header_size bytes after the start of the message plus the chain_key  and bucket_key. */
		hashTableId = (T_HTI *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header + ( ( sizeof(uint32_t) + sizeof(T_KEY) + sizeof(T_KEY) ) * np_max ) );
		
		label_key = (T_KEY *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header + ( ( sizeof(uint32_t) + sizeof(T_KEY) + sizeof(T_KEY) + sizeof(T_HTI) ) * np_max ) );
	}
protected:
public:
	/**
	 * MsgBkt constructor.
	 * Creates the position for chain_key, bucket_key and hashTableId.
	 */
	MsgBkt(const uint32_t np_max) : MsgLS( ( sizeof(uint32_t)+sizeof(T_KEY)+sizeof(T_KEY)+sizeof(T_HTI)+sizeof(T_KEY) ) * np_max ), last_pos(0), max_pts(np_max) {
		setUpMessage(max_pts);
	}

	MsgBkt() : MsgLS(), pointsId(NULL), chain_key(NULL), bucket_key(NULL), hashTableId(NULL), label_key(NULL), last_pos(0), max_pts(0)
	{}

	MsgBkt(const MsgBkt &in) : MsgLS(in), last_pos(in.last_pos), max_pts(in.max_pts) {
		setUpMessage(max_pts);
	}

	/**
	 * MsgBkt destructor.
	 */
	virtual ~MsgBkt()
	{
		pointsId = NULL;
		chain_key = NULL;
		bucket_key = NULL;
		hashTableId = NULL;
		label_key = NULL;
	}

	void deserialize() {
		max_pts = getMaxNumberOfPoints(getSizeMsg());
		setUpMessage(max_pts);
	}

	/**
	 * Indicates whether the number of messages in this buffer is above.
	 * @return true if buffer is full, false otherwise.
	 */
	bool isBufferFull () const {
		return last_pos >= max_pts;
	}

	void flushBuffer() {
		last_pos = 0;
	}

	/**
	 *
	 * @return
	 */
	bool push_back(uint32_t id, T_KEY chk, T_KEY bk, T_HTI htid, T_KEY l_key) {
		if(last_pos < max_pts) {
			pointsId[last_pos] = id;
			chain_key[last_pos] = chk;
			bucket_key[last_pos] = bk;
			hashTableId[last_pos] = htid;
			label_key[last_pos] = l_key;
			last_pos++;
			return true;
		} else {
			return false;
		}
	}

	uint32_t getPointId(uint32_t i) const {
		return pointsId[i];
	}

	/**
	 * Get bucket_key.
	 * @return bucket_key.
	 */
	T_KEY getBucketKey(uint32_t i) const {
		return bucket_key[i];
	}

	/**
	 * Get the chain of this bucket.
	 * @return Chain_key of this bucket.
	 */
	T_KEY getChainKey(uint32_t i) {
		return chain_key[i];
	}

	/**
	 * Get hash table id of this message.
	 * @return hash table id.
	 */
	T_HTI getHashTableId(uint32_t i) const {
		return hashTableId[i];
	}

	T_KEY getLabelKey(uint32_t i) const {
		return label_key[i];
	}

	uint32_t getLastPos() const {
		return last_pos;
	}

	static uint32_t getMaxNumberOfPoints(const uint32_t mem_msg_size){
		uint32_t n_points = 0;
		if(mem_msg_size > MsgLS::sizeof_header) {
			n_points = ( mem_msg_size - MsgLS::sizeof_header ) / ( sizeof(uint32_t) + sizeof(T_KEY) + sizeof(T_KEY) + sizeof(T_HTI) + sizeof(T_KEY) );
		}
		return n_points;
	}
};

#endif /* LSBUCKETMSG_H_ */
