/*
 * LabelStreamFC.cpp
 *
 *  Created on: Feb 26, 2013
 *      Author: uq49
 */

#include "LabelStreamFC.h"


const std::string LabelStreamFC::classname = "LabelStreamFC";
std::vector<LabelStreamFC::ListMsgFC<MsgLS *> *> LabelStreamFC::vec_msgs_lists;
std::vector<int> LabelStreamFC::vec_msgs_tags;
std::vector<LabelStreamFC *> LabelStreamFC::vec_objs_pointers;
std::vector<pthread_mutex_t > LabelStreamFC::vec_msgListMutexFC;
int LabelStreamFC::numTotalObj = 0;
int LabelStreamFC::nStreamsDest = 0;
bool LabelStreamFC::commThreadCreated = false;
pthread_mutex_t LabelStreamFC::globalCommMutexFC;

struct threadDataLSFC{
	int tid;
	void *LabelStreamFC;
};

void *callThreadLSFC(void *arg){
	LabelStreamFC *lsfc = (LabelStreamFC *)((threadDataLSFC*) arg)->LabelStreamFC;
	int tid = (int)((threadDataLSFC*) arg)->tid;
	lsfc->commProccessThread(tid);
	free(arg);
	pthread_exit(NULL);
}

LabelStreamFC::LabelStreamFC(int32_t oF_i_r,
							int32_t oF_n,
							int32_t dF_i_r,
							int32_t dF_n,
							int tag,
							bool lastStream_in) : LabelStream(oF_i_r,
													oF_n,
													dF_i_r,
													dF_n,
													tag),
										lsfc_tag(tag+TAG_MSG_MAX_EXCLUSIVE),
										LIST_MIN_SIZE(DEF_LIST_MIN_SIZE),
										N_MSGS_PER_REQ(DEF_N_MSGS_PER_REQ),
										numMsgsRequested(0),
										commThread(NULL),
										vec_pos(0),
										lastStream(lastStream_in)
{
	CreateLSFC();
}

LabelStreamFC::LabelStreamFC(int32_t oF_i_r,
								int32_t oF_n,
								int32_t dF_i_r,
								int32_t dF_n,
								int tag,
								uint32_t l_min_size,
								uint32_t n_msg_req,
								bool lastStream_in): LabelStream(oF_i_r,
																oF_n,
																dF_i_r,
																dF_n,
																tag),
													lsfc_tag(tag+TAG_MSG_MAX_EXCLUSIVE),
													LIST_MIN_SIZE(l_min_size),
													N_MSGS_PER_REQ(n_msg_req),
													numMsgsRequested(0),
													commThread(NULL),
													vec_pos(0),
													lastStream(lastStream_in)
{
	CreateLSFC();
}

void LabelStreamFC::CreateLSFC() {

	/**
	 * First time this class is created,
	 * clears everything.
	 */
	if(numTotalObj == 0) {
		vec_msgs_lists.clear();
		vec_msgs_tags.clear();
		vec_objs_pointers.clear();
		vec_msgListMutexFC.clear();
	}

	if( getInstType() == SOURCE ) {

#ifdef DEBUG_P_LSFC
		std::stringstream st;
		st << "LSFC_S_" << getpid() << "_" << Stream::getTag() << ".log";
		log = new Logger(st.str(), st.str());
		log->PrintMsgS(classname, "constructor" ) << "Starting." << endl;
#endif

	} else if ( getInstType() == DESTINATION ) {

#ifdef DEBUG_P_LSFC
		std::stringstream st;
		st << "LSFC_D_" << getpid() << "_" << Stream::getTag() << ".log";
		log = new Logger(st.str(),st.str());
		log->PrintMsgS(classname, "constructor" ) << "Starting." << endl;
#endif

		/**
		 * Store the LSFC object's pointer.
		 */
		vec_objs_pointers.push_back(this);

		/**
		 * Creates a new list for LSFC created.
		 * This one is for ordinary messages or to request messages.
		 */
		vec_msgs_lists.push_back(new LabelStreamFC::ListMsgFC<MsgLS*>());
		vec_pos = nStreamsDest;

		/**
		 * Create one mutex for each stream to avoid thread problems.
		 */
		pthread_mutex_t tmp;
		pthread_mutex_init(&tmp, NULL);
		vec_msgListMutexFC.push_back(tmp);

		/**
		 * Add this LSFC tag to the map< message tag -> vec_pos.
		 *
		 */
		vec_msgs_tags.push_back(Stream::getTag());

		nStreamsDest++;
	}
	numTotalObj++;

	/**
	 * If it is the last stream
	 */
	if(lastStream && nStreamsDest > 0) {
		createCommThread();
		commThreadCreated = true;
	}
}

LabelStreamFC::~LabelStreamFC()
{

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"Destructor") << "Started." << endl;
#endif

/*
	if( getInstType() == SOURCE ){


	} else
*/
	if ( getInstType() == DESTINATION ) {
		delete vec_msgs_lists[vec_pos];

		pthread_mutex_destroy(&vec_msgListMutexFC[vec_pos]);
	}

	numTotalObj--;

	/**
	 * If the vec_msgs_lists is empty, seems that this
	 * is the last lsfc to be destroyed. Therefore, destroy
	 * the communication thread.
	 */
	if (numTotalObj == 0) {
		if (commThreadCreated) {
			destroyCommThread();
		}
		vec_msgs_lists.clear();
		vec_msgs_tags.clear();
		vec_objs_pointers.clear();
		vec_msgListMutexFC.clear();
	}




#ifdef DEBUG_P_LSFC
	delete log;
#endif
}

void LabelStreamFC::createCommThread() {

#ifdef DEBUG_P_LSFC
		std::stringstream stComm;
		stComm << "LSFC_D_commThread_" << getpid() << "_" << Stream::getTag() << ".log" ;
		//stComm << "LSFC_D_commThread_" << getpid() << ".log" ;
		logCommThread = new Logger(stComm.str(), stComm.str());
#endif

	/** Create the thread to receive the messages and add them to the list.*/
	threadDataLSFC * arg = (threadDataLSFC *) malloc(sizeof(threadDataLSFC));
	if(!arg) {
		cerr << "[LabelStreamFC][Constructor] ERROR could not allocate args thread." << endl;
		exit(EXIT_FAILURE);
	}

	pthread_mutex_init(&globalCommMutexFC, NULL);

	arg->tid = 0;
	arg->LabelStreamFC = this;

	commThread = (pthread_t *) malloc(sizeof(pthread_t));
	if (!commThread) {
		cerr << "[LabelStreamFC][Constructor] ERROR could not allocate thread." << endl;
		exit(EXIT_FAILURE);
	}

	int ret = pthread_create(commThread, NULL, callThreadLSFC, (void *)arg);
	if (ret != 0){
		errno = ret;
		perror("pthread_create()");
		exit(EXIT_FAILURE);
	}

}

void LabelStreamFC::destroyCommThread() {

	pthread_join(*commThread, NULL);

	pthread_mutex_destroy(&globalCommMutexFC);

	free(commThread);

#ifdef DEBUG_P_LSFC
	delete logCommThread;
#endif

}

void LabelStreamFC::sendBuffer(MsgLS& b)
{

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"sendBuffer") << "Started." << endl;
#endif
	while(1) {

		if (canSendMessages()) {
			pthread_mutex_lock(&globalCommMutexFC);

			LabelStream::sendBuffer(b);

			pthread_mutex_unlock(&globalCommMutexFC);

			decrementeMsgsReqsed();


			break;
		} else { /* Will wait for request. */

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"sendBuffer") << "Waiting for a request." << endl;
#endif

			/*
			 * If there is any request message update the canSendMessages
			 * and send it, otherwise wait.
			 */
			MsgRequest b;
			/* Probe and receive the message. */
			LabelStreamFC::probeAndRecvRequestFC(b);

			updateNumMsgsReqsed(b.getNumMsgsRequed());

		}
	}
#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"sendBuffer") << "Finished." << endl;
#endif

}

int LabelStreamFC::probeStream(int& src)
{
	int ret;
	bool requested = false;

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"probeStream") << "Started." << endl;
#endif

	while(1) {

		pthread_mutex_lock(&vec_msgListMutexFC[vec_pos]);

		if(isEndOfWork()) {
			ret = EOW;
			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);
			break;
		}

		if(vec_msgs_lists[vec_pos]->empty()) {
			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);

			/* From probe only sends one request for messages. */
			if(requested == false) {
				sendRequest();
				requested = true;
			}
			/* TODO check this sleep time. */
			usleep(USLEEP_TIME);
		} else {
			ret = vec_msgs_lists[vec_pos]->front()->getSizeMsg();

			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);

			src = MPI_ANY_SOURCE;

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"probeStream") << "Found msg with size " << ret << "." << endl;
#endif

			break;
		}
	}

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"probeStream") << "Finished." << endl;
#endif

	return ret;
}

int LabelStreamFC::probeAndReadStream(MsgLS * m) {
	int ret;

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"probeAndReadStream") << "Started." << endl;
#endif

	if(vec_msgs_lists[vec_pos]->size() < LIST_MIN_SIZE) {
		sendRequest();
	}

	while(1) {

#ifdef DEBUG_P_LSFC
//		log->PrintDebugMsgS(classname,"probeAndReadStream") << " Esperando ." << endl;
#endif

		pthread_mutex_lock(&vec_msgListMutexFC[vec_pos]);

#ifdef DEBUG_P_LSFC
//		log->PrintDebugMsgS(classname,"probeAndReadStream") << " Passou ." << endl;
#endif

		if(isEndOfWork()) {
			ret = EOW;
			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);
			break;
		}

		if(vec_msgs_lists[vec_pos]->empty()) {
			/* TODO check this sleep time. */
			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);
			usleep(USLEEP_TIME);
		} else {
			/* Get the message */
			m = vec_msgs_lists[vec_pos]->front();
			vec_msgs_lists[vec_pos]->pop_front();

			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);

			/* The consumer decrement the number of messages requested. */
			decrementeMsgsReqsed();

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"probeAndReadStream") << " Recvd msg with size " << m->getSizeMsg() << "." << endl;
#endif
			break;
		}
	}


#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"probeAndReadStream") << "Finished." << endl;
#endif

	return ret;
}

int LabelStreamFC::readBuffer(MsgLS * b, const int source)
{
	int ret;

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"readBuffer") << "Started." << endl;
#endif

	if(vec_msgs_lists[vec_pos]->size() < LIST_MIN_SIZE) {
		sendRequest();
	}

	while(1) {

		pthread_mutex_lock(&vec_msgListMutexFC[vec_pos]);

		if(isEndOfWork()) {
			ret = EOW;
			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);
			break;
		}

		if(vec_msgs_lists[vec_pos]->empty()) {
			/* TODO check this sleep time. */
			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);
			usleep(USLEEP_TIME);
		} else {
			/* Get the message */
			b = vec_msgs_lists[vec_pos]->front();
			vec_msgs_lists[vec_pos]->pop_front();

			pthread_mutex_unlock(&vec_msgListMutexFC[vec_pos]);

			ret = ORDINARY_MSG;

			/* The consumer decrement the number of messages requested. */
			decrementeMsgsReqsed();

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"readBuffer") << " Recvd msg with size " << b->getSizeMsg() << "." << endl;
#endif
			break;
		}
	}

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"readBuffer") << "Finished." << endl;
#endif

	return ret;
}

void LabelStreamFC::sendRequest()
{
#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"sendRequest") << "Started." << std::endl;
#endif

	/** Only request message when possible. */
	if(canRequestMessages()) {

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"sendRequest") << " src init " << LabelStream::getSourceInstInitRank() << " size " << LabelStream::getSourceInstSize()  << endl;
#endif


		MsgRequest m;
		m.allocMsgRequest();
		m.setNumMsgsRequed(N_MSGS_PER_REQ);
		/* Send to instance producers (sources) of the current stream. */
		for(int32_t i = LabelStream::getSourceInstInitRank();
				i < LabelStream::getSourceInstSize() + LabelStream::getSourceInstInitRank();
				i++) {

			m.setHeaderLabel(i);

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"sendRequest") << "tag " << lsfc_tag << " to instance " << i << endl;
#endif

			pthread_mutex_lock(&globalCommMutexFC);

			LabelStream::send((void *) m.getMsg(), m.getSizeMsg(), i, lsfc_tag);

			pthread_mutex_unlock(&globalCommMutexFC);

#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"sendRequest") << " Sent." << endl;
#endif
		}

		/** Update the number of messages requested. */
		updateNumMsgsReqsed(N_MSGS_PER_REQ);
	}

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"sendRequest") << "Finished." << std::endl;
#endif

}

void LabelStreamFC::probeAndRecvRequestFC(MsgRequest &m) {
	int mem_size_msg, src;

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"probeAndRecvRequestFC") << "Started probing tag " << lsfc_tag << "." << endl;
#endif

	while(1) {

		pthread_mutex_lock(&globalCommMutexFC);

		mem_size_msg = iprobe(src, lsfc_tag);


		if(mem_size_msg > 0) {
			m.allocMsgLS(mem_size_msg);

			LabelStream::recv((void *) m.getMsg(), m.getSizeMsg(), src, lsfc_tag);

			pthread_mutex_unlock(&globalCommMutexFC);

	#ifdef DEBUG_P_LSFC
			log->PrintDebugMsgS(classname,"probeAndRecvRequestFC") << " Recvd msg with size " << m.getSizeMsg() << "." << endl;
	#endif
			break;
		} else {
			pthread_mutex_unlock(&globalCommMutexFC);

			usleep(USLEEP_TIME);
		}
	}

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"probeAndRecvRequestFC") << "Finished." << endl ;
#endif
}

void LabelStreamFC::commProccessThread(int tid)
{
	int n_streams = vec_msgs_lists.size(), n_streams_closed = 0, ind = 0;

	std::vector<bool> streamsClosed(n_streams, false);

#ifdef DEBUG_P_LSFC
	logCommThread->PrintDebugMsgS(classname,"commProccessThread") << "Started looking for " << n_streams << " " << nStreamsDest << "." << endl ;
#endif

	while(1) {

		int out_src, mem_size_msg;

		ind++;
		if(ind == n_streams) ind = 0;

		if(n_streams_closed == nStreamsDest) {
			break;
		}

#ifdef DEBUG_P_LSFC
//		logCommThread->PrintDebugMsgS(classname,"commProccessThread") << " streamsClosed["<< ind <<"] " << streamsClosed[ind] << " n_streams_closed " <<  n_streams_closed << ". " << endl ;
#endif

		if(vec_objs_pointers[ind]->getActiveSourceInst() == 0) {
			if (streamsClosed[ind] == false) {
				streamsClosed[ind] = true;
				n_streams_closed++;
			}
			continue;
		}

#ifdef DEBUG_P_LSFC
//		logCommThread->PrintDebugMsgS(classname,"commProccessThread") << " going to probe tag " << vec_msgs_tags[ind] << " n_streams_closed " <<  n_streams_closed << ". " << endl ;
#endif

		pthread_mutex_lock(&globalCommMutexFC);

		mem_size_msg = LabelStream::iprobe(out_src, vec_msgs_tags[ind]);

		/* Check  if received a message, otherwise wait and then test again. */
		if(mem_size_msg > 0) {
			/* Control messages are checked here and do not return to the user. */
			if((unsigned int) mem_size_msg > MsgLS::getSizeofHeader()) {
				MsgLS * msg = new MsgLS();
				msg->allocMsgLS(mem_size_msg);

				LabelStream::recv((void *) msg->getMsg(), msg->getSizeMsg(), out_src, vec_msgs_tags[ind]);

				pthread_mutex_unlock(&globalCommMutexFC);

#ifdef DEBUG_P_LSFC
				logCommThread->PrintDebugMsgS(classname,"commProccessThread") << " Recved msg tag " << vec_msgs_tags[ind] << " src " << out_src << "." << endl ;
#endif

				pthread_mutex_lock(&vec_msgListMutexFC[ind]);

				vec_msgs_lists[ind]->push_back(msg);

				pthread_mutex_unlock(&vec_msgListMutexFC[ind]);
			} else {
				MsgLS msg(0);

				LabelStream::recv((void *) msg.getMsg(), msg.getSizeMsg(), out_src, vec_msgs_tags[ind]);

				pthread_mutex_unlock(&globalCommMutexFC);

				if(msg.isEOWmsg()) {
					vec_objs_pointers[ind]->closeSourceInstLS();

#ifdef DEBUG_P_LSFC
					logCommThread->PrintDebugMsgS(classname,"commProccessThread") << " Recved closeLabelStream tag " << vec_msgs_tags[ind] << "." << endl ;
#endif
				}
			}
		} else {
			pthread_mutex_unlock(&globalCommMutexFC);

			//TODO sleep before going to next stream when no message is found on the current stream.
			usleep(USLEEP_TIME);
		}


/*
#ifdef DEBUG_P_LSFC
		logCommThread->PrintDebugMsgS(classname,"commProccessThread") << "Received msg ." << endl ;
#endif
*/

	}

#ifdef DEBUG_P_LSFC
	logCommThread->PrintDebugMsgS(classname,"commProccessThread") << "Finished." << endl ;
#endif
}

bool LabelStreamFC::canSendMessages()
{
	if(numMsgsRequested > 0){
		return true;
	} else {
		return false;
	}
}

bool LabelStreamFC::canRequestMessages() {

#ifdef DEBUG_P_LSFC
	log->PrintDebugMsgS(classname,"canRequestMessages") << "Started." << endl;
#endif

	if (numMsgsRequested < (int32_t) LIST_MIN_SIZE) {

#ifdef DEBUG_P_LSFC
		log->PrintDebugMsgS(classname,"canRequestMessages") << "Finished true " << numMsgsRequested << " < " << LIST_MIN_SIZE << "." << endl;
#endif

		return true;
	} else {

#ifdef DEBUG_P_LSFC
		log->PrintDebugMsgS(classname,"canRequestMessages") << "Finished false " << numMsgsRequested << " < " << LIST_MIN_SIZE << "." << endl;
#endif

		return false;
	}
}

/**
 * Checks it the stream is closed by looking for the
 * number of messages in the list and the number os souce instances
 * in the stream.
 */
bool LabelStreamFC::isEndOfWork() {
	if(this->getActiveSourceInst() == 0 && vec_msgs_lists[vec_pos]->empty()) {
#ifdef DEBUG_P_LSFC
		log->PrintDebugMsgS(classname,"isEndOfWork") << " true. vec_pos " << vec_pos << endl;
#endif
		return true;
	} else {
#ifdef DEBUG_P_LSFC
		log->PrintDebugMsgS(classname,"isEndOfWork") << " false. vec_pos " << vec_pos << " size "  << vec_msgs_lists[vec_pos]->size() << endl;
#endif
		return false;
	}
}

void LabelStreamFC::decrementeMsgsReqsed() {
	this->numMsgsRequested--;
}

void LabelStreamFC::updateNumMsgsReqsed(uint32_t msgsToSent) {
	numMsgsRequested += msgsToSent;
}

uint32_t LabelStreamFC::getListMinSize() const {
	return LIST_MIN_SIZE;
}

void LabelStreamFC::setListMinSize(uint32_t listMinSize) {
	LIST_MIN_SIZE = listMinSize;
}

uint32_t LabelStreamFC::getMsgsPerReq() const {
	return N_MSGS_PER_REQ;
}

/**
 *
 * @param msgsPerReq
 */
void LabelStreamFC::setMsgsPerReq(uint32_t msgsPerReq) {
	N_MSGS_PER_REQ = msgsPerReq;
}
