/*
 * LSDataPointMsg.h
 *
 *  Created on: Sep 4, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef LSDATAPOINTMSG_H_
#define LSDATAPOINTMSG_H_

#include "LabelStream.h"

/**
 * \class MsgDP
 * \brief Message from InputReader to DataPoints with
 * point's values to be stored.
 */
template<class T>
class MsgDP : public MsgLS {
private:
	uint32_t * pointsId;
	T * points;
	uint32_t dim;
	uint32_t last_pos;
	uint32_t max_pts;

	void setUpMessage(const uint32_t np_max, const uint32_t d) {
		pointsId = (uint32_t *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header );
		points = (T *) ( ((uint8_t *) MsgLS::getMsg()) + MsgLS::sizeof_header + (np_max*sizeof(uint32_t)) );
	}

	MsgDP();
protected:
public:
	/**
	 * MsgDP constructor.
	 * @param size_msg_in Size of message in bytes.
	 */
	MsgDP(const uint32_t np_max, const uint32_t d) : MsgLS((np_max*sizeof(uint32_t))+(np_max*d*sizeof(T))), dim(d), last_pos(0), max_pts(np_max) {
		setUpMessage(np_max, d);
	}

	MsgDP(const uint32_t d) : MsgLS(), pointsId(NULL), points(NULL), dim(d), last_pos(0), max_pts(0)
	{}

	MsgDP(const MsgDP &in) : MsgLS(in), dim(in.dim), last_pos(in.last_pos), max_pts(in.max_pts){
		setUpMessage(max_pts, dim);
	}

	/**
	 * MsgDP destructor.
	 */
	virtual ~MsgDP()
	{
		pointsId = NULL;
		points = NULL;
	}

	void deserialize() {
		max_pts = getMaxNumberOfPoints(getSizeMsg(),dim);
		setUpMessage(max_pts, dim);
	}

	/**
	 * Indicates whether the number of messages in this buffer is above.
	 * @return true if buffer is full, false otherwise.
	 */
	bool isBufferFull () const {
		return last_pos >= max_pts;
	}

	void flushBuffer() {
		last_pos = 0;
	}

	/**
	 *
	 * @param id Point's id.
	 * @param pointValues Buffer with point's values.
	 * @return True if success or false if could not push the point.
	 */
	bool push_back(uint32_t id, T * pointValues) {
		if(last_pos < max_pts) {
			pointsId[last_pos] = id;
			memcpy(&points[last_pos*dim], pointValues, sizeof(T)*dim);
			last_pos++;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the id of the point.
	 * @param i ith point.
	 * @return Id of ith point.
	 */
	uint32_t getPointId(uint32_t i) const {
		return pointsId[i];
 	}

	/**
	 * Get pointer to point's buffer.
	 * @param i point position from 0 to NP.
	 * @return ith point.
	 */
	T * getPointValues(uint32_t i) const {
		return &points[i*dim];
	}

	uint32_t getLastPos() const {
		return last_pos;
	}

	static uint32_t getMaxNumberOfPoints(const uint32_t mem_msg_size, const uint32_t d){
		uint32_t n_points = 0;
		if(mem_msg_size > MsgLS::sizeof_header) {
			n_points = ( mem_msg_size - MsgLS::sizeof_header ) / ( sizeof(uint32_t) + (sizeof(T)*d) );
		}
		return n_points;
	}
};

#endif /* LSDATAPOINTMSG_H_ */
