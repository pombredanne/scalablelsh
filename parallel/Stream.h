/*
 * Stream.h
 *
 *  Created on: Aug 22, 2012
 *      Author: Thiago S. F. X. Teixeira
 */ 


#ifndef STREAM_H_
#define STREAM_H_ 

#include <mpi.h>
#include <pthread.h>

/*!
 * Maximum Tag value, greater than this is for internal use only.
 * For instance, they are used on LabelStreamFC, the label stream with flow control.
 */
#define TAG_MSG_MAX_EXCLUSIVE 1000

/**
 * \class Stream
 * \brief Abstract Class which defines a Stream.
 *
 * This class defines the stream's methods based on
 * MPI interface.
 */ 
class Stream {
private:
	/*! Tag of the stream, must be unique. */
	int m_tag;

	Stream();
protected:
	/**
	 * Checks for source if there is any message received. If so, get
	 * the size of message.
	 * @param source MPI rank of source process that will be checked, may be MPI_ANY_SOURCE.
	 * @param out_source Get which source the message came from.
	 * Especially useful when MPI_ANY_SOURCE is received as input.
	 * @param out_tag Get which tag the message came from.
	 * Especially useful when MPI_ANY_TAG is received as input.
	 * @return The size in bytes of the message.
	 * If value returned is greater than zero
	 * there is a message to be received; otherwise,
	 * no message arrived from this source.
	 */
	int iprobe(int source, int &out_source, int tag, int &out_tag) {
		int err = 0, flag, mem_size_msg = -1;
		MPI_Status mpi_st;
//		std::cout << "Stream::iprobe source " << source << " tag " << tag << std::endl;
		err = MPI_Iprobe(source, tag, MPI_COMM_WORLD, &flag, &mpi_st);
		if(err != MPI_SUCCESS) {
			std::cerr << "[ERROR] MPI_Probe has finished with problem! " << std::endl;
			exit(EXIT_FAILURE);
		}

		if(flag) {
			mem_size_msg = 0;
			err = MPI_Get_count(&mpi_st, MPI_BYTE, &mem_size_msg);
			if(err != MPI_SUCCESS) {
				std::cerr << "[ERROR] MPI_Get_Count has finished with problem! " << std::endl;
				exit(EXIT_FAILURE);
			}
			/* Get specific source of probed message. Very important when input source is MPI_ANY_SOURCE.*/
			out_source = mpi_st.MPI_SOURCE;
			out_tag = mpi_st.MPI_TAG;
		}
		return mem_size_msg;
	}

	/**
	 * Checks for source if there is any message received. This is blocking.
	 * @param source MPI rank of source process that will be checked, may be MPI_ANY_SOURCE.
	 * @param out_source Get which source the message came from.
	 * Especially useful when MPI_ANY_SOURCE is received as input.
	 * @return The size in bytes of the message.
	 */
	int probe(int source, int &out_source, int tag) {
		int err = 0, mem_size_msg = -1;
		MPI_Status mpi_st;
		err = MPI_Probe(source, tag, MPI_COMM_WORLD, &mpi_st);
		if(err != MPI_SUCCESS) {
			std::cerr << "[ERROR] MPI_Probe has finished with problem! " << std::endl;
			exit(EXIT_FAILURE);
		}

		err = MPI_Get_count(&mpi_st, MPI_BYTE, &mem_size_msg);
		if(err != MPI_SUCCESS) {
			std::cerr << "[ERROR] MPI_Get_Count has finished with problem! " << std::endl;
			exit(EXIT_FAILURE);
		}
		/* Get specific source of probed message. Very important when input source is MPI_ANY_SOURCE. */
		out_source = mpi_st.MPI_SOURCE;
		return mem_size_msg;
	}

	/**
	 * Send buffer method.
	 * @param msg Pointer to message buffer.
	 * @param size_msg Size of message in bytes.
	 * @param dest MPI rank of destination process.
	 */
	void send(void * msg, uint32_t size_msg, int dest, int tag) {
//		int dest = labelFunction();
//		std::cout << "send 2 dest " << dest << std::endl;
		int ret = MPI_Send(msg, size_msg, MPI_BYTE, dest, tag, MPI_COMM_WORLD);
		if(ret != MPI_SUCCESS) {
			std::cerr << "[ERROR] MPI_Send has finished with problem! " << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	/**
	 * Receive buffer method.
	 * @param msg Pointer to message buffer.
	 * @param size_msg Size of message in bytes.
	 * @param source MPI rank of source process.
	 */
	void recv(void * msg, uint32_t size_msg, int source, int tag) {
//		std::cout << "Strem recv  source " << source <<  " size_msg " << size_msg << std::endl;
		MPI_Status mpi_st;
		int ret = MPI_Recv(msg, size_msg, MPI_BYTE, source, tag, MPI_COMM_WORLD, &mpi_st);
		if(ret != MPI_SUCCESS) {
			std::cerr << "[ERROR] MPI_Recv has finished with problem! " << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	int getTag() const {
		return m_tag;
	}
public:
	/**
	 * Stream constructor.
	 * \param tag Tag of the stream, must be unique.
	 */
	Stream(int tag) : m_tag(tag){

		if(m_tag < 1) {
			std::cerr << "[Stream] ERROR: Stream tag - " << m_tag << " - must be greater than 0. " << std::endl;
			exit(EXIT_FAILURE);
		}
		if(m_tag > TAG_MSG_MAX_EXCLUSIVE) {
			std::cerr << "[Stream] ERROR: Stream tag greater than " << TAG_MSG_MAX_EXCLUSIVE  << " is exclusive to internal use." << std::endl;
			exit(EXIT_FAILURE);
		}

	}
	/**
	 * Stream Destructor.
	 */
	virtual ~Stream(){
	}

	/**
	 * Send buffer method.
	 * @param msg Pointer to message buffer.
	 * @param size_msg Size of message in bytes.
	 * @param dest MPI rank of destination process.
	 */
	void send(void * msg, uint32_t size_msg, int dest) {
		send(msg, size_msg, dest, m_tag);
	}

	/**
	 * Receive buffer method.
	 * @param msg Pointer to message buffer.
	 * @param size_msg Size of message in bytes.
	 * @param source MPI rank of source process.
	 */
	void recv(void * msg, uint32_t size_msg, int source) {
		recv(msg, size_msg, source, m_tag);
	}

	/**
	 * Checks for source if there is any message received. If so, get
	 * the size of message.
	 * @param source MPI rank of source process that will be checked, may be MPI_ANY_SOURCE.
	 * @param out_source Get which source the message came from.
	 * Especially useful when MPI_ANY_SOURCE is received as input.
	 * @return The size in bytes of the message.
	 * If value returned is greater than zero
	 * there is a message to be received; otherwise,
	 * no message arrived from this source.
	 */
	int iprobe(int source, int &out_source) {
		int mem_size_msg = -1, out_tag;
		mem_size_msg = iprobe(source, out_source, m_tag, out_tag);
		return mem_size_msg;
	}


	/**
	 * Checks for source if there is any message received. This is blocking.
	 * @param source MPI rank of source process that will be checked, may be MPI_ANY_SOURCE.
	 * @param out_source Get which source the message came from.
	 * Especially useful when MPI_ANY_SOURCE is received as input.
	 * @return The size in bytes of the message.
	 */
	int probe(int source, int &out_source) {
		int mem_size_msg = -1;
		mem_size_msg = probe(source, out_source, m_tag);
		return mem_size_msg;
	}


};


#endif /* STREAM_H_ */
