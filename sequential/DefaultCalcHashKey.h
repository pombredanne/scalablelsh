/*
 * DefaultCalcHashKey.h
 *
 *  Created on: Sep 10, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#ifndef DEFAULTCALCHASHKEY_H_
#define DEFAULTCALCHASHKEY_H_

#include <sstream>
#include <stdint.h>
#include "PStableHash.h"

class DefaultCalcHashKey {
private:
	static const unsigned TWO_TO_32_MINUS_1 = 4294967295U;

	/** We did not implemented the most efficient version of this function, as available with LSH (E2LSH) */
	static unsigned computeBlockProductModDefaultPrime(const std::vector<unsigned>& a, const std::vector<unsigned>& b);

	/** This is used to hash the g_i = (h1,h2,...,hm) to a single value, that is used to index the hash table. */
	static unsigned computeUHashFunction(const std::vector<unsigned>& a, const std::vector<unsigned>& b, unsigned hashTableSize);

public:
	static const unsigned UH_PRIME_DEFAULT = 4294967291U;
	static const unsigned MAX_HASH_RND = 536870912U;

	DefaultCalcHashKey();
	virtual ~DefaultCalcHashKey();

	/**
	 * To be used by the sequential version. It uses the calcHashKey below.
	 * @param point
	 * @param hashTableId
	 * @param hashFunctions
	 * @param m
	 * @param hashTableSize
	 * @param R
	 * @param mainHash
	 * @param controlHash
	 * @param chain_key
	 * @return
	 */
	static std::string calcHashKey(const std::vector<uint8_t> &point,
								const int hashTableId,
								std::vector<PStableHash> &hashFunctions,
								const int m,
								unsigned hashTableSize,
								float R,
								const std::vector<unsigned>& mainHash,
								const std::vector<unsigned>& controlHash,
								uint32_t& chain_key);
	/**
	 * To be used by the parallel version.
	 * @param point
	 * @param hashTableId
	 * @param hashFunctions
	 * @param m
	 * @param hashTableSize
	 * @param mainHash
	 * @param controlHash
	 * @param dataHashKey Reference to return dataHashKey to the caller.
	 * @param chain_key Reference to return chain_key to the caller.
	 */
	void static calcHashKey(const std::vector<uint8_t> &point,
							const int hashTableId,
							std::vector<PStableHash> &hashFunctions,
							const int m,
							unsigned hashTableSize,
							float R,
							const std::vector<unsigned>& mainHash,
							const std::vector<unsigned>& controlHash,
							uint32_t& dataHashKey,
							uint32_t& chain_key);

	static unsigned genRandomUns32(unsigned rangeStart, unsigned rangeEnd);

    /**
     * To be used after calcHashKey with the bucket_key.
     */
	static std::string KeyInt2Str(uint32_t key, uint32_t hashTableId);
};

#endif /* DEFAULTCALCHASHKEY_H_ */
