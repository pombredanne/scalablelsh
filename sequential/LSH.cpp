/*
 * LSH.cpp
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#include "LSH.h"

LSH::LSH(FileIO &f_io, int L, int m, int dimensions, int w, float R, int T) {
	fileIO = &f_io;
	this->L = L;
	this->m = m;
	this->R = R;
	this->T = T; // number of variations of each hashkey for multiprobe;
	// The hash table size value should equals to the # of input registers
	this->hashTableSize = f_io.getNumDBRegisters();

	/* NOT using chain_key. */
	this->useChainKey = false;

	if( ! useChainKey ) {
		std::cout << "** NOT using chain_key. **" << std::endl;
	}

	this->reg_id_offset = 0;

	if (T > 0) {
		multiprobe = true;
		std::cout << "Using multiprobe T = " << T << "." << std::endl;
	} else {
		multiprobe = false;
		std::cout << "Not using multiprobe T " << T << "." << std::endl;
	}

	assert(L > 0 && m > 0);

	/*
	 * The same seed is used in parallel and sequential versions.
	 * Therefore, the pseudo-random generation of the hash tables
	 * are made in the same order for both versions, and
	 * they have the same hash tables with the same hash functions.
	 */
	srandom(DEFAULT_INITIAL_SEED);

	for(int i = 0; i < L*m; i++){
		hashFunctions.push_back(PStableHash(dimensions, w));
	}

	for(int i = 0; i < m; i++){
		this->mainHash.push_back(DefaultCalcHashKey::genRandomUns32(1, DefaultCalcHashKey::MAX_HASH_RND));
	}
	for(int i = 0; i < m; i++){
		this->controlHash.push_back(DefaultCalcHashKey::genRandomUns32(1, DefaultCalcHashKey::MAX_HASH_RND));
	}
}

LSH::~LSH() {

}

/*
 * The index and the query table will, in the sequential version, independently of the input will start with the first register with 0 index.
 */
bool LSH::buildIndex() {
	std::vector<uint8_t> point;
	int reg_id = 0;
	uint32_t chain_key;

	/* To start the id from 0 in PointsTable, subtracts the first id from the others.*/
	bool first_reg = true;

	while(1){

		point.clear();
		reg_id = fileIO->readInputPoint(point);
		if (point.size() == 0) {
			break;
		}

		/* Gets the id of the first register as offset. It will be used in pintANN. */
		if(first_reg) {
			reg_id_offset = reg_id;
			first_reg = false;
		}

		reg_id -= reg_id_offset;

/*		std::cout << reg_id << std::endl;
		for(int i = 0; i < point.size(); i++){
			std::cout << (int)point[i]<< " ";
		}
		std::cout<< std::endl;
*/

		for(int j = 0; j < L; j++){
			std::string bucket_key = calcHashKey(point, j, chain_key);
			/* Chain_key is ignored when using multiprobe. */
			if(!useChainKey) {
				chain_key = 0;
			}
			hashTable.addPoint(bucket_key, chain_key, reg_id, reg_id);
			//std::cout << "L:"<< j << " bucket_key:" << bucket_key << " chain_key:"<< chain_key << std::endl;
		}

		pointsTable.addPoint(point);
		if(reg_id % 100000 == 0){
			std::cout << reg_id<< " registers indexed!" <<std::endl;
		}
	}
//	pointsTable.print();
	return true;
}



std::string LSH::calcHashKey(std::vector<uint8_t> &point, int hashTableId, uint32_t &chain_key) {

	std::string str_dataHashKey;
	unsigned dataHashkey;

	DefaultCalcHashKey::calcHashKey(point,
								    hashTableId,
								    this->hashFunctions,
								    this->m,
								    this->hashTableSize,
								    this->R,
								    this->mainHash,
								    this->controlHash,
								    dataHashkey,
								    chain_key);

	str_dataHashKey = DefaultCalcHashKey::KeyInt2Str(dataHashkey, hashTableId);

	return str_dataHashKey;
}


void LSH::calcHashKeyMP(std::vector<uint8_t> &point, int hashTableId, std::vector<unsigned> &seq) {


	MultiProbeLsh::genProbeSequence(point,
								    hashTableId,
								    this->hashFunctions,
								    this->m,
								    this->hashTableSize,
								    this->R,
								    this->mainHash,
								    this->T,
								    seq);

}


/*
 * The index and the query table will, in the sequential version, independently of the input will start with the first register with 0 index.
 */
bool LSH::loadQueryFile() {
	std::vector<uint8_t> point;
	int query_id=0;
//	uint32_t chain_key;

	while(1){

		point.clear();
		query_id = fileIO->readQueryPoint(point);
		if (point.size() <= 0) {
			break;
		}

/*		std::cout << query_id << std::endl;
		for(int i = 0; i < point.size(); i++){
			std::cout << (int)point[i]<< " ";
		}
		std::cout<< std::endl;*/

		/*if(query_id == 1){
			for(int j = 0; j < L; j++){
				std::string bucket_key = calcHashKey(point, j, chain_key);
				std::string bucket_key_complete = calcHashKeyComplete(point, j);
				std::cout << "query_id="<<query_id<<" bucket_key="<<bucket_key<< " bucket_key_complete="<< bucket_key_complete<<std::endl;


			}
		}*/

		queryTable.addPoint(point);
		if(query_id % 100000 == 0){
			std::cout << query_id << " queries indexed!" <<std::endl;
		}
	}
	return true;
}

std::vector<ANNPoint> LSH::calcANN(std::vector<uint8_t> query, int k) {
	uint32_t chain_key = 0;
	std::string bucket_key;
	std::vector<ANNPoint> ANNPoints;

	for(int j = 0; j < L; j++){

		/***************************************************************************/
		/* Computes the bucket's hash key variation to test in multiple buckets.   */
		if(multiprobe) {

#ifdef DEBUG_S_LSH
			std::cout << "Multiprobe :" << std::endl;
#endif

			std::vector<unsigned> seq;

			calcHashKeyMP(query, j, seq);

			/* For hashkeys variations generated search in the database the nearest points. */
			for(unsigned i = 0; i < seq.size(); i++) {
				bucket_key = DefaultCalcHashKey::KeyInt2Str(seq[i], j);

#ifdef DEBUG_S_LSH
				std::cout << i << " " << bucket_key << std::endl;
#endif

				DataBucket<DBPnt>* curDataBucket = hashTable.retrieveDataBucket(bucket_key, chain_key);
				if(curDataBucket != NULL)
					Operator::calculateANN(ANNPoints, k, query,curDataBucket, pointsTable);
			}

#ifdef DEBUG_S_LSH
			std::cout << "Fim" << std::endl;
#endif
		/*                                                                           */
		/*****************************************************************************/
		} else {
		/*****************************************************************************/
		/*  Computes the default bucket's hash key.                                  */
			bucket_key = calcHashKey(query, j, chain_key);

#ifdef DEBUG_S_LSH
			std::cout << " " << bucket_key << std::endl;
#endif

			 /* Chain_key is not used with multiprobe, so we assign zero to all positions. */
			 if( ! useChainKey ) {
				 chain_key = 0;
			 }

			DataBucket<DBPnt>* curDataBucket = hashTable.retrieveDataBucket(bucket_key, chain_key);
			if(curDataBucket != NULL)
				Operator::calculateANN(ANNPoints, k, query,curDataBucket, pointsTable);

		/*                                                                           */
		/*****************************************************************************/
		}
	}
	return ANNPoints;
}

bool LSH::processQueries(int k) {
	fileIO->printOutputHeader(k);
	for(size_t i=0; i < queryTable.getSize(); i++){
		std::vector<ANNPoint> ANN = calcANN(queryTable.getPoint(i), k);
		fileIO->printANN(ANN, i, reg_id_offset);
	}
	return true;
}




