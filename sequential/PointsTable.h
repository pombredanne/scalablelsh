/*
 * PointsTable.h
 *
 *  Created on: May 19, 2012
 *      Author: george
 */

#ifndef POINTSTABLE_H_
#define POINTSTABLE_H_

#include <vector>
#include <stdint.h>
#include <iostream>
#include <assert.h>
#include <math.h>
#include <stdlib.h>

class PointsTable {
private:
	std::vector<uint8_t> points;
	size_t pointDimensions;
public:
	PointsTable();
	virtual ~PointsTable();

	bool addPoint(const std::vector<uint8_t> &point);
	bool addPoint(const uint8_t * point, const size_t pointId, const size_t dim);
	std::vector<uint8_t> getPoint(const size_t id);
	size_t getSize();
	float distance(const std::vector<uint8_t>& query, const size_t pointId);

	void print();

};

#endif /* POINTSTABLE_H_ */
