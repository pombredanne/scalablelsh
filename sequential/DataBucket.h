/*
 * DataBucket.h
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#ifndef DATABUCKET_H_
#define DATABUCKET_H_

#include <stdlib.h>
#include <vector>
#include <assert.h>
#include <stdint.h>
#include "DBPnt.h"

template<class T>
class DataBucket {
private:
	// store pointer to points in this data bucket
	std::vector<T> points;
	uint32_t chain_key;

public:
	DataBucket() : chain_key(0) {}
	virtual ~DataBucket() {}

	bool addPoint(T point) {
		// Check whether there is space available to store the point in bucket
		points.push_back(point);
		return true;
	}

	size_t getSize() {
		return points.size();
	}

	const T& getPoint(size_t index) const {
		assert(index >=0 && index < points.size());
		return points[index];
	}

	uint32_t getChainKey() const {
		return chain_key;
	}

	void setChainKey(uint32_t chainKey) {
		chain_key = chainKey;
	}
};

#endif /* DATABUCKET_H_ */
