/*
 * DBPnt.h
 *
 *  Created on: Sep 19, 2013
 *      Author: uq49
 */

#ifndef DBPNT_H_
#define DBPNT_H_

#include <stdint.h>

class DBPnt {
private:
	uint32_t pointId;
	uint32_t label;
	DBPnt();
public:
	DBPnt(uint32_t pId, uint32_t lbl);
	virtual ~DBPnt();

	uint32_t getLabel() const;

	uint32_t getPointId() const;
};

#endif /* DBPNT_H_ */
