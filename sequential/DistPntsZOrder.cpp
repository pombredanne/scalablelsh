/*
 * DistPntsZOrder.cpp
 *
 *  Created on: Sep 26, 2013
 *      Author: uq49
 */

#include "DistPntsZOrder.h"

DistPntsZOrder::DistPntsZOrder() {
	// TODO Auto-generated constructor stub

}

DistPntsZOrder::~DistPntsZOrder() {
	// TODO Auto-generated destructor stub
}

/**
 * Put 3 zeros between each bit in a 8-bit number to become a 32-bit and make the z-order.
 */
uint32_t DistPntsZOrder::SeparateBy3(uint8_t i) {
	uint32_t x = 0x000000ff & i;
//	printf("x %08X\n", x);
	x = (x ^ ( x << 12 )) & 0x000f000f;
//	printf("x %08X\n", x);
	x = (x ^ ( x << 6 ))  & 0x03030303;
//	printf("x %08X\n", x);
	x = (x ^ ( x << 3 )) & 0x11111111;
//	printf("x %08X\n", x);
	return x;
}


uint32_t DistPntsZOrder::MortonCode4D(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {

	return SeparateBy3(a) | (SeparateBy3(b) << 1) | (SeparateBy3(c) << 2) | (SeparateBy3(d) << 3);

}


/**
 * Calculate the z-order from multidimensional points in 4 by 4 dimensions.
 *
 * @param pointValues point
 * @param dim dimension of the point.
 * @param w_zorder the way we split among the points among the DPs.
 * @return
 */
uint32_t DistPntsZOrder::calcZorderLabel(const std::vector<uint8_t> &pointValues, const uint32_t dim, const uint32_t w_zorder) {

	long long unsigned sum = 0;
	uint32_t partial = 0;

	/* Truncate to generate only dimensions multiple of 4.*/
	uint32_t dim_local = (pointValues.size() / 4 ) * 4;

	for(uint32_t i = 0; i < dim_local; i+=4) {
		partial = MortonCode4D(pointValues[i], pointValues[i+1], pointValues[i+2], pointValues[i+3]);
		if(partial >= DefaultCalcHashKey::UH_PRIME_DEFAULT){
			  partial = partial % DefaultCalcHashKey::UH_PRIME_DEFAULT;
		}
		sum += partial;
		if(sum >= DefaultCalcHashKey::UH_PRIME_DEFAULT){
			sum = sum % DefaultCalcHashKey::UH_PRIME_DEFAULT;
		}
	}

	return sum / w_zorder;
}
