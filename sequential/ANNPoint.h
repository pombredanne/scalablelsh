/*
 * ANNPoint.h
 *
 *  Created on: May 20, 2012
 *      Author: george
 */

#ifndef ANNPOINT_H_
#define ANNPOINT_H_

class ANNPoint {
private:
	int id;
	float distance;
public:
	ANNPoint();
	virtual ~ANNPoint();

	float getDistance() const {
		return distance;
	}

	void setDistance(float distance) {
		this->distance = distance;
	}

	int getId() const {
		return id;
	}

	void setId(const int id) {
		this->id = id;
	}
};

#endif /* ANNPOINT_H_ */
