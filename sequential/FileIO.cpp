/*
 * FileIO.cpp
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#include "FileIO.h"

FileIO::FileIO(std::string DBFileName, std::string queryFileName, std::string outputFileName) {
	f_io_in = new FileIn(DBFileName, queryFileName);
	f_io_out = new FileOut(outputFileName);
}

FileIO::~FileIO() {
	delete f_io_in;
	delete f_io_out;
}

int32_t FileIO::readInputPoint(std::vector<uint8_t> &p){
	return f_io_in->readInputPoint(p);
}

int32_t FileIO::readQueryPoint(std::vector<uint8_t> &q){
	return f_io_in->readQueryPoint(q);
}

std::ofstream& FileIO::getOutputFile(){
	return f_io_out->getOutputFile();
}

unsigned int FileIO::getNumDBRegisters() {
	return f_io_in->getNumberOfRegisters();
}

void FileIO::printANN(const std::vector<ANNPoint>& ANN, const int ind) {
	f_io_out->printANN(ANN, ind);
}

void FileIO::printANN(const std::vector<ANNPoint>& ANN, const int ind, const int offset) {
	f_io_out->printANN(ANN, ind, offset);
}

void FileIO::printOutputHeader(const uint32_t k) {
	f_io_out->printOutputHeader(k);
}
