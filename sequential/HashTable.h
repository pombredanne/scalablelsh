/*
 * HashTable.h
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#ifndef HASHTABLE_H_
#define HASHTABLE_H_

#include <map>

#include <tr1/unordered_map>
#include <string>
#include <iostream>
#include "DBPnt.h"
#include "DataBucket.h"
#include <list>


class HashTable {
private:
	//std::map<std::string, DataBucket*> buckets_hash;

	std::tr1::unordered_map<std::string, std::list<DataBucket<DBPnt>*> > buckets_hash;
public:
	HashTable();
	virtual ~HashTable();

	bool addPoint(std::string bucket_key, uint32_t chain_key, uint32_t point, uint32_t label);
	DataBucket<DBPnt>* retrieveDataBucket(std::string bucket_key, uint32_t chain_key);

};

#endif /* HASHTABLE_H_ */
