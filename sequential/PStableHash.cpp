/*
 * PStableHash.cpp
 *
 *  Created on: May 21, 2012
 *      Author: george
 */

#include "PStableHash.h"

PStableHash::PStableHash(int dimensions, int w) {
	for(int i = 0; i < dimensions; i++){
		a.push_back(genGaussianRandom());
	}
	b = genUniformRandom(0, w);
	this->w = w;
}
// Generate a random real from normal distribution N(0,1).
float PStableHash::genGaussianRandom(){
	// Use Box-Muller transform to generate a point from normal
	// distribution.
	float x1, x2;
	do{
		x1 = genUniformRandom(0.0, 1.0);
	} while (x1 == 0); // cannot take log of 0.
	x2 = genUniformRandom(0.0, 1.0);
	float z;
	z = sqrtf(-2.0 * log(x1)) * cos(2.0 * M_PI * x2);
	return z;
}

// Generate a random real distributed uniformly in [rangeStart,
// rangeEnd]. Input must satisfy: rangeStart <= rangeEnd. The
// granularity of generated random reals is given by RAND_MAX.
float PStableHash::genUniformRandom(float rangeStart, float rangeEnd){
	assert(rangeStart <= rangeEnd);
	float r;

	r = rangeStart + ((rangeEnd - rangeStart) * (float)random() / (float)RAND_MAX);
	assert(r >= rangeStart && r <= rangeEnd);
	return r;
}

PStableHash::~PStableHash() {

}

unsigned PStableHash::calcHash(std::vector<uint8_t>& point) {

	if(point.size() != a.size()) {
		std::cerr << "[PStableHash][calcHash] ERROR dimensions of point ("<<point.size()<<") and vector("<<a.size()<<") are different. Check the dimensions parameter (-d)." << std::endl;
		exit(1);
	}

	float accValue = 0.0;
	// calculate dot product of a to point
	for(uint32_t i = 0; i < a.size(); i++){
//		std::cout << "a["<<i<<"]="<<a[i]<< " point["<<i<<"]="<<(int)point[i]<<std::endl;
//		std::cout <<"accValue="<<accValue<<std::endl;
		accValue += (a[i]*point[i]);
	}
//	std::cout <<"b="<<b<< " w="<<w<<std::endl;
	accValue += b;
	accValue /=w;
//	std::cout <<"accValue="<<accValue<<std::endl;

	return (int)accValue;
}

unsigned PStableHash::calcHash(std::vector<float>& point) {

	float delta;
	return calcHash(point, &delta);
}

unsigned PStableHash::calcHash(std::vector<float>& point, float * delta) {

	if(point.size() != a.size()) {
		std::cerr << "[PStableHash][calcHash] ERROR dimensions of point ("<<point.size()<<") and vector("<<a.size()<<") are different. Check the dimensions parameter (-d)." << std::endl;
		exit(1);
	}

	float accValue = 0.0;
	// calculate dot product of a to point
	for(uint32_t i = 0; i < a.size(); i++){
//		std::cout << "a["<<i<<"]="<<a[i]<< " point["<<i<<"]="<<(int)point[i]<<std::endl;
//		std::cout <<"accValue="<<accValue<<std::endl;
		accValue += (a[i]*point[i]);
	}
//	std::cout <<"b="<<b<< " w="<<w<<std::endl;
	accValue += b;
	accValue /= w;
	float orig = accValue;
//	std::cout <<"accValue="<<accValue<<std::endl;
	accValue = floor(accValue);

	*delta = orig - accValue;

	return (int)accValue;
}

