/**
  * \file mplsh.h
  * \brief Multi-Probe LSH indexing.
  *
  * Multi-Probe LSH (MPLSH) uses the same data structure as LshIndex, except that it
  * probes more than one buckets in each hash table to generate more accurate
  * results.  Equivalently, less hash tables are needed to achieve the same
  * accuracy.  The limitation is that the current implementation only works for
  * L2 distance.
  * 
  * Follow the following 4 steps to use the MPLSH API.
  * 
  * \section mplsh-1 1. Implement a scanner class which scan the candidate keys.
  * 
  * The MPLSH data structure doesn't manage the feature vectors, but only keeps
  * the keys to retrieve them.  You need to provide a scanner class to MPLSH, and
  * for each query, MPLSH will pass the candidate keys to the scanner.
  * The scanner usually keeps an K-NN data structure internally and updates
  * it when receives candidate keys.
  *
  * MPLSH uses the scanner as a unary function taking a key as argument.
  *
  * The default scanner implementation is TopkScanner.
  * 
  * \code
  * class Scanner
  * {
  *      ...
  *      void operator () (unsigned key);
  * };
  * \endcode
  *
  * \section mplsh-2 2. Construct the MPLSH data structure.
  *
  * Assume we use key type KEY.
  *
  * \code
  *
  * typedef MultiProbeLshIndex<KEY> Index;
  *
  * Index index;
  * \endcode
  *
  * The index can not be used yet.
  *
  * \section mplsh-3 3. Populate the index / Load the index from a previously saved file.
  *
  * When the index is initially built, use the following to populate the index:
  * \code
  * Index::Parameter param;
  *
  * //Setup the parameters.  Note that L is not provided here.
  * param.W = W;
  * param.H = H; // See H in the program parameters.  You can just use the default value.
  * param.M = M;
  * param.dim = DIMENSION_OF_THE_DATA
  * DefaultRng rng; // random number generator.
  * 
  * index.init(param, rng, L);
  * 
  * for (each possible key, value pair) {
  *     index.insert(key, value);
  * }
  * 
  * // You can now save the index for future use.
  * ofstream os(index_file.c_str(), std::ios::binary);
  * index.save(os);
  * \endcode
  *  
  * Or you can load from a previously saved file
  *  
  * \code
  * ifstream is(index_file.c_str(), std::ios::binary);
  * index.load(is);
  * \endcode
  * 
  * \section mplsh-4 4. Query the MPLSH. 
  * 
  * \code
  *   
  * float *query;
  * ...
  * index.query(query, T, scanner);   cnt is the number of points actually scanned.
  * 
  * \endcode
  *
  * See the source file lshkit/tools/mplsh-run.cpp for a full example of using MPLSH.
  *
  * For adaptive probing, I hard coded the sensitive range of KNN distance to
  * [0.0001W, 100W] and logarithmically quantized the range to 200 levels.
  * If you find that your KNN distances fall outside this range, or want more refined
  * quantization, you'll have to modify the code in lshkit::MultiProbeLshIndex::init().
  *
  * \section ref Reference
  *
  * Wei Dong, Zhe Wang, William Josephson, Moses Charikar, Kai Li. Modeling LSH
  * for Performance Tuning.. To appear in In Proceedings of ACM 17th Conference
  * on Information and Knowledge Management (CIKM). Napa Valley, CA, USA.
  * October 2008.
  *
  * Qin Lv, William Josephson, Zhe Wang, Moses Charikar, Kai Li. Multi-Probe LSH:
  * Efficient Indexing for High-Dimensional Similarity Search. Proceedings of the
  * 33rd International Conference on Very Large Data Bases (VLDB). Vienna,
  * Austria. September 2007.
  *
  */

#ifndef MULTIPROBE_H_
#define MULTIPROBE_H_

#include <stdint.h>
#include <vector>

#include "PStableHash.h"
#include "DefaultCalcHashKey.h"


static inline unsigned long long leftshift (unsigned N) {
    return (unsigned long long)1 << N;
}

/// Probe vector.
struct Probe
{
    unsigned long long mask;
    unsigned long long shift;
    float score;
    unsigned reserve;
    bool operator < (const Probe &p) const { return score < p.score; }
    Probe operator + (const Probe &m) const
    {
        Probe ret;
        ret.mask = mask | m.mask;
        ret.shift = shift | m.shift;
        ret.score = score + m.score;
        return ret;
    }
    bool conflict (const Probe &m)
    {
        return (mask & m.mask) != 0;
    }
    static const unsigned MAX_M = 64;
    static const unsigned MAX_T = 240;
};

/// Probe sequence.
typedef std::vector<Probe> ProbeSequence;

/// Generate a template probe sequence.
void GenProbeSequenceTemplate (ProbeSequence &seq, unsigned M, unsigned T);

/// Probe sequence template.
class ProbeSequenceTemplates: public std::vector<ProbeSequence>
{
public:
    ProbeSequenceTemplates(unsigned max_M, unsigned max_T)
        : std::vector<ProbeSequence>(max_M + 1)
    {
        for (unsigned i = 1; i <= max_M; ++i)
        {
            GenProbeSequenceTemplate(at(i), i, max_T);
        }
    }
};

extern ProbeSequenceTemplates __probeSequenceTemplates;

/// Multi-Probe LSH class.
class MultiProbeLsh
{
private:

public:

    MultiProbeLsh () {}

    static void genProbeSequence (const std::vector<uint8_t> &obj,
    										const int hashTableId,
    										std::vector<PStableHash> &hashFunctions,
    										const unsigned m,
    										const unsigned hashTableSize,
    										const float R,
    										const std::vector<unsigned> &a,
    										const unsigned T,
    										std::vector<unsigned> &seq);
};

#endif
