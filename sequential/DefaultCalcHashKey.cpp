/*
 * DefaultCalcHashKey.cpp
 *
 *  Created on: Sep 10, 2012
 *      Author: Thiago S. F. X. Teixeira
 */

#include "DefaultCalcHashKey.h"

DefaultCalcHashKey::DefaultCalcHashKey() {
}

DefaultCalcHashKey::~DefaultCalcHashKey() {
}

std::string DefaultCalcHashKey::calcHashKey(const std::vector<uint8_t> &point,
											const int hashTableId,
											std::vector<PStableHash> &hashFunctions,
											const int m,
											unsigned hashTableSize,
											float R,
											const std::vector<unsigned>& mainHash,
											const std::vector<unsigned>& controlHash,
											uint32_t& chain_key) {
	std::stringstream ss;
	uint32_t dataHashKey;
	ss << hashTableId;

	calcHashKey(point, hashTableId, hashFunctions, m, hashTableSize, R, mainHash, controlHash, dataHashKey, chain_key);
	ss << "_" << dataHashKey;

	return ss.str();
}

void DefaultCalcHashKey::calcHashKey(const std::vector<uint8_t> &point,
									const int hashTableId,
									std::vector<PStableHash> &hashFunctions,
									const int m,
									unsigned hashTableSize,
									float R,
									const std::vector<unsigned>& mainHash,
									const std::vector<unsigned>& controlHash,
									uint32_t& dataHashKey,
									uint32_t& chain_key) {

	std::vector<unsigned int> a;
	std::vector<float> point_reduced;
	for(uint32_t i = 0; i < point.size(); i++){
		point_reduced.push_back((float)point[i]/R);
	}
	// for all hashfunction within this g(hashTableId)
	for(int i = 0; i < m; i++){
//		a.push_back(hashFunctions[m*hashTableId +i].calcHash((std::vector<uint8_t>&)point));
		a.push_back(hashFunctions[m*hashTableId +i].calcHash(point_reduced));

/*		ss << "_";
		ss << dataHashKey[i+1];
*/
	}

	dataHashKey = computeUHashFunction(a, mainHash, hashTableSize);
	chain_key = computeUHashFunction(a, controlHash, hashTableSize);
//	std::cout << "DefaultCalcHashKey: dataHashKey: "<< dataHashKey << " chain_key:" << chain_key << std::endl;
}

unsigned DefaultCalcHashKey::computeBlockProductModDefaultPrime(const std::vector<unsigned>& a, const std::vector<unsigned>& b) {
  long long unsigned h = 0;
  for(uint32_t i = 0; i < a.size(); i++){
	  h += a[i] * b[i];
	  if(h >= UH_PRIME_DEFAULT){
		  h = h % UH_PRIME_DEFAULT;
	  }
  }
  return h;
}

unsigned DefaultCalcHashKey::computeUHashFunction(const std::vector<unsigned>& a, const std::vector<unsigned>& b, unsigned hashTableSize){

	unsigned h = computeBlockProductModDefaultPrime(a, b) % hashTableSize;
	return h;
}

unsigned DefaultCalcHashKey::genRandomUns32(unsigned rangeStart, unsigned rangeEnd) {

  unsigned r;
  if (RAND_MAX >= rangeEnd - rangeStart) {
    r = rangeStart + (unsigned)((rangeEnd - rangeStart + 1.0) * random() / (RAND_MAX + 1.0));
  } else {
    r = rangeStart + (unsigned)((rangeEnd - rangeStart + 1.0) * ((long long unsigned)random() * ((long long unsigned)RAND_MAX + 1) + (long long unsigned)random()) / ((long long unsigned)RAND_MAX * ((long long unsigned)RAND_MAX + 1) + (long long unsigned)RAND_MAX + 1.0));
  }

  return r;
}


std::string DefaultCalcHashKey::KeyInt2Str(uint32_t key, uint32_t hashTableId) {
	std::stringstream ss;

	ss << hashTableId;

	ss << "_" << key;

	return ss.str();
}

