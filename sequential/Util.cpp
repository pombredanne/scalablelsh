/*
 * util.cpp
 *
 *  Created on: Feb 15, 2012
 *      Author: george
 */

#include "Util.h"

long long Util::ClockGetTime()
{
        struct timeval ts;
        gettimeofday(&ts, NULL);
        return (ts.tv_sec*1000000 + (ts.tv_usec))/1000LL;
}


long Util::getNumProcs() {
	return sysconf( _SC_NPROCESSORS_ONLN );
}


const std::string Util::getStringTime() {
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [24];

	time (&rawtime);
	timeinfo = localtime (&rawtime);
	strftime (buffer,24,"%a %F %T",timeinfo);
	std::string r = buffer;
	return r;
}
