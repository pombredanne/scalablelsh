/*
 * FileIn.h
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#ifndef FILEIN_H_
#define FILEIN_H_


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


#define MAX_LINE	10000

class FileIn {
private:
	bool inputOpened;
	bool queryOpened;

	std::ifstream DB_file;
	int numberOfRegisters;

	std::ifstream query_file;
	int numberOfQueries;

	void setNumberOfQueries(int numberOfQueries) {
		this->numberOfQueries = numberOfQueries;
	}

	void setNumberOfRegisters(int numberOfRegisters) {
		this->numberOfRegisters = numberOfRegisters;
	}

	FileIn();

	int32_t readPoint(std::ifstream &file, std::vector<uint8_t> &v);
public:
	FileIn(std::string DBFileName, std::string queryFileName);
	virtual ~FileIn();

	int32_t readInputPoint(std::vector<uint8_t> &p);
	int32_t readQueryPoint(std::vector<uint8_t> &q);

	int getNumberOfQueries() const {
		return numberOfQueries;
	}

	int getNumberOfRegisters() const {
		return numberOfRegisters;
	}

	bool isInputOpened() const {
		return inputOpened;
	}

	void setInputOpened(bool inputOpened) {
		this->inputOpened = inputOpened;
	}

	bool isQueryOpened() const {
		return queryOpened;
	}

	void setQueryOpened(bool queryOpened) {
		this->queryOpened = queryOpened;
	}
};

#endif /* FILEIN_H_ */
