/*
 * LSH.h
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#ifndef LSH_H_
#define LSH_H_

#include "FileIO.h"
#include "HashTable.h"
#include "PointsTable.h"
#include "Operator.h"
#include "ANNPoint.h"
#include "PStableHash.h"
#include "DefaultCalcHashKey.h"
#include "Multiprobe.h"

#include <sstream>

//#ifndef DEBUG_S_LSH
//#define DEBUG_S_LSH
//#endif

class LSH {
private:
	static const unsigned TWO_TO_32_MINUS_1 = 4294967295U;

	FileIO *fileIO;
	HashTable hashTable;
	PointsTable pointsTable;
	PointsTable queryTable;

	// set of all h(v) hash functions, first m are
	// for g1, second set of h are for g2, and so on.
	std::vector<PStableHash> hashFunctions;
	int L;
	int m;
	float R;
	int T; //number of variations of multiprobe;
	bool multiprobe;
	/*
	 * The multiprobe approach does not have the chain_key.
	 * Thus, when comparing with multiprobe we do not use chain_key.
	 */
	bool useChainKey;

	unsigned hashTableSize;

	std::vector<unsigned> mainHash;
	std::vector<unsigned> controlHash;

	LSH();


	std::string calcHashKey(std::vector<uint8_t> &point, int hashTableId, uint32_t &chain_key);

	void calcHashKeyMP(std::vector<uint8_t> &point, int hashTableId, std::vector<unsigned> &seq);

	std::vector<ANNPoint> calcANN(std::vector<uint8_t> query, int k);

	/**
	 * Gets the index of the first reg id. If it does not start from 0 it will add this offset when printing output.
	 * This happens because the pointsTable e queryTable are always 0-based index.
	 */
	unsigned reg_id_offset;
public:
	LSH(FileIO &fIO, int L, int m, int dimensions, int w, float R, int T);
	virtual ~LSH();

	bool buildIndex();
	bool loadQueryFile();
	bool processQueries(int k);
};

#endif /* LSH_H_ */
