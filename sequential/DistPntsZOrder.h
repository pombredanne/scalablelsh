/*
 * DistPntsZOrder.h
 *
 *  Created on: Sep 26, 2013
 *      Author: uq49
 */

#ifndef DISTPNTSZORDER_H_
#define DISTPNTSZORDER_H_

#include <stdint.h>
#include <vector>

#include "DefaultCalcHashKey.h"

/**
 * Calculates the Z-order to distribute the points along the DataPoints instances.
 * Calculates it using 4 by 4 dimensions.
 */
class DistPntsZOrder {
private:
	static uint32_t SeparateBy3(uint8_t i);
	static uint32_t MortonCode4D(uint8_t a, uint8_t b, uint8_t c, uint8_t d);

	DistPntsZOrder();
	virtual ~DistPntsZOrder();

public:
	static uint32_t calcZorderLabel(const std::vector<uint8_t> &pointValues, const uint32_t dim, const uint32_t w_zorder);
};

#endif /* DISTPNTSZORDER_H_ */
