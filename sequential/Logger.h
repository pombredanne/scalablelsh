#ifndef _LOGGER_H
#define _LOGGER_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#define LSH_SUCCESS EXIT_SUCCESS

using namespace std;
enum LogLevel{ERROR, NORMAL, DEBUG};

class Logger {

	public:
	Logger();
	Logger(string errFileName, string logFileName);
	Logger(char * errFileName, char * logFileName);
	~Logger();

	int PrintMsg(string header, string msg);
	int PrintMsg(string classname, string methodname, string msg);
	int PrintDebugMsg(string header, string msg);
	int PrintDebugMsg(string classname, string methodname, string msg);
	int PrintError(string header, string error);
	int PrintError(string classname, string methodname, string error);
	static int PrintFatalError(string classname, string error);
	static int PrintFatalError(string classname, string methodname, string error);

	ofstream& PrintMsgS(string classname, string methodname);
	ofstream& PrintDebugMsgS(string classname, string methodname);
	ofstream& PrintErrorS(string classname, string methodname);


	template<class T>
  	ofstream &operator<< (T const& rhs) {
		this->logFile << rhs;
		return this->logFile;
  	}


	ofstream& level(LogLevel a) {
		if (a == ERROR) {
			return this->errFile;
		} else {
			return this->logFile;
		} 
	}	

	private:
	FILE * OpenFile(string fileName);	
	int CloseFile(FILE ** f);

	static unsigned int nLoggerObjs; //control the number of log objects
	
	
	ofstream logFile;
	ofstream errFile;

//	FILE * errFile;
//	FILE * logFile;	
	
};


#endif
