/*
 * FileIO.h
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#ifndef FILEOUT_H_
#define FILEOUT_H_


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "ANNPoint.h"
#include "Util.h"


class FileOut {
private:
	std::ofstream output_file;

	FileOut();
public:
	FileOut(std::string outputFileName);
	virtual ~FileOut();

	std::ofstream& getOutputFile();
	void printANN(const std::vector<ANNPoint>& ANN, const int ind, const int offset);
	void printANN(const std::vector<ANNPoint>& ANN, const int ind);

	/**
	 * Print the header lines of output file.
	 * @param k Parameter k nearest neighbors.
	 */
	void printOutputHeader(const uint32_t k);
};

#endif /* FILEOUT_H_ */
