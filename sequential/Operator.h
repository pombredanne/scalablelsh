/*
 * Operator.h
 *
 *  Created on: May 20, 2012
 *      Author: george
 */

#ifndef OPERATOR_H_
#define OPERATOR_H_

#include "ANNPoint.h"
#include "DataBucket.h"
#include "PointsTable.h"
#include "DBPnt.h"
#include <vector>
#include <assert.h>
#include <math.h>



class Operator {
private:
	Operator();
	virtual ~Operator();
public:
	static void calculateANN(std::vector<ANNPoint> &curANN,
							uint32_t k,
							std::vector<uint8_t> &query,
							DataBucket<DBPnt> * bucket,
							PointsTable& pointsTable);


};

#endif /* OPERATOR_H_ */
