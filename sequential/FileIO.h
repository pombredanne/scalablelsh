/*
 * FileIO.h
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#ifndef FILEIO_H_
#define FILEIO_H_


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "FileIn.h"
#include "FileOut.h"
#include "ANNPoint.h"


#define MAX_LINE	10000

class FileIO {
private:
	FileIn * f_io_in;
	FileOut * f_io_out;

	FileIO();
public:
	FileIO(std::string DBFileName, std::string queryFileName, std::string outputFileName);
	virtual ~FileIO();

	int32_t readInputPoint(std::vector<uint8_t> &p);
	int32_t readQueryPoint(std::vector<uint8_t> &q);

	std::ofstream& getOutputFile();
	unsigned int getNumDBRegisters();

	void printANN(const std::vector<ANNPoint>& ANN, const int ind, const int offset);
	void printANN(const std::vector<ANNPoint>& ANN, const int ind);

	void printOutputHeader(const uint32_t k);
};

#endif /* FILEIO_H_ */
