/*
 * FileIO.cpp
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#include "FileIO.h"

FileOut::FileOut(std::string outputFileName) {
	if(outputFileName.size() > 0){
		output_file.open(outputFileName.c_str());
		if(output_file.fail()){
			std::cout << "Failed to open file:"<< outputFileName << std::endl;
			exit(1);
		}
	}
}

FileOut::~FileOut() {
	output_file.close();
}

std::ofstream& FileOut::getOutputFile(){
	return output_file;
}

void FileOut::printANN(const std::vector<ANNPoint>& ANN, const int ind) {
	printANN(ANN, ind, 0);
}


void FileOut::printANN(const std::vector<ANNPoint>& ANN, const int ind, const int offset) {
	output_file << ind + offset;
//		std::cout << "Query_id="<<i+1<<" size="<<ANN.size()<<std::endl;
	for(uint32_t j = 0; j < ANN.size(); j++){

		output_file << " " << ANN[j].getId() + offset <<" "<<ANN[j].getDistance();
//			std::cout << "Id="<< ANN[j].id <<" distance="<<ANN[j].distance<<std::endl;
	}
	output_file << " " << Util::ClockGetTime() <<std::endl;

}

void FileOut::printOutputHeader(const uint32_t k) {
	this->getOutputFile() << "MATCHPOINTS" << std::endl;
	this->getOutputFile() << k << std::endl;
}
