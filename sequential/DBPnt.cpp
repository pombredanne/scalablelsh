/*
 * DBPnt.cpp
 *
 *  Created on: Sep 19, 2013
 *      Author: uq49
 */

#include "DBPnt.h"

DBPnt::DBPnt(uint32_t pId, uint32_t lbl) : pointId(pId), label(lbl) {}

DBPnt::~DBPnt() {}


uint32_t DBPnt::getLabel() const {
	return label;
}

uint32_t DBPnt::getPointId() const {
	return pointId;
}
