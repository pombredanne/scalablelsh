/*
 * Operator.cpp
 *
 *  Created on: May 20, 2012
 *      Author: george
 */

#include "Operator.h"

Operator::Operator() {

}

Operator::~Operator() {
}

void Operator::calculateANN(std::vector<ANNPoint>& curANN, uint32_t k, std::vector<uint8_t> &query, DataBucket<DBPnt>* bucket, PointsTable& pointsTable) {
	for(size_t i=0; i < bucket->getSize(); i++){
		ANNPoint point;
		//point.id=bucket->getPoint(i);
		const DBPnt& dbpnt = bucket->getPoint(i);
		point.setId(dbpnt.getPointId());
		//point.distance = pointsTable.distance( query, point.getId()));
		point.setDistance(pointsTable.distance( query, point.getId()));
		std::vector<ANNPoint>::iterator it = curANN.begin();

//		std::cout << "[Operator][calculateANN] p id " << point.getId() << " dist " << point.getDistance() << std::endl;

		if(it==curANN.end()){
			curANN.push_back(point);
		}else{
			bool insert=true;

			// Find the place to insert point
			while(it != curANN.end()){
				// Avoid the same point from being inserted due different hash functions
				if((*it).getId() == point.getId()){
					insert=false;
				}
				// If found the place to insert point
				if((*it).getDistance() > point.getDistance() ){
					break;
				}

				it++;
			}
			if(insert)
				curANN.insert(it, point);
		}
		if(curANN.size() > k){
			curANN.pop_back();
		}
	}
}



