project(LSH_sequential_Examples)

# Make sure the compiler can find the includes in the LSH directory
include_directories(${LSH_sequential_SOURCE_DIR})

# Make sure the compiler can find the library in LSH directory
link_directories(${LSH_sequential_SOURCE_DIR})


set(programs LSH_hello)
foreach(program ${programs})
        # Add an executable to be built from the files listed
        add_executable(${program} ${program}.cpp)

        # Link the executable to the Features Computation library
        target_link_libraries(${program} lsh)
endforeach(program)


