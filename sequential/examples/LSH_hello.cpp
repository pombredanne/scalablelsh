

#define NUM_PARAMS 21

#include <string>
#include <iostream>
#include <unistd.h>

#include "FileIO.h"
#include "LSH.h"
#include "Util.h"

using namespace std;

int main(int argc, char ** argv){

	string pointsFileName;
	string queryFileName;
	string outputFileName;

	int L = -1, k = -1, m = -1, d = -1, w = -1, T = 0;
	float R = 1.0;
	
	if (argc != NUM_PARAMS) {
		cout << "Usage: ./lsh -p <pointsFileName> -q <queryFileName> -o <outputFileName> -k <kValue> -L <#HashTables> -m <#HashFunctionPerFunctionL> -d <#dimensions> -w <w (use 4 as default)> -r (radius) -T <max variations o hashkey in multiprobe, greater than 1 means multiprobe will be used>"<<endl;
		exit(1);
	}

	int optc;
	while ((optc = getopt (argc, argv, "p:P:q:Q:k:K:l:L:o:O:m:M:d:D:w:W:r:R:t:T:")) != -1) {
		switch (optc)
		{
			case 'p':
			case 'P':
				if (optarg != NULL) {   
					//le o nome do arquivo de entrada que contem os pontos 
					pointsFileName.append(optarg);
					cout<<pointsFileName<<endl;
				}
				break;	
			case 'q':
			case 'Q':
				if (optarg != NULL) {   
					//le o nome do arquivo de entrada com o ponto de consulta
					queryFileName.append(optarg);
					cout<<queryFileName<<endl;
				}
				break;
			case 'o':
			case 'O':
				if (optarg != NULL) {
					//le o nome do arquivo de entrada com o ponto de consulta
					outputFileName.append(optarg);
					cout<<outputFileName<<endl;
				}
				break;
			case 'k':
			case 'K':
				if (optarg != NULL) {
					//le o numero de vizinhos a serem retornados pelo programa
					k = atoi(optarg);	
					cout<<"k="<<k<<endl;
				} 
				break;
			case 'l':
			case 'L':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					L = atoi(optarg);
					cout<<"L="<<L<<endl;
				}
				break;
			case 'm':
			case 'M':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					m = atoi(optarg);
					cout<<"m="<<m<<endl;
				}
				break;
			case 'd':
			case 'D':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					d = atoi(optarg);
					cout<<"d="<<d<<endl;
				}
				break;
			case 'w':
			case 'W':
				if (optarg != NULL) {
					//Number of hashtables created by the application
					w = atoi(optarg);
					cout<<"w="<<w<<endl;
				}
				break;
			case 'r':
			case 'R':
				if (optarg != NULL) {
					//Radius for the RNN search
					R = atof(optarg);
					cout<<"R="<<R<<endl;
				}
				break;
			case 't':
			case 'T':
				if (optarg != NULL) {
					//max number of hashkey variations for multiprobe
					T = atoi(optarg);
					cout<<"T="<<T<<endl;
				}
				break;
			default:
				std::cout << "Unrecognized parameter" <<std::endl;
				break;
		}
	}

	FileIO f_IO(pointsFileName, queryFileName, outputFileName);
	LSH lsh(f_IO, L, m, d, w, R, T);

	long long t1 = Util::ClockGetTime();
	std::cout << "Building LSH index" << std::endl;
	lsh.buildIndex();
	long long t2 = Util::ClockGetTime();
	std::cout << "Time = "<< t2-t1 << " (miliseconds)" << std::endl;

	t1 = Util::ClockGetTime();
	std::cout << "Loading queries "<< std::endl;
	lsh.loadQueryFile();
	t2 = Util::ClockGetTime();
	std::cout << "Time = "<< t2-t1 << " (miliseconds)" << std::endl;

	t1 = Util::ClockGetTime();
	std::cout << "Processing queries "<< std::endl;
	lsh.processQueries(k);
	t2 = Util::ClockGetTime();
	std::cout << "Time = "<< t2-t1 << " (miliseconds)" << std::endl;

	return 0;
}
