/*
 * PStableHash.h
 *
 *  Created on: May 21, 2012
 *      Author: george
 */

#ifndef PSTABLEHASH_H_
#define PSTABLEHASH_H_

#include <vector>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <stdint.h>
#include <stdlib.h>

#define DEFAULT_INITIAL_SEED 10

class PStableHash {
private:
	std::vector<float> a;
	float b;
	int w;

	float genGaussianRandom();
	float genUniformRandom(float rangeStart, float rangeEnd);
public:

	PStableHash(int dimensions, int w);
	virtual ~PStableHash();
	unsigned calcHash(std::vector<uint8_t>& point);
	unsigned calcHash(std::vector<float>& point);
	unsigned calcHash(std::vector<float>& point, float * delta);
};

#endif /* PSTABLEHASH_H_ */
