+ Entrada do programa:
	- arquivo txt com os pontos de entrada (olhar arquivo p.txt)
	- arquivo txt com a query (olhar arquivo q.txt)
	- arquivo txt com a associacao (o arquivo só contem os valores das celulas definidas por uma linha e uma coluna, nao contem as linhas e as colunas)
		(olhar arquivo a.txt)
		exemplo:	para pontos com 9 dimensoes cada, podemos ter a seguinte associacao (o arquivo só contem os valores dentro do recorte abaixo) 	
						0 1 2				ou até mesmo          0 1 2
					  _ _ _ _                               _ _ _ _
					0 |0 1 2                             0 |3 5 8 
					1 |3 4 5                             1 |2 4 0
					2 |6 7 8                             2 |6 1 7

+ Como executar o programa:
	- ./multicurves -a <associationFileName> -p <pointsFileName> -q <queryFileName> -m <mValue> -d <depth> -k <kValue>
	- para maiores detalhes, favor olhar o código
	- pode colocar qualquer valor para "m", ele não está sendo usado, deixei ele para manter a compatibilidade e para o caso de precisar...
	
+ Saida do programa:
	- arquivo txt com os K pontos mais próximos da query
	
+ Observações:
	- acionar a flag DEBUG no Makefile para que o programa imprima informações de cada passo do programa
