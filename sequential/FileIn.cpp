/*
 * FileIn.cpp
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#include "FileIn.h"

FileIn::FileIn(std::string DBFileName, std::string queryFileName) : numberOfRegisters(0), numberOfQueries(0) {
	char line[MAX_LINE];

	/** Try to open points file. */
	if(DBFileName.size() > 0){
		DB_file.open(DBFileName.c_str(),std::ifstream::in);
		if(DB_file.fail()){
			std::cout << " ==================================== " << std::endl;
			std::cout << "WARNING!! Could not found or open file: " << DBFileName << std::endl;
			std::cout << " ==================================== " << std::endl;
			inputOpened = false;
		} else {
			inputOpened = true;
			std::cout << "[FileIn] " << DBFileName << " has been opened as registers file." << std::endl;
			DB_file.getline(line,MAX_LINE);// Read header. Not used for anything
			DB_file.getline(line,MAX_LINE);// Read number of images. Also not used
			DB_file.getline(line,MAX_LINE);// Read number of vectors. This is used by LSH
			line[MAX_LINE-1] = '\0';
			this->setNumberOfRegisters(atoi(line));
			DB_file.getline(line,MAX_LINE);// Read second header. This is not used by LSH
		}
	}

	/** Try to open query file. */
	if(queryFileName.size() > 0){
		query_file.open(queryFileName.c_str(),std::ifstream::in);
		if(query_file.fail()){
			std::cout << " ==================================== " << std::endl;
			std::cout << "WARNING!! Could not found or open file: " << queryFileName << std::endl;
			std::cout << " ==================================== " << std::endl;
			queryOpened = false;
		} else {
			queryOpened = true;
			std::cout << "[FileIn] " << queryFileName << " has been opened as query file." << std::endl;
			// read a single line
			query_file.getline(line,MAX_LINE);// Read header. Not used for anything
			query_file.getline(line,MAX_LINE);// Read number of images. Also not used
			query_file.getline(line,MAX_LINE);// Read number of vectors(queries). This is used by LSH
			line[MAX_LINE-1] = '\0';
			this->setNumberOfQueries(atoi(line));
			query_file.getline(line,MAX_LINE);// Read second header. This is not used by LSH
		}
	}

}

FileIn::~FileIn() {
	DB_file.close();
	query_file.close();
}

int32_t FileIn::readInputPoint(std::vector<uint8_t> &p){
	int p_id = 0;

	if (inputOpened) {
		p_id = readPoint(DB_file, p);
	}

#ifdef DEBUG
	std::cout<<std::endl<<"Point " << p_id << ":"<<std::endl;
	for(int j=0;j<p.size();j++){
		std::cout<<(int)p[j]<<" ";
	}
	std::cout<<std::endl;
#endif

	return p_id;
}

int32_t FileIn::readQueryPoint(std::vector<uint8_t> &q){
	int q_id = 0;

	if(queryOpened) {
		q_id = readPoint(query_file, q);
	}

#ifdef DEBUG
	std::cout<<std::endl<<"Query "<< q_id << ":"<<std::endl;
	for(int j=0;j<p.size();j++){
		std::cout<<(int)p[j]<<" ";
	}
	std::cout<<std::endl;
#endif

	return q_id;
}

int32_t FileIn::readPoint(std::ifstream &file, std::vector<uint8_t> &v) {
	// temp variables using during the line parsing
	int v_id = -1;
	int dimension;
	char line[MAX_LINE];
	char *token;

	// read a single line
	file.getline(line,MAX_LINE);
	line[MAX_LINE-1]='\0';

	// break line into individual values
	token = strtok(line," \n");

	if (token != NULL) {
		// get first field with the id
		v_id = atoi(token);

		// through out 6 first fields of the line. They are not used in the algorithm.
		for(int i = 0; i < 6; i++){
			token = strtok(NULL," \n");
		}

		// add each of the line values to the point (p) vector
		while(token!=NULL){
			dimension = atoi(token);
			v.push_back(uint8_t(dimension));
			token = strtok(NULL," \n");
		}
	}

	return v_id;
}
