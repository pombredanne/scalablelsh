/*
 * PointsTable.cpp
 *
 *  Created on: May 19, 2012
 *      Author: george
 */

#include "PointsTable.h"

PointsTable::PointsTable() {
	pointDimensions = 0;
}

PointsTable::~PointsTable() {
	points.clear();
}

bool PointsTable::addPoint(const std::vector<uint8_t> &point) {
	if(pointDimensions == 0)
		pointDimensions=point.size();

	points.insert(points.end(), point.begin(), point.end());
//	std::cout << (int)points[1]<<" point.size()="<< point.size()<<std::endl;
	return true;
}

/**
 * This has been developed to add points in order according to their id, independent of their
 * arrival order.
 * \Attention The pointId must start from 0.
 * @param point
 * @return
 */
bool PointsTable::addPoint(const uint8_t * point, const size_t pointId, const size_t dim) {
	if(pointDimensions == 0)
		pointDimensions=dim;

//	std::cout << "[PointsTable][addPoint] Insert pId " << pointId << std::endl;

	/** Get the additionaSize in number of points, regards the pointId starts from 0. */
	ssize_t additionalSize = (pointId + 1) - getSize();
//	std::cout << " additionalSize " << additionalSize << " pointId+1 " << pointId+1 << " getSize() " << getSize() << std::endl;
	if ( additionalSize > 1 ) {
		size_t newSize = points.size() + (additionalSize * pointDimensions);
//		std::cout << "new size "  << newSize << " max size " << points.max_size() << std::endl;
		try {
			points.resize(newSize);
		} catch (const std::bad_alloc&) {
			std::cout << "[PointsTable][addPoint] ERROR Alloc has not been possible with resize. " << newSize << std::endl;
			return false;
		}

//		std::cout << "[PointsTable][addPoint] Resize pId " << pointId << " newSize " << newSize << std::endl;
	}

	/** To avoid resize, when the point is next just do an insert. */
	if (additionalSize == 1) {
//		std::cout << "[PointsTable][addPoint] a "<< std::endl;
		try {
			points.insert(points.end(), point, point + dim);
		} catch (const std::bad_alloc&) {
			std::cout << "[PointsTable][addPoint] ERROR Alloc has not been possible with insert. " << points.size() << std::endl;
			return false;
		}
//		std::cout << "[PointsTable][addPoint] Insert pId " << pointId << " dim " << dim << " addSize " << additionalSize << std::endl;
	} else {
		size_t start = pointId * pointDimensions, i2 = 0;
		for (size_t i = start; i < start + pointDimensions; i++) {
			points[i] = point[i2++];
		}
//		std::cout << "[PointsTable][addPoint] No insert pId " << pointId << " dim " << dim << " addSize " << additionalSize << std::endl;
	}

//	std::cout << "+++ point size " << points.size() << " n_p " << getSize() << std::endl;
	return true;
}


std::vector<uint8_t> PointsTable::getPoint(const size_t id) {
	std::vector<uint8_t> ret_point;
	if(pointDimensions * (id+1) <= points.size()){
		for(size_t i = 0; i < pointDimensions; i++){
			ret_point.push_back(points[(id*pointDimensions)+i]);
		}
	}
	return ret_point;
}

size_t PointsTable::getSize() {
	return points.size()/pointDimensions;
}

float PointsTable::distance(const std::vector<uint8_t> &query, const size_t pointId) {
	float distance=0.0;
	assert(query.size() == pointDimensions);

	if(pointDimensions * (pointId+1) <= points.size()){
		for(size_t i=0; i < query.size(); i++){
			distance+=(query[i] - points[(pointId*pointDimensions)+i])*(query[i] - points[(pointId*pointDimensions)+i]) ;
		}
		distance=sqrtf(distance);
	}else{
		std::cout << "[PointsTable][distance] Error: pointId="<<pointId<<" not found!"<<std::endl;
		exit(1);
	}
	return distance;
}

void PointsTable::print() {
	std::vector<uint8_t> point;
	int id=0;
	while((point = getPoint(id)).size() > 0){
		for(uint32_t i = 0; i < point.size(); i++){
			std::cout << (int)point[i] << " ";
		}
		std::cout << std::endl;
		id++;
	}
}



