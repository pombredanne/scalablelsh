/*
 * HashTable.cpp
 *
 *  Created on: May 18, 2012
 *      Author: george
 */

#include "HashTable.h"

HashTable::HashTable() {
}

HashTable::~HashTable() {
	std::tr1::unordered_map<std::string, std::list<DataBucket<DBPnt>*> >::iterator it = buckets_hash.begin();
//	std::map<std::string, DataBucket*>::iterator it = buckets_hash.begin();
	while(it != buckets_hash.end()){
		std::list<DataBucket<DBPnt>*>::iterator bucket_chain_iterator = (*it).second.begin();
		for(; bucket_chain_iterator != (*it).second.end(); bucket_chain_iterator ++){
			delete (*bucket_chain_iterator);
		}
		//(*it).second.clear();
//		delete (*it).second;
		it++;
	}
	buckets_hash.clear();
}

bool HashTable::addPoint(std::string bucket_key, uint32_t chain_key, uint32_t point, uint32_t label) {
	std::tr1::unordered_map<std::string, std::list<DataBucket<DBPnt>*> >::iterator it = buckets_hash.find(bucket_key);

	DataBucket<DBPnt> *curBucket = NULL;
	// if bucket with give key exists
	if(it != buckets_hash.end()){
		std::list<DataBucket<DBPnt>*>::iterator bucket_chain_iterator = (*it).second.begin();
		for(; bucket_chain_iterator != (*it).second.end(); bucket_chain_iterator ++){
			DataBucket<DBPnt> *aux = (*bucket_chain_iterator);
			if(aux->getChainKey() == chain_key){
				curBucket = aux;
				break;
			}
		}
		// bucket with given chain_key does not exist in table
		if(curBucket == NULL){
			// Create the first bucket that will be part of the chain
			curBucket = new DataBucket<DBPnt>();
			curBucket->setChainKey(chain_key);
			// insert bucket to the chain in the map
			(*it).second.push_back(curBucket);
		}
	}else{
		// create an empty list for chaining that given hash entry (bucket_key)
		std::list<DataBucket<DBPnt>*> bucket_chain;
		// create a bucket
		curBucket = new DataBucket<DBPnt>();
		curBucket->setChainKey(chain_key);
		// insert new bucket into the chain
		bucket_chain.push_back(curBucket);
		// insert bucket chain to the map
		buckets_hash.insert( std::pair<std::string, std::list<DataBucket<DBPnt>*> >(bucket_key, bucket_chain) );
	}
	curBucket->addPoint(DBPnt(point, label));

	return true;
}

DataBucket<DBPnt>* HashTable::retrieveDataBucket(std::string bucket_key, uint32_t chain_key) {
	DataBucket<DBPnt> *curBucket = NULL;
	std::tr1::unordered_map<std::string, std::list<DataBucket<DBPnt>*> >::iterator it = buckets_hash.find(bucket_key);

	// if bucket with give key exists
	if(it != buckets_hash.end()){
		std::list<DataBucket<DBPnt>*>::iterator bucket_chain_iterator = (*it).second.begin();
		for(; bucket_chain_iterator != (*it).second.end(); bucket_chain_iterator ++){
			DataBucket<DBPnt> *aux = (*bucket_chain_iterator);
			if(aux->getChainKey() == chain_key){
				curBucket = aux;
				break;
			}
		}

	}
	return curBucket;
}



