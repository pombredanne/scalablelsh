#include "Logger.h"

/*
 * Static members.
 */ 

unsigned int Logger::nLoggerObjs = 0;

/*
 * Routines Implementation.
 */
Logger::Logger() {
	errFile.open("Logger.txt");
	if(errFile.fail()) {
		Logger::PrintFatalError("Logger", "Could not open error file on constructor.");
	}
	logFile.open("Logger.txt");
	if(logFile.fail()) {
		Logger::PrintFatalError("Logger", "Could not open logger file on constructor.");
	}

	nLoggerObjs++;
}

Logger::Logger(string errFileName,  string logFileName) {

	if(!errFileName.empty()) {
		errFile.open(errFileName.c_str());
		if(errFile.fail()) {
			cerr << "[Logger] FATAL ERROR Could not open file " << errFileName << "." << endl; 
		}
	}
	
	if(!logFileName.empty()) {
		logFile.open(logFileName.c_str());
		if(errFile.fail()) {
			cerr << "[Logger] FATAL ERROR Could not open file " << logFileName << "." << endl; 
		}
	}	
	nLoggerObjs++;
}

Logger::Logger(char * errFileName,  char * logFileName) {

	if(strlen(errFileName) > 0) {
		errFile.open(errFileName);
		if(errFile.fail()) {
			cerr << "[Logger] FATAL ERROR Could not open file " << errFileName << "." << endl;
		}
	}

	if(strlen(logFileName) > 0) {
		logFile.open(logFileName);
		if(errFile.fail()) {
			cerr << "[Logger] FATAL ERROR Could not open file " << logFileName << "." << endl;
		}
	}
	nLoggerObjs++;
}


Logger::~Logger() {
	errFile.close();
	logFile.close();
	nLoggerObjs--;
}


int Logger::PrintMsg(string header, string msg) {
//	fprintf(logFile, "%s %s\n", header.c_str(), msg.c_str());
//	fflush(logFile);
	logFile << header << " " << msg.c_str() << endl;	
	logFile.flush();
	return LSH_SUCCESS;
}

int Logger::PrintMsg(string classname, string methodname, string msg) {
//	fprintf(logFile, "[%s][%s] %s\n", classname.c_str(), methodname.c_str(), msg.c_str());
//	fflush(logFile);
	logFile << "[" << classname << "]" << "[" << methodname << "] " << msg << endl;
	logFile.flush();
	return LSH_SUCCESS;
}

int Logger::PrintDebugMsg(string classname, string msg) {
//	fprintf(logFile, "[DEBUG][%s] %s\n", classname.c_str(), msg.c_str());
//	fflush(logFile);
	logFile << "[DEBUG][" << classname << "] " << msg << endl;
	logFile.flush();
	return LSH_SUCCESS;
}

int Logger::PrintDebugMsg(string classname, string methodname, string msg) {
//	fprintf(logFile, "[DEBUG][%s][%s] %s\n", classname.c_str(), methodname.c_str(), msg.c_str());
//	fflush(logFile);
	logFile << "[DEBUG][" << classname << "][" << methodname << "] " << msg << endl;
	logFile.flush();
	return LSH_SUCCESS;
}

int Logger::PrintError(string header, string error) {
	errFile << "[" << header << "] ERROR " << error << endl;     	
	errFile.flush();
	cerr << "[" << header << "] ERROR " << error << endl;
	cerr.flush();
	return LSH_SUCCESS;
}

int Logger::PrintError(string classname, string methodname, string error) {
	errFile << "[" << classname << "][" << methodname << "] ERROR " << error << endl; 
	errFile.flush();
	cerr << "[" << classname << "][" << methodname << "] ERROR " << error << endl;
	cerr.flush();
	return LSH_SUCCESS;
}


ofstream& Logger::PrintMsgS(string classname, string methodname) {
	logFile << "[" << classname << "]" << "[" << methodname << "] ";
	logFile.flush();
	return this->logFile;
}

ofstream& Logger::PrintDebugMsgS(string classname, string methodname) {
	logFile << "[DEBUG][" << classname << "][" << methodname << "] ";
	logFile.flush();
	return this->logFile;
}

ofstream& Logger::PrintErrorS(string classname, string methodname) {
	errFile << "[" << classname << "][" << methodname << "] ERROR "; 
	errFile.flush();
	cerr << "[" << classname << "][" << methodname << "] ERROR " << endl; 
	cerr.flush();
	return this->errFile;
}


int Logger::PrintFatalError(string classname, string error) {
//	fprintf(stderr, "[%s] FATAL ERROR %s\n",classname.c_str(), error.c_str());
//	fflush(stderr);
	cerr << "[" << classname << "] FATAL ERROR " << error << endl; 	
	cerr.flush();
	return LSH_SUCCESS;
}

int Logger::PrintFatalError(string classname, string methodname, string error) {
//	fprintf(stderr, "[%s][%s] FATAL ERROR %s\n",classname.c_str(), methodname.c_str(), error.c_str());
//	fflush(stderr);
	cerr << "[" << classname << "][" << methodname << "] FATAL ERROR " << error << endl; 	
	cerr.flush();
	return LSH_SUCCESS;
}

FILE * Logger::OpenFile(string fileName) {
	FILE * f = fopen(fileName.c_str(), "w");
	if(!f)	{
		PrintError("[Logger][OpenFile]", "Could not open file %s.", fileName.c_str());
	}
	return f;
}

int Logger::CloseFile(FILE ** f) {
	fclose(*f);
	return LSH_SUCCESS;
}

